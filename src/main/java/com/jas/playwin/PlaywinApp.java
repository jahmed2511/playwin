package com.jas.playwin;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ScheduledFuture;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;

import com.jas.playwin.config.ApplicationProperties;
import com.jas.playwin.config.DefaultProfileUtil;
import com.jas.playwin.domain.CronScheduler;
import com.jas.playwin.domain.Game;
import com.jas.playwin.domain.GameResult;
import com.jas.playwin.service.CronSchedulerService;
import com.jas.playwin.service.GameResultService;
import com.jas.playwin.service.GameService;
import com.jas.playwin.service.JobService;
import com.jas.playwin.service.impl.GameDrawerService;
import com.jas.playwin.service.util.DrawerThread;

import io.github.jhipster.config.JHipsterConstants;
import lombok.Data;

@SpringBootApplication
@EnableConfigurationProperties({ LiquibaseProperties.class, ApplicationProperties.class })
@EnableDiscoveryClient
@Data
public class PlaywinApp implements InitializingBean {

	private static final Logger log = LoggerFactory.getLogger(PlaywinApp.class);

	private final Environment env;

	@Autowired
	private CronSchedulerService cronSchedulerService;

	@Autowired
	private ThreadPoolTaskScheduler taskScheduler;

	@Autowired
	private GameResultService gameResultService;

	@Autowired
	private GameService gameService;

	@Autowired
	private GameDrawerService gameDrawerService;


	public PlaywinApp(Environment env) {
		this.env = env;
	}

	/**
	 * Initializes playwin.
	 * <p>
	 * Spring profiles can be configured with a program argument
	 * --spring.profiles.active=your-active-profile
	 * <p>
	 * You can find more information on how profiles work with JHipster on <a href=
	 * "https://www.jhipster.tech/profiles/">https://www.jhipster.tech/profiles/</a>.
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		Collection<String> activeProfiles = Arrays.asList(env.getActiveProfiles());
		if (activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_DEVELOPMENT)
				&& activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_PRODUCTION)) {
			log.error("You have misconfigured your application! It should not run "
					+ "with both the 'dev' and 'prod' profiles at the same time.");
		}
		if (activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_DEVELOPMENT)
				&& activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_CLOUD)) {
			log.error("You have misconfigured your application! It should not "
					+ "run with both the 'dev' and 'cloud' profiles at the same time.");
		}

		log.info("========================================================================================");
		log.info("All properties are set now. Fetcing cron data and restarting game draw operation...");
		restartJobs();
		log.info("========================================================================================");

	}

	/**
	 * Main method, used to run the application.
	 *
	 * @param args the command line arguments.
	 */
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(PlaywinApp.class);
		DefaultProfileUtil.addDefaultProfile(app);
		Environment env = app.run(args).getEnvironment();
		logApplicationStartup(env);
	}

	private static void logApplicationStartup(Environment env) {
		String protocol = "http";
		if (env.getProperty("server.ssl.key-store") != null) {
			protocol = "https";
		}
		String serverPort = env.getProperty("server.port");
		String contextPath = env.getProperty("server.servlet.context-path");
		if (StringUtils.isBlank(contextPath)) {
			contextPath = "/";
		}
		String hostAddress = "localhost";
		try {
			hostAddress = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			log.warn("The host name could not be determined, using `localhost` as fallback");
		}
		log.info(
				"\n----------------------------------------------------------\n\t"
						+ "Application '{}' is running! Access URLs:\n\t" + "Local: \t\t{}://localhost:{}{}\n\t"
						+ "External: \t{}://{}:{}{}\n\t"
						+ "Profile(s): \t{}\n----------------------------------------------------------",
				env.getProperty("spring.application.name"), protocol, serverPort, contextPath, protocol, hostAddress,
				serverPort, contextPath, env.getActiveProfiles());
	}

	private void restartJobs() {
		List<CronScheduler> cronSchedulers = cronSchedulerService.findAll();
		if (cronSchedulers.isEmpty()) {
			log.info("No jobs found to be started on up time.");
			return;
		}
		JobService.activeJobs.clear();
		cronSchedulers.forEach(cronJob -> {

			Optional<Game> gameEntity = gameService.findOne(cronJob.getGameId());

			if (gameEntity.isPresent()) {
				CronTrigger cronTrigger = new CronTrigger(cronJob.getCronExp());
				Game game = gameEntity.get();
				DrawerThread drawer = new DrawerThread(game, null, gameDrawerService);
				ScheduledFuture<?> scheduleAtFixedRate = taskScheduler.schedule(drawer, cronTrigger);

				JobService.activeJobs.put(game.getId(), scheduleAtFixedRate);

				log.info("Cron started for Game:" + game.getGameName());

				// TODO Add down time game draw logic
				GameResult gameLastDraw = gameResultService.findLastDraw(game);
				if (gameLastDraw != null) {
					gameDrawerService.drawGameDowntime(game, gameLastDraw, cronJob.getCronExp());
				}

			} else {
				log.error("No game entry found for the game id : " + cronJob.getGameId());
			}
		});
		log.info("Starting job operation is completed.");
	}

}