package com.jas.playwin.service;

import com.jas.playwin.domain.User;
import com.jas.playwin.domain.Wallet;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Wallet}.
 */
public interface WalletService {

    /**
     * Save a wallet.
     *
     * @param wallet the entity to save.
     * @return the persisted entity.
     */
    Wallet save(Wallet wallet);

    /**
     * Get all the wallets.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Wallet> findAll(Pageable pageable);


    /**
     * Get the "id" wallet.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Wallet> findOne(Long id);

    /**
     * Delete the "id" wallet.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the wallet corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Wallet> search(String query, Pageable pageable);
    



	/**
	 * Save all.
	 *
	 * @param wallets the wallets
	 * @return the list
	 */
	List<Wallet> saveAll(List<Wallet> wallets);

	/**
	 * Gets the recent wallet by user.
	 *
	 * @param user the user
	 * @return the recent wallet by user
	 */
	Wallet getRecentWalletByUser(User user);
	
	Wallet findByUserId(Long userId);
}
