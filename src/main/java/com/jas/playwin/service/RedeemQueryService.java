package com.jas.playwin.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.jas.playwin.domain.Redeem;
import com.jas.playwin.domain.*; // for static metamodels
import com.jas.playwin.repository.RedeemRepository;
import com.jas.playwin.repository.search.RedeemSearchRepository;
import com.jas.playwin.service.dto.RedeemCriteria;

/**
 * Service for executing complex queries for {@link Redeem} entities in the database.
 * The main input is a {@link RedeemCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Redeem} or a {@link Page} of {@link Redeem} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RedeemQueryService extends QueryService<Redeem> {

    private final Logger log = LoggerFactory.getLogger(RedeemQueryService.class);

    private final RedeemRepository redeemRepository;

    private final RedeemSearchRepository redeemSearchRepository;

    public RedeemQueryService(RedeemRepository redeemRepository, RedeemSearchRepository redeemSearchRepository) {
        this.redeemRepository = redeemRepository;
        this.redeemSearchRepository = redeemSearchRepository;
    }

    /**
     * Return a {@link List} of {@link Redeem} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Redeem> findByCriteria(RedeemCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Redeem> specification = createSpecification(criteria);
        return redeemRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Redeem} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Redeem> findByCriteria(RedeemCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Redeem> specification = createSpecification(criteria);
        return redeemRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RedeemCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Redeem> specification = createSpecification(criteria);
        return redeemRepository.count(specification);
    }

    /**
     * Function to convert RedeemCriteria to a {@link Specification}.
     */
    private Specification<Redeem> createSpecification(RedeemCriteria criteria) {
        Specification<Redeem> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Redeem_.id));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), Redeem_.status));
            }
            if (criteria.getAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAmount(), Redeem_.amount));
            }
            if (criteria.getApprovedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getApprovedBy(), Redeem_.approvedBy));
            }
            if (criteria.getRemarks() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRemarks(), Redeem_.remarks));
            }
            if (criteria.getCreatedOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedOn(), Redeem_.createdOn));
            }
            if (criteria.getLastModifiedOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedOn(), Redeem_.lastModifiedOn));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(Redeem_.user, JoinType.LEFT).get(User_.id)));
            }
        }
        return specification;
    }
}
