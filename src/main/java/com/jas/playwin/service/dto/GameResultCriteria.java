package com.jas.playwin.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.jas.playwin.domain.GameResult} entity. This class is used
 * in {@link com.jas.playwin.web.rest.GameResultResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /game-results?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class GameResultCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private InstantFilter gameTime;

    private StringFilter winnersNos;

    private StringFilter remarks;

    private LongFilter gameId;

    private LongFilter coinId;

    public GameResultCriteria(){
    }

    public GameResultCriteria(GameResultCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.gameTime = other.gameTime == null ? null : other.gameTime.copy();
        this.winnersNos = other.winnersNos == null ? null : other.winnersNos.copy();
        this.remarks = other.remarks == null ? null : other.remarks.copy();
        this.gameId = other.gameId == null ? null : other.gameId.copy();
        this.coinId = other.coinId == null ? null : other.coinId.copy();
    }

    @Override
    public GameResultCriteria copy() {
        return new GameResultCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public InstantFilter getGameTime() {
        return gameTime;
    }

    public void setGameTime(InstantFilter gameTime) {
        this.gameTime = gameTime;
    }

    public StringFilter getWinnersNos() {
        return winnersNos;
    }

    public void setWinnersNos(StringFilter winnersNos) {
        this.winnersNos = winnersNos;
    }

    public StringFilter getRemarks() {
        return remarks;
    }

    public void setRemarks(StringFilter remarks) {
        this.remarks = remarks;
    }

    public LongFilter getGameId() {
        return gameId;
    }

    public void setGameId(LongFilter gameId) {
        this.gameId = gameId;
    }

    public LongFilter getCoinId() {
        return coinId;
    }

    public void setCoinId(LongFilter coinId) {
        this.coinId = coinId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final GameResultCriteria that = (GameResultCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(gameTime, that.gameTime) &&
            Objects.equals(winnersNos, that.winnersNos) &&
            Objects.equals(remarks, that.remarks) &&
            Objects.equals(gameId, that.gameId) &&
            Objects.equals(coinId, that.coinId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        gameTime,
        winnersNos,
        remarks,
        gameId,
        coinId
        );
    }

    @Override
    public String toString() {
        return "GameResultCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (gameTime != null ? "gameTime=" + gameTime + ", " : "") +
                (winnersNos != null ? "winnersNos=" + winnersNos + ", " : "") +
                (remarks != null ? "remarks=" + remarks + ", " : "") +
                (gameId != null ? "gameId=" + gameId + ", " : "") +
                (coinId != null ? "coinId=" + coinId + ", " : "") +
            "}";
    }

}
