package com.jas.playwin.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.jas.playwin.domain.Game} entity. This class is used
 * in {@link com.jas.playwin.web.rest.GameResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /games?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class GameCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter gameName;

    private StringFilter description;

    private InstantFilter startTime;

    private InstantFilter endTime;

    private IntegerFilter interval;

    private IntegerFilter numberRangeFrom;

    private IntegerFilter numberRangeTo;

    private BooleanFilter isEnable;

    private LongFilter coinId;

    private LongFilter userId;

    private LongFilter bidId;

    public GameCriteria(){
    }

    public GameCriteria(GameCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.gameName = other.gameName == null ? null : other.gameName.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.startTime = other.startTime == null ? null : other.startTime.copy();
        this.endTime = other.endTime == null ? null : other.endTime.copy();
        this.interval = other.interval == null ? null : other.interval.copy();
        this.numberRangeFrom = other.numberRangeFrom == null ? null : other.numberRangeFrom.copy();
        this.numberRangeTo = other.numberRangeTo == null ? null : other.numberRangeTo.copy();
        this.isEnable = other.isEnable == null ? null : other.isEnable.copy();
        this.coinId = other.coinId == null ? null : other.coinId.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.bidId = other.bidId == null ? null : other.bidId.copy();
    }

    @Override
    public GameCriteria copy() {
        return new GameCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getGameName() {
        return gameName;
    }

    public void setGameName(StringFilter gameName) {
        this.gameName = gameName;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public InstantFilter getStartTime() {
        return startTime;
    }

    public void setStartTime(InstantFilter startTime) {
        this.startTime = startTime;
    }

    public InstantFilter getEndTime() {
        return endTime;
    }

    public void setEndTime(InstantFilter endTime) {
        this.endTime = endTime;
    }

    public IntegerFilter getInterval() {
        return interval;
    }

    public void setInterval(IntegerFilter interval) {
        this.interval = interval;
    }

    public IntegerFilter getNumberRangeFrom() {
        return numberRangeFrom;
    }

    public void setNumberRangeFrom(IntegerFilter numberRangeFrom) {
        this.numberRangeFrom = numberRangeFrom;
    }

    public IntegerFilter getNumberRangeTo() {
        return numberRangeTo;
    }

    public void setNumberRangeTo(IntegerFilter numberRangeTo) {
        this.numberRangeTo = numberRangeTo;
    }

    public BooleanFilter getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(BooleanFilter isEnable) {
        this.isEnable = isEnable;
    }

    public LongFilter getCoinId() {
        return coinId;
    }

    public void setCoinId(LongFilter coinId) {
        this.coinId = coinId;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getBidId() {
        return bidId;
    }

    public void setBidId(LongFilter bidId) {
        this.bidId = bidId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final GameCriteria that = (GameCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(gameName, that.gameName) &&
            Objects.equals(description, that.description) &&
            Objects.equals(startTime, that.startTime) &&
            Objects.equals(endTime, that.endTime) &&
            Objects.equals(interval, that.interval) &&
            Objects.equals(numberRangeFrom, that.numberRangeFrom) &&
            Objects.equals(numberRangeTo, that.numberRangeTo) &&
            Objects.equals(isEnable, that.isEnable) &&
            Objects.equals(coinId, that.coinId) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(bidId, that.bidId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        gameName,
        description,
        startTime,
        endTime,
        interval,
        numberRangeFrom,
        numberRangeTo,
        isEnable,
        coinId,
        userId,
        bidId
        );
    }

    @Override
    public String toString() {
        return "GameCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (gameName != null ? "gameName=" + gameName + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (startTime != null ? "startTime=" + startTime + ", " : "") +
                (endTime != null ? "endTime=" + endTime + ", " : "") +
                (interval != null ? "interval=" + interval + ", " : "") +
                (numberRangeFrom != null ? "numberRangeFrom=" + numberRangeFrom + ", " : "") +
                (numberRangeTo != null ? "numberRangeTo=" + numberRangeTo + ", " : "") +
                (isEnable != null ? "isEnable=" + isEnable + ", " : "") +
                (coinId != null ? "coinId=" + coinId + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (bidId != null ? "bidId=" + bidId + ", " : "") +
            "}";
    }

}
