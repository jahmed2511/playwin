package com.jas.playwin.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.jas.playwin.domain.UserPref} entity. This class is used
 * in {@link com.jas.playwin.web.rest.UserPrefResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /user-prefs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class UserPrefCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter distributorShare;

    private BooleanFilter isSpecificPrice;

    private StringFilter mobileNo;

    private LongFilter userId;

    public UserPrefCriteria(){
    }

    public UserPrefCriteria(UserPrefCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.distributorShare = other.distributorShare == null ? null : other.distributorShare.copy();
        this.isSpecificPrice = other.isSpecificPrice == null ? null : other.isSpecificPrice.copy();
        this.mobileNo = other.mobileNo == null ? null : other.mobileNo.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
    }

    @Override
    public UserPrefCriteria copy() {
        return new UserPrefCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getDistributorShare() {
        return distributorShare;
    }

    public void setDistributorShare(IntegerFilter distributorShare) {
        this.distributorShare = distributorShare;
    }

    public BooleanFilter getIsSpecificPrice() {
        return isSpecificPrice;
    }

    public void setIsSpecificPrice(BooleanFilter isSpecificPrice) {
        this.isSpecificPrice = isSpecificPrice;
    }

    public StringFilter getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(StringFilter mobileNo) {
        this.mobileNo = mobileNo;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UserPrefCriteria that = (UserPrefCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(distributorShare, that.distributorShare) &&
            Objects.equals(isSpecificPrice, that.isSpecificPrice) &&
            Objects.equals(mobileNo, that.mobileNo) &&
            Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        distributorShare,
        isSpecificPrice,
        mobileNo,
        userId
        );
    }

    @Override
    public String toString() {
        return "UserPrefCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (distributorShare != null ? "distributorShare=" + distributorShare + ", " : "") +
                (isSpecificPrice != null ? "isSpecificPrice=" + isSpecificPrice + ", " : "") +
                (mobileNo != null ? "mobileNo=" + mobileNo + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
            "}";
    }

}
