package com.jas.playwin.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.jas.playwin.domain.SmsConfig} entity. This class is used
 * in {@link com.jas.playwin.web.rest.SmsConfigResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /sms-configs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SmsConfigCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter smsTimeSlots;

    private StringFilter coins;

    private InstantFilter expiryDate;

    private StringFilter description;

    private StringFilter mobileNo;

    private LongFilter userId;

    private LongFilter gameId;

    public SmsConfigCriteria(){
    }

    public SmsConfigCriteria(SmsConfigCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.smsTimeSlots = other.smsTimeSlots == null ? null : other.smsTimeSlots.copy();
        this.coins = other.coins == null ? null : other.coins.copy();
        this.expiryDate = other.expiryDate == null ? null : other.expiryDate.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.mobileNo = other.mobileNo == null ? null : other.mobileNo.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.gameId = other.gameId == null ? null : other.gameId.copy();
    }

    @Override
    public SmsConfigCriteria copy() {
        return new SmsConfigCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getSmsTimeSlots() {
        return smsTimeSlots;
    }

    public void setSmsTimeSlots(StringFilter smsTimeSlots) {
        this.smsTimeSlots = smsTimeSlots;
    }

    public StringFilter getCoins() {
        return coins;
    }

    public void setCoins(StringFilter coins) {
        this.coins = coins;
    }

    public InstantFilter getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(InstantFilter expiryDate) {
        this.expiryDate = expiryDate;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public StringFilter getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(StringFilter mobileNo) {
        this.mobileNo = mobileNo;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getGameId() {
        return gameId;
    }

    public void setGameId(LongFilter gameId) {
        this.gameId = gameId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SmsConfigCriteria that = (SmsConfigCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(smsTimeSlots, that.smsTimeSlots) &&
            Objects.equals(coins, that.coins) &&
            Objects.equals(expiryDate, that.expiryDate) &&
            Objects.equals(description, that.description) &&
            Objects.equals(mobileNo, that.mobileNo) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(gameId, that.gameId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        smsTimeSlots,
        coins,
        expiryDate,
        description,
        mobileNo,
        userId,
        gameId
        );
    }

    @Override
    public String toString() {
        return "SmsConfigCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (smsTimeSlots != null ? "smsTimeSlots=" + smsTimeSlots + ", " : "") +
                (coins != null ? "coins=" + coins + ", " : "") +
                (expiryDate != null ? "expiryDate=" + expiryDate + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (mobileNo != null ? "mobileNo=" + mobileNo + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (gameId != null ? "gameId=" + gameId + ", " : "") +
            "}";
    }

}
