package com.jas.playwin.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.jas.playwin.domain.Coin} entity. This class is used
 * in {@link com.jas.playwin.web.rest.CoinResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /coins?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CoinCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter cointType;

    private StringFilter displayName;

    private FloatFilter basePrice;

    private FloatFilter baseWinningPrice;

    private StringFilter description;

    private LongFilter userId;

    private LongFilter gameId;

    private LongFilter bidId;

    public CoinCriteria(){
    }

    public CoinCriteria(CoinCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.cointType = other.cointType == null ? null : other.cointType.copy();
        this.displayName = other.displayName == null ? null : other.displayName.copy();
        this.basePrice = other.basePrice == null ? null : other.basePrice.copy();
        this.baseWinningPrice = other.baseWinningPrice == null ? null : other.baseWinningPrice.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.gameId = other.gameId == null ? null : other.gameId.copy();
        this.bidId = other.bidId == null ? null : other.bidId.copy();
    }

    @Override
    public CoinCriteria copy() {
        return new CoinCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCointType() {
        return cointType;
    }

    public void setCointType(StringFilter cointType) {
        this.cointType = cointType;
    }

    public StringFilter getDisplayName() {
        return displayName;
    }

    public void setDisplayName(StringFilter displayName) {
        this.displayName = displayName;
    }

    public FloatFilter getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(FloatFilter basePrice) {
        this.basePrice = basePrice;
    }

    public FloatFilter getBaseWinningPrice() {
        return baseWinningPrice;
    }

    public void setBaseWinningPrice(FloatFilter baseWinningPrice) {
        this.baseWinningPrice = baseWinningPrice;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getGameId() {
        return gameId;
    }

    public void setGameId(LongFilter gameId) {
        this.gameId = gameId;
    }

    public LongFilter getBidId() {
        return bidId;
    }

    public void setBidId(LongFilter bidId) {
        this.bidId = bidId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CoinCriteria that = (CoinCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(cointType, that.cointType) &&
            Objects.equals(displayName, that.displayName) &&
            Objects.equals(basePrice, that.basePrice) &&
            Objects.equals(baseWinningPrice, that.baseWinningPrice) &&
            Objects.equals(description, that.description) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(gameId, that.gameId) &&
            Objects.equals(bidId, that.bidId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        cointType,
        displayName,
        basePrice,
        baseWinningPrice,
        description,
        userId,
        gameId,
        bidId
        );
    }

    @Override
    public String toString() {
        return "CoinCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (cointType != null ? "cointType=" + cointType + ", " : "") +
                (displayName != null ? "displayName=" + displayName + ", " : "") +
                (basePrice != null ? "basePrice=" + basePrice + ", " : "") +
                (baseWinningPrice != null ? "baseWinningPrice=" + baseWinningPrice + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (gameId != null ? "gameId=" + gameId + ", " : "") +
                (bidId != null ? "bidId=" + bidId + ", " : "") +
            "}";
    }

}
