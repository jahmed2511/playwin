package com.jas.playwin.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.jas.playwin.domain.Redeem} entity. This class is used
 * in {@link com.jas.playwin.web.rest.RedeemResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /redeems?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RedeemCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter status;

    private FloatFilter amount;

    private StringFilter approvedBy;

    private StringFilter remarks;

    private InstantFilter createdOn;

    private InstantFilter lastModifiedOn;

    private LongFilter userId;

    public RedeemCriteria(){
    }

    public RedeemCriteria(RedeemCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.amount = other.amount == null ? null : other.amount.copy();
        this.approvedBy = other.approvedBy == null ? null : other.approvedBy.copy();
        this.remarks = other.remarks == null ? null : other.remarks.copy();
        this.createdOn = other.createdOn == null ? null : other.createdOn.copy();
        this.lastModifiedOn = other.lastModifiedOn == null ? null : other.lastModifiedOn.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
    }

    @Override
    public RedeemCriteria copy() {
        return new RedeemCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getStatus() {
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public FloatFilter getAmount() {
        return amount;
    }

    public void setAmount(FloatFilter amount) {
        this.amount = amount;
    }

    public StringFilter getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(StringFilter approvedBy) {
        this.approvedBy = approvedBy;
    }

    public StringFilter getRemarks() {
        return remarks;
    }

    public void setRemarks(StringFilter remarks) {
        this.remarks = remarks;
    }

    public InstantFilter getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(InstantFilter createdOn) {
        this.createdOn = createdOn;
    }

    public InstantFilter getLastModifiedOn() {
        return lastModifiedOn;
    }

    public void setLastModifiedOn(InstantFilter lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RedeemCriteria that = (RedeemCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(status, that.status) &&
            Objects.equals(amount, that.amount) &&
            Objects.equals(approvedBy, that.approvedBy) &&
            Objects.equals(remarks, that.remarks) &&
            Objects.equals(createdOn, that.createdOn) &&
            Objects.equals(lastModifiedOn, that.lastModifiedOn) &&
            Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        status,
        amount,
        approvedBy,
        remarks,
        createdOn,
        lastModifiedOn,
        userId
        );
    }

    @Override
    public String toString() {
        return "RedeemCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (amount != null ? "amount=" + amount + ", " : "") +
                (approvedBy != null ? "approvedBy=" + approvedBy + ", " : "") +
                (remarks != null ? "remarks=" + remarks + ", " : "") +
                (createdOn != null ? "createdOn=" + createdOn + ", " : "") +
                (lastModifiedOn != null ? "lastModifiedOn=" + lastModifiedOn + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
            "}";
    }

}
