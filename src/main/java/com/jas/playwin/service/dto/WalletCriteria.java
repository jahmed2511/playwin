package com.jas.playwin.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.jas.playwin.domain.Wallet} entity. This class is used
 * in {@link com.jas.playwin.web.rest.WalletResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /wallets?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class WalletCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private FloatFilter balance;

    private InstantFilter date;

    private FloatFilter credit;

    private FloatFilter debit;

    private StringFilter remarks;

    private LongFilter userId;

    public WalletCriteria(){
    }

    public WalletCriteria(WalletCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.balance = other.balance == null ? null : other.balance.copy();
        this.date = other.date == null ? null : other.date.copy();
        this.credit = other.credit == null ? null : other.credit.copy();
        this.debit = other.debit == null ? null : other.debit.copy();
        this.remarks = other.remarks == null ? null : other.remarks.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
    }

    @Override
    public WalletCriteria copy() {
        return new WalletCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public FloatFilter getBalance() {
        return balance;
    }

    public void setBalance(FloatFilter balance) {
        this.balance = balance;
    }

    public InstantFilter getDate() {
        return date;
    }

    public void setDate(InstantFilter date) {
        this.date = date;
    }

    public FloatFilter getCredit() {
        return credit;
    }

    public void setCredit(FloatFilter credit) {
        this.credit = credit;
    }

    public FloatFilter getDebit() {
        return debit;
    }

    public void setDebit(FloatFilter debit) {
        this.debit = debit;
    }

    public StringFilter getRemarks() {
        return remarks;
    }

    public void setRemarks(StringFilter remarks) {
        this.remarks = remarks;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final WalletCriteria that = (WalletCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(balance, that.balance) &&
            Objects.equals(date, that.date) &&
            Objects.equals(credit, that.credit) &&
            Objects.equals(debit, that.debit) &&
            Objects.equals(remarks, that.remarks) &&
            Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        balance,
        date,
        credit,
        debit,
        remarks,
        userId
        );
    }

    @Override
    public String toString() {
        return "WalletCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (balance != null ? "balance=" + balance + ", " : "") +
                (date != null ? "date=" + date + ", " : "") +
                (credit != null ? "credit=" + credit + ", " : "") +
                (debit != null ? "debit=" + debit + ", " : "") +
                (remarks != null ? "remarks=" + remarks + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
            "}";
    }

}
