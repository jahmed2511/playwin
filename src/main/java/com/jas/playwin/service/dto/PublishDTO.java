package com.jas.playwin.service.dto;

import java.time.Instant;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class PublishDTO {

	private Long id;
	private Instant bidTime;
	private Long coinId;
	private Integer noOfCoins;
	private Long gameId;
	private Integer bidNo;

}
