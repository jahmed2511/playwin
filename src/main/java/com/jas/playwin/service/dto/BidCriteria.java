package com.jas.playwin.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.jas.playwin.domain.Bid} entity. This class is used
 * in {@link com.jas.playwin.web.rest.BidResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /bids?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class BidCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter isWinner;

    private IntegerFilter bidNo;

    private IntegerFilter noOfCoins;

    private InstantFilter bidTime;

    private BooleanFilter isDrowHappan;

    private StringFilter remarks;

    private LongFilter gameId;

    private LongFilter coinId;

    private LongFilter userId;

    public BidCriteria(){
    }

    public BidCriteria(BidCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.isWinner = other.isWinner == null ? null : other.isWinner.copy();
        this.bidNo = other.bidNo == null ? null : other.bidNo.copy();
        this.noOfCoins = other.noOfCoins == null ? null : other.noOfCoins.copy();
        this.bidTime = other.bidTime == null ? null : other.bidTime.copy();
        this.isDrowHappan = other.isDrowHappan == null ? null : other.isDrowHappan.copy();
        this.remarks = other.remarks == null ? null : other.remarks.copy();
        this.gameId = other.gameId == null ? null : other.gameId.copy();
        this.coinId = other.coinId == null ? null : other.coinId.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
    }

    @Override
    public BidCriteria copy() {
        return new BidCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getIsWinner() {
        return isWinner;
    }

    public void setIsWinner(BooleanFilter isWinner) {
        this.isWinner = isWinner;
    }

    public IntegerFilter getBidNo() {
        return bidNo;
    }

    public void setBidNo(IntegerFilter bidNo) {
        this.bidNo = bidNo;
    }

    public IntegerFilter getNoOfCoins() {
        return noOfCoins;
    }

    public void setNoOfCoins(IntegerFilter noOfCoins) {
        this.noOfCoins = noOfCoins;
    }

    public InstantFilter getBidTime() {
        return bidTime;
    }

    public void setBidTime(InstantFilter bidTime) {
        this.bidTime = bidTime;
    }

    public BooleanFilter getIsDrowHappan() {
        return isDrowHappan;
    }

    public void setIsDrowHappan(BooleanFilter isDrowHappan) {
        this.isDrowHappan = isDrowHappan;
    }

    public StringFilter getRemarks() {
        return remarks;
    }

    public void setRemarks(StringFilter remarks) {
        this.remarks = remarks;
    }

    public LongFilter getGameId() {
        return gameId;
    }

    public void setGameId(LongFilter gameId) {
        this.gameId = gameId;
    }

    public LongFilter getCoinId() {
        return coinId;
    }

    public void setCoinId(LongFilter coinId) {
        this.coinId = coinId;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BidCriteria that = (BidCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(isWinner, that.isWinner) &&
            Objects.equals(bidNo, that.bidNo) &&
            Objects.equals(noOfCoins, that.noOfCoins) &&
            Objects.equals(bidTime, that.bidTime) &&
            Objects.equals(isDrowHappan, that.isDrowHappan) &&
            Objects.equals(remarks, that.remarks) &&
            Objects.equals(gameId, that.gameId) &&
            Objects.equals(coinId, that.coinId) &&
            Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        isWinner,
        bidNo,
        noOfCoins,
        bidTime,
        isDrowHappan,
        remarks,
        gameId,
        coinId,
        userId
        );
    }

    @Override
    public String toString() {
        return "BidCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (isWinner != null ? "isWinner=" + isWinner + ", " : "") +
                (bidNo != null ? "bidNo=" + bidNo + ", " : "") +
                (noOfCoins != null ? "noOfCoins=" + noOfCoins + ", " : "") +
                (bidTime != null ? "bidTime=" + bidTime + ", " : "") +
                (isDrowHappan != null ? "isDrowHappan=" + isDrowHappan + ", " : "") +
                (remarks != null ? "remarks=" + remarks + ", " : "") +
                (gameId != null ? "gameId=" + gameId + ", " : "") +
                (coinId != null ? "coinId=" + coinId + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
            "}";
    }

}
