package com.jas.playwin.service.dto;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The Class BidDTO.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BidDTO {

	private static final String N_A = "N/A";
	/** The status. */
	private String status;

	/** The date. */
	private Instant date;

	/** The game name. */
	private String gameName;
	
	private int	totalCoins;

	/** The coins. */
	private List<CoinDTO> coinDtos;

	/** The total price. */
	private String totalPrice = N_A;

	/** The total winning price. */
	private String totalWinningPrice = N_A;

	/** The total profit. */
	private String totalProfit=N_A;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Instant getDate() {
		return date;
	}

	public void setDate(Instant date) {
		this.date = date;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public List<CoinDTO> getCoins() {
		return coinDtos;
	}

	public void setCoins(List<CoinDTO> coins) {
		this.coinDtos = coins;
	}

	public String getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getTotalWinningPrice() {
		return totalWinningPrice;
	}

	public void setTotalWinningPrice(String totalWinningPrice) {
		this.totalWinningPrice = totalWinningPrice;
	}

	public String getTotalProfit() {
		return totalProfit;
	}

	public void setTotalProfit(String totalProfit) {
		this.totalProfit = totalProfit;
	}

	public void addCoinDTO(CoinDTO coinDTO) {
		if (this.coinDtos == null) {
			this.coinDtos = new ArrayList<CoinDTO>();
		}
		this.coinDtos.add(coinDTO);
	}

	/**
	 * @return the totalCoins
	 */
	public int getTotalCoins() {
		return totalCoins;
	}

	/**
	 * @param totalCoins the totalCoins to set
	 */
	public void setTotalCoins(int totalCoins) {
		this.totalCoins = totalCoins;
	}

	/**
	 * @return the coinDtos
	 */
	public List<CoinDTO> getCoinDtos() {
		return coinDtos;
	}

	/**
	 * @param coinDtos the coinDtos to set
	 */
	public void setCoinDtos(List<CoinDTO> coinDtos) {
		this.coinDtos = coinDtos;
	}

	

}
