package com.jas.playwin.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The Class CoinDTO.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CoinDTO {
	
	
	private static final String N_A = "N/A";
	private boolean notBided;
	
	private String status;
	
	/** The price. */
	private String price;
	
	/** The win price. */
	private String winPrice;
	
	/** The no of coin. */
	private int noOfCoin;
	
	/** The bid no. */
	private String bidNo;
	
	/** The total price. */
	private String totalPrice = N_A;
	
	/** The total winning price. */
	private String totalWinningPrice = N_A;
	
	/** The total profit. */
	private String totalProfit = N_A;
	
	private String displayName;

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getWinPrice() {
		return winPrice;
	}

	public void setWinPrice(String winPrice) {
		this.winPrice = winPrice;
	}

	public int getNoOfCoin() {
		return noOfCoin;
	}

	public void setNoOfCoin(int noOfCoin) {
		this.noOfCoin = noOfCoin;
	}

	public String getBidNo() {
		return bidNo;
	}

	public void setBidNo(String bidNo) {
		this.bidNo = bidNo;
	}

	public String getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getTotalWinningPrice() {
		return totalWinningPrice;
	}

	public void setTotalWinningPrice(String totalWinningPrice) {
		this.totalWinningPrice = totalWinningPrice;
	}

	public String getTotalProfit() {
		return totalProfit;
	}

	public void setTotalProfit(String totalProfit) {
		this.totalProfit = totalProfit;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isNotBided() {
		return notBided;
	}

	public void setNotBided(boolean notBided) {
		this.notBided = notBided;
	}
	

}
