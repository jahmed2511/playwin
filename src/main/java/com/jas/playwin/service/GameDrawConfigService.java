package com.jas.playwin.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.jas.playwin.domain.GameDrawConfig;

public interface GameDrawConfigService {
	/**
	 * Save a cronScheduler.
	 *
	 * @param cronScheduler the entity to save.
	 * @return the persisted entity.
	 */
	GameDrawConfig save(GameDrawConfig gameDrawConfig);

	/**
	 * Get all the cronSchedulers.
	 *
	 * @return the list of entities.
	 */
	List<GameDrawConfig> findAll();

	/**
	 * Get the "id" cronScheduler.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	Optional<GameDrawConfig> findOne(Long id);

	/**
	 * Delete the "id" cronScheduler.
	 *
	 * @param id the id of the entity.
	 */
	void delete(Long id);

	/**
	 * Search for the cronScheduler corresponding to the query.
	 *
	 * @param query the query of the search.
	 * 
	 * @return the list of entities.
	 */
	Page<GameDrawConfig> search(String query, Pageable pageable);

	void deleteByGameId(Long gameId);

	Optional<GameDrawConfig> findByGameId(Long id);
	
	List<GameDrawConfig> findByGameIdAndCoinId(Long gameId,Long coinId);
	
	

	
}
