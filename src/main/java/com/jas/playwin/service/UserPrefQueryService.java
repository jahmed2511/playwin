package com.jas.playwin.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.jas.playwin.domain.UserPref;
import com.jas.playwin.domain.*; // for static metamodels
import com.jas.playwin.repository.UserPrefRepository;
import com.jas.playwin.repository.search.UserPrefSearchRepository;
import com.jas.playwin.service.dto.UserPrefCriteria;

/**
 * Service for executing complex queries for {@link UserPref} entities in the database.
 * The main input is a {@link UserPrefCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link UserPref} or a {@link Page} of {@link UserPref} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class UserPrefQueryService extends QueryService<UserPref> {

    private final Logger log = LoggerFactory.getLogger(UserPrefQueryService.class);

    private final UserPrefRepository userPrefRepository;

    private final UserPrefSearchRepository userPrefSearchRepository;

    public UserPrefQueryService(UserPrefRepository userPrefRepository, UserPrefSearchRepository userPrefSearchRepository) {
        this.userPrefRepository = userPrefRepository;
        this.userPrefSearchRepository = userPrefSearchRepository;
    }

    /**
     * Return a {@link List} of {@link UserPref} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<UserPref> findByCriteria(UserPrefCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<UserPref> specification = createSpecification(criteria);
        return userPrefRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link UserPref} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<UserPref> findByCriteria(UserPrefCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<UserPref> specification = createSpecification(criteria);
        return userPrefRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(UserPrefCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<UserPref> specification = createSpecification(criteria);
        return userPrefRepository.count(specification);
    }

    /**
     * Function to convert UserPrefCriteria to a {@link Specification}.
     */
    private Specification<UserPref> createSpecification(UserPrefCriteria criteria) {
        Specification<UserPref> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), UserPref_.id));
            }
            if (criteria.getDistributorShare() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDistributorShare(), UserPref_.distributorShare));
            }
            if (criteria.getIsSpecificPrice() != null) {
                specification = specification.and(buildSpecification(criteria.getIsSpecificPrice(), UserPref_.isSpecificPrice));
            }
            if (criteria.getMobileNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMobileNo(), UserPref_.mobileNo));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(UserPref_.user, JoinType.LEFT).get(User_.id)));
            }
        }
        return specification;
    }
}
