package com.jas.playwin.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.jas.playwin.domain.Wallet;
import com.jas.playwin.domain.*; // for static metamodels
import com.jas.playwin.repository.WalletRepository;
import com.jas.playwin.repository.search.WalletSearchRepository;
import com.jas.playwin.service.dto.WalletCriteria;

/**
 * Service for executing complex queries for {@link Wallet} entities in the database.
 * The main input is a {@link WalletCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Wallet} or a {@link Page} of {@link Wallet} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class WalletQueryService extends QueryService<Wallet> {

    private final Logger log = LoggerFactory.getLogger(WalletQueryService.class);

    private final WalletRepository walletRepository;

    private final WalletSearchRepository walletSearchRepository;

    public WalletQueryService(WalletRepository walletRepository, WalletSearchRepository walletSearchRepository) {
        this.walletRepository = walletRepository;
        this.walletSearchRepository = walletSearchRepository;
    }

    /**
     * Return a {@link List} of {@link Wallet} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Wallet> findByCriteria(WalletCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Wallet> specification = createSpecification(criteria);
        return walletRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Wallet} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Wallet> findByCriteria(WalletCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Wallet> specification = createSpecification(criteria);
        return walletRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(WalletCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Wallet> specification = createSpecification(criteria);
        return walletRepository.count(specification);
    }

    /**
     * Function to convert WalletCriteria to a {@link Specification}.
     */
    private Specification<Wallet> createSpecification(WalletCriteria criteria) {
        Specification<Wallet> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Wallet_.id));
            }
            if (criteria.getBalance() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBalance(), Wallet_.balance));
            }
            if (criteria.getDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDate(), Wallet_.date));
            }
            if (criteria.getCredit() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCredit(), Wallet_.credit));
            }
            if (criteria.getDebit() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDebit(), Wallet_.debit));
            }
            if (criteria.getRemarks() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRemarks(), Wallet_.remarks));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(Wallet_.user, JoinType.LEFT).get(User_.id)));
            }
        }
        return specification;
    }
}
