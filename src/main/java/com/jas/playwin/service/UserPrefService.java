package com.jas.playwin.service;

import com.jas.playwin.domain.UserPref;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link UserPref}.
 */
public interface UserPrefService {

    /**
     * Save a userPref.
     *
     * @param userPref the entity to save.
     * @return the persisted entity.
     */
    UserPref save(UserPref userPref);

    /**
     * Get all the userPrefs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserPref> findAll(Pageable pageable);


    /**
     * Get the "id" userPref.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserPref> findOne(Long id);

    /**
     * Delete the "id" userPref.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the userPref corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserPref> search(String query, Pageable pageable);
}
