package com.jas.playwin.service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

import com.jas.playwin.domain.Game;

public interface JobService {

	Map<Long, ScheduledFuture<?>> activeJobs = new HashMap<>();

	boolean addJob(Game game);

	void updateJob(Game game);

	boolean deleteJob(Game game);

}
