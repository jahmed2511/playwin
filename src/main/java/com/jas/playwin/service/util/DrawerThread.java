package com.jas.playwin.service.util;

import java.time.Instant;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jas.playwin.domain.Game;
import com.jas.playwin.service.impl.GameDrawerService;


public class DrawerThread implements Runnable {


	private final Logger log = LoggerFactory.getLogger(DrawerThread.class);

	private Game game;
	private Instant drawTime;
	private GameDrawerService drawerService;

	public DrawerThread(Game game, Instant drawTime, GameDrawerService drawerHelper) {
		super();
		this.game = game;
		this.drawTime = drawTime;
		this.drawerService = drawerHelper;
	}



	@Override
	public void run() {
		log.info("......game drwer.....");
		drawerService.drawGame(game,drawTime);
		drawerService.sendEmail();
		drawerService.sendSMS();

	}


}