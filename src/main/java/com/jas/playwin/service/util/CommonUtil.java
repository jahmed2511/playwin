package com.jas.playwin.service.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;

public class CommonUtil {

	private static final String ASIA_KOLKATA = "Asia/Kolkata";
	public static int getRandom(long long1, long long2) {
		return (int) ((int) (Math.random() * (long2 + 1 - long1)) + long1);
	}
	public static Instant roundSecondAndMillis(Instant drawTime) {
		if (null == drawTime) {
			drawTime = Instant.now().atZone(ZoneId.of("Asia/Kolkata")).toInstant(); // need to change round millisecond
		} 
		return drawTime.atZone(ZoneId.of(ASIA_KOLKATA)).with(ChronoField.SECOND_OF_MINUTE,0)
				.with(ChronoField.MILLI_OF_SECOND,0)
				.with(ChronoField.NANO_OF_SECOND,0).toInstant();
	}
	public static Instant currentDateTime() {
		return Instant.now().atZone(ZoneId.of("Asia/Kolkata")).toInstant();
	}
	public static LocalDate currentDate(Instant drowTime) {
		// TODO Auto-generated method stub
		return LocalDate.ofInstant(drowTime, ZoneId.of(ASIA_KOLKATA));
	}
	public static String getCurrTimeStr(Instant drowTime) {
		ZonedDateTime now= drowTime.atZone(ZoneId.of(ASIA_KOLKATA));
		return now.getHour()+" : "+now.getMinute();
	}
	

}
