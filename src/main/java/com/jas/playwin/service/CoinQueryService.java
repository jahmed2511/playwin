package com.jas.playwin.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.jas.playwin.domain.Coin;
import com.jas.playwin.domain.*; // for static metamodels
import com.jas.playwin.repository.CoinRepository;
import com.jas.playwin.repository.search.CoinSearchRepository;
import com.jas.playwin.service.dto.CoinCriteria;

/**
 * Service for executing complex queries for {@link Coin} entities in the database.
 * The main input is a {@link CoinCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Coin} or a {@link Page} of {@link Coin} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CoinQueryService extends QueryService<Coin> {

    private final Logger log = LoggerFactory.getLogger(CoinQueryService.class);

    private final CoinRepository coinRepository;

    private final CoinSearchRepository coinSearchRepository;

    public CoinQueryService(CoinRepository coinRepository, CoinSearchRepository coinSearchRepository) {
        this.coinRepository = coinRepository;
        this.coinSearchRepository = coinSearchRepository;
    }

    /**
     * Return a {@link List} of {@link Coin} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Coin> findByCriteria(CoinCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Coin> specification = createSpecification(criteria);
        return coinRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Coin} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Coin> findByCriteria(CoinCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Coin> specification = createSpecification(criteria);
        return coinRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CoinCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Coin> specification = createSpecification(criteria);
        return coinRepository.count(specification);
    }

    /**
     * Function to convert CoinCriteria to a {@link Specification}.
     */
    private Specification<Coin> createSpecification(CoinCriteria criteria) {
        Specification<Coin> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Coin_.id));
            }
            if (criteria.getCointType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCointType(), Coin_.cointType));
            }
            if (criteria.getDisplayName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDisplayName(), Coin_.displayName));
            }
            if (criteria.getBasePrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBasePrice(), Coin_.basePrice));
            }
            if (criteria.getBaseWinningPrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBaseWinningPrice(), Coin_.baseWinningPrice));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Coin_.description));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(Coin_.user, JoinType.LEFT).get(User_.id)));
            }
            if (criteria.getGameId() != null) {
                specification = specification.and(buildSpecification(criteria.getGameId(),
                    root -> root.join(Coin_.game, JoinType.LEFT).get(Game_.id)));
            }
//            if (criteria.getBidId() != null) {
//                specification = specification.and(buildSpecification(criteria.getBidId(),
//                    root -> root.join(Coin_.bids, JoinType.LEFT).get(Bid_.id)));
//            }
        }
        return specification;
    }
}
