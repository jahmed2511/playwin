package com.jas.playwin.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.jas.playwin.domain.Coin;

/**
 * Service Interface for managing {@link Coin}.
 */
public interface CoinService {

	/**
	 * Save a coin.
	 *
	 * @param coin the entity to save.
	 * @return the persisted entity.
	 */
	Coin save(Coin coin);

	/**
	 * Get all the coins.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	Page<Coin> findAll(Pageable pageable);

	/**
	 * Get the "id" coin.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	Optional<Coin> findOne(Long id);

	/**
	 * Delete the "id" coin.
	 *
	 * @param id the id of the entity.
	 */
	void delete(Long id);

	/**
	 * Search for the coin corresponding to the query.
	 *
	 * @param query    the query of the search.
	 * 
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	Page<Coin> search(String query, Pageable pageable);

	List<Coin> findByUserIsCurrentUser();

	List<Coin> findByGameId(Long id);
}
