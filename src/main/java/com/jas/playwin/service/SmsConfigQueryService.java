package com.jas.playwin.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.jas.playwin.domain.SmsConfig;
import com.jas.playwin.domain.*; // for static metamodels
import com.jas.playwin.repository.SmsConfigRepository;
import com.jas.playwin.repository.search.SmsConfigSearchRepository;
import com.jas.playwin.service.dto.SmsConfigCriteria;

/**
 * Service for executing complex queries for {@link SmsConfig} entities in the database.
 * The main input is a {@link SmsConfigCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SmsConfig} or a {@link Page} of {@link SmsConfig} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SmsConfigQueryService extends QueryService<SmsConfig> {

    private final Logger log = LoggerFactory.getLogger(SmsConfigQueryService.class);

    private final SmsConfigRepository smsConfigRepository;

    private final SmsConfigSearchRepository smsConfigSearchRepository;

    public SmsConfigQueryService(SmsConfigRepository smsConfigRepository, SmsConfigSearchRepository smsConfigSearchRepository) {
        this.smsConfigRepository = smsConfigRepository;
        this.smsConfigSearchRepository = smsConfigSearchRepository;
    }

    /**
     * Return a {@link List} of {@link SmsConfig} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SmsConfig> findByCriteria(SmsConfigCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<SmsConfig> specification = createSpecification(criteria);
        return smsConfigRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link SmsConfig} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SmsConfig> findByCriteria(SmsConfigCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<SmsConfig> specification = createSpecification(criteria);
        return smsConfigRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SmsConfigCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<SmsConfig> specification = createSpecification(criteria);
        return smsConfigRepository.count(specification);
    }

    /**
     * Function to convert SmsConfigCriteria to a {@link Specification}.
     */
    private Specification<SmsConfig> createSpecification(SmsConfigCriteria criteria) {
        Specification<SmsConfig> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SmsConfig_.id));
            }
            if (criteria.getSmsTimeSlots() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSmsTimeSlots(), SmsConfig_.smsTimeSlots));
            }
            if (criteria.getCoins() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCoins(), SmsConfig_.coins));
            }
            if (criteria.getExpiryDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getExpiryDate(), SmsConfig_.expiryDate));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), SmsConfig_.description));
            }
            if (criteria.getMobileNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMobileNo(), SmsConfig_.mobileNo));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(SmsConfig_.user, JoinType.LEFT).get(User_.id)));
            }
            if (criteria.getGameId() != null) {
                specification = specification.and(buildSpecification(criteria.getGameId(),
                    root -> root.join(SmsConfig_.game, JoinType.LEFT).get(Game_.id)));
            }
        }
        return specification;
    }
}
