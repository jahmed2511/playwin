package com.jas.playwin.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.jas.playwin.domain.GameResult;
import com.jas.playwin.domain.*; // for static metamodels
import com.jas.playwin.repository.GameResultRepository;
import com.jas.playwin.repository.search.GameResultSearchRepository;
import com.jas.playwin.service.dto.GameResultCriteria;

/**
 * Service for executing complex queries for {@link GameResult} entities in the database.
 * The main input is a {@link GameResultCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link GameResult} or a {@link Page} of {@link GameResult} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class GameResultQueryService extends QueryService<GameResult> {

    private final Logger log = LoggerFactory.getLogger(GameResultQueryService.class);

    private final GameResultRepository gameResultRepository;

    private final GameResultSearchRepository gameResultSearchRepository;

    public GameResultQueryService(GameResultRepository gameResultRepository, GameResultSearchRepository gameResultSearchRepository) {
        this.gameResultRepository = gameResultRepository;
        this.gameResultSearchRepository = gameResultSearchRepository;
    }

    /**
     * Return a {@link List} of {@link GameResult} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<GameResult> findByCriteria(GameResultCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<GameResult> specification = createSpecification(criteria);
        return gameResultRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link GameResult} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<GameResult> findByCriteria(GameResultCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<GameResult> specification = createSpecification(criteria);
        return gameResultRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(GameResultCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<GameResult> specification = createSpecification(criteria);
        return gameResultRepository.count(specification);
    }

    /**
     * Function to convert GameResultCriteria to a {@link Specification}.
     */
    private Specification<GameResult> createSpecification(GameResultCriteria criteria) {
        Specification<GameResult> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), GameResult_.id));
            }
            if (criteria.getGameTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGameTime(), GameResult_.gameTime));
            }
            if (criteria.getWinnersNos() != null) {
                specification = specification.and(buildStringSpecification(criteria.getWinnersNos(), GameResult_.winnersNos));
            }
            if (criteria.getRemarks() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRemarks(), GameResult_.remarks));
            }
            if (criteria.getGameId() != null) {
                specification = specification.and(buildSpecification(criteria.getGameId(),
                    root -> root.join(GameResult_.game, JoinType.LEFT).get(Game_.id)));
            }
            if (criteria.getCoinId() != null) {
                specification = specification.and(buildSpecification(criteria.getCoinId(),
                    root -> root.join(GameResult_.coin, JoinType.LEFT).get(Coin_.id)));
            }
        }
        return specification;
    }
}
