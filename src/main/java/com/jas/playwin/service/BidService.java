package com.jas.playwin.service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.jas.playwin.domain.Bid;
import com.jas.playwin.service.dto.BidDTO;

// TODO: Auto-generated Javadoc
/**
 * Service Interface for managing {@link Bid}.
 */
public interface BidService {

	/**
	 * Save a bid.
	 *
	 * @param bid the entity to save.
	 * @return the persisted entity.
	 */
	Bid save(Bid bid);

	/**
	 * Get all the bids.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	Page<Bid> findAll(Pageable pageable);


	/**
	 * Get the "id" bid.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	Optional<Bid> findOne(Long id);

	/**
	 * Delete the "id" bid.
	 *
	 * @param id the id of the entity.
	 */
	void delete(Long id);

	/**
	 * Search for the bid corresponding to the query.
	 *
	 * @param query the query of the search.
	 * 
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	Page<Bid> search(String query, Pageable pageable);

	List<Bid> findAllByGameIdAndIsDrowHappan(Long gameId, boolean isDrowHappan, Instant bidTime);

	/**
	 * Find by current user and undrow.
	 *
	 * @return the list
	 */
	List<Bid> findByCurrentUserAndUndrow();

	List<BidDTO> getByBidReport(String login);


	List<Bid> getBidForPublish();
	
	Bid findDrawNumberWithMinimumBid(Long gameId, Long coinId, boolean isDrawHappen, Instant bidTime);
	
	Bid findDrawNumberWithMaximumBid(Long gameId, Long coinId, boolean isDrawHappen, Instant bidTime);
}

