package com.jas.playwin.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.jas.playwin.domain.Bid;
import com.jas.playwin.domain.*; // for static metamodels
import com.jas.playwin.repository.BidRepository;
import com.jas.playwin.repository.search.BidSearchRepository;
import com.jas.playwin.service.dto.BidCriteria;

/**
 * Service for executing complex queries for {@link Bid} entities in the database.
 * The main input is a {@link BidCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Bid} or a {@link Page} of {@link Bid} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BidQueryService extends QueryService<Bid> {

    private final Logger log = LoggerFactory.getLogger(BidQueryService.class);

    private final BidRepository bidRepository;

    private final BidSearchRepository bidSearchRepository;

    public BidQueryService(BidRepository bidRepository, BidSearchRepository bidSearchRepository) {
        this.bidRepository = bidRepository;
        this.bidSearchRepository = bidSearchRepository;
    }

    /**
     * Return a {@link List} of {@link Bid} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Bid> findByCriteria(BidCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Bid> specification = createSpecification(criteria);
        return bidRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Bid} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Bid> findByCriteria(BidCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Bid> specification = createSpecification(criteria);
        return bidRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BidCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Bid> specification = createSpecification(criteria);
        return bidRepository.count(specification);
    }

    /**
     * Function to convert BidCriteria to a {@link Specification}.
     */
    private Specification<Bid> createSpecification(BidCriteria criteria) {
        Specification<Bid> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Bid_.id));
            }
            if (criteria.getIsWinner() != null) {
                specification = specification.and(buildSpecification(criteria.getIsWinner(), Bid_.isWinner));
            }
            if (criteria.getBidNo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBidNo(), Bid_.bidNo));
            }
            if (criteria.getNoOfCoins() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNoOfCoins(), Bid_.noOfCoins));
            }
            if (criteria.getBidTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBidTime(), Bid_.bidTime));
            }
            if (criteria.getIsDrowHappan() != null) {
                specification = specification.and(buildSpecification(criteria.getIsDrowHappan(), Bid_.isDrowHappan));
            }
            if (criteria.getRemarks() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRemarks(), Bid_.remarks));
            }
            if (criteria.getGameId() != null) {
                specification = specification.and(buildSpecification(criteria.getGameId(),
                    root -> root.join(Bid_.game, JoinType.LEFT).get(Game_.id)));
            }
            if (criteria.getCoinId() != null) {
                specification = specification.and(buildSpecification(criteria.getCoinId(),
                    root -> root.join(Bid_.coin, JoinType.LEFT).get(Coin_.id)));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(Bid_.user, JoinType.LEFT).get(User_.id)));
            }
        }
        return specification;
    }
}
