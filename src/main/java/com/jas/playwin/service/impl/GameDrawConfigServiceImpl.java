package com.jas.playwin.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jas.playwin.domain.GameDrawConfig;
import com.jas.playwin.repository.GameDrawConfigRepository;
import com.jas.playwin.repository.search.GameDrawConfigSearchRepository;
import com.jas.playwin.service.GameDrawConfigService;

@Service
@Transactional
public class GameDrawConfigServiceImpl implements GameDrawConfigService {

	private final GameDrawConfigRepository drawConfigRepository;

	private final GameDrawConfigSearchRepository drawConfigSearchRepository;

	public GameDrawConfigServiceImpl(GameDrawConfigRepository drawConfigRepository,
			GameDrawConfigSearchRepository drawConfigSearchRepository) {
		this.drawConfigRepository = drawConfigRepository;
		this.drawConfigSearchRepository = drawConfigSearchRepository;
	}

	@Override
	public GameDrawConfig save(GameDrawConfig gameDrawConfig) {
		GameDrawConfig drawConfig = drawConfigRepository.save(gameDrawConfig);
		drawConfigSearchRepository.save(gameDrawConfig);
		return drawConfig;
	}

	@Override
	public List<GameDrawConfig> findAll() {
		return drawConfigRepository.findAll();
	}

	@Override
	public Optional<GameDrawConfig> findOne(Long id) {
		return drawConfigRepository.findById(id);
	}

	@Override
	public void delete(Long id) {
		drawConfigRepository.deleteById(id);
		drawConfigSearchRepository.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<GameDrawConfig> search(String query,Pageable pageable) {
		return drawConfigSearchRepository.search(queryStringQuery(query), pageable); 
	}
	
	@Override
	public void deleteByGameId(Long game) {
		// TODO
	}

	@Override
	public Optional<GameDrawConfig> findByGameId(Long id) {
		return drawConfigRepository.findByGameId(id);
	}
	
	public List<GameDrawConfig> findByGameIdAndCoinId(Long gameId,Long coinId){
		return drawConfigRepository.findByGameIdAndCoinId(gameId,coinId);
	}
	;

}
