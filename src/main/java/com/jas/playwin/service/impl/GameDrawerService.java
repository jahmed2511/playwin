package com.jas.playwin.service.impl;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.common.base.Strings;
import com.jas.playwin.config.Constants;
import com.jas.playwin.domain.AgentCommision;
import com.jas.playwin.domain.Authority;
import com.jas.playwin.domain.Bid;
import com.jas.playwin.domain.Coin;
import com.jas.playwin.domain.Game;
import com.jas.playwin.domain.GameDrawConfig;
import com.jas.playwin.domain.GameResult;
import com.jas.playwin.domain.PublishResult;
import com.jas.playwin.domain.User;
import com.jas.playwin.domain.Wallet;
import com.jas.playwin.repository.AgentCommisionRepository;
import com.jas.playwin.repository.PublishResultRepository;
import com.jas.playwin.repository.UserRepository;
import com.jas.playwin.security.AuthoritiesConstants;
import com.jas.playwin.service.BidService;
import com.jas.playwin.service.GameDrawConfigService;
import com.jas.playwin.service.GameResultService;
import com.jas.playwin.service.WalletService;
import com.jas.playwin.service.util.CommonUtil;
import com.jas.playwin.service.util.DrawerThread;

@Service
public class GameDrawerService {

	private final Logger log = LoggerFactory.getLogger(DrawerThread.class);

	@Autowired
	private AgentCommisionRepository commisionRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private GameResultService gameResultService;

	@Autowired
	private BidService bidService;

	@Autowired
	private WalletService walletService;

	@Autowired
	private GameDrawConfigService gameDrawConfigService;

	@Autowired
	private PublishResultRepository publishResultRepository;

	public static ExecutorService executor = Executors.newCachedThreadPool();

	private static Map<String, Integer> parsedCronData = new HashMap<>();

	private static final String MINUTE = "MINUTE";
	private static final String START_HOUR = "START_HOUR";
	private static final String END_HOUR = "END_HOUR";
	private static final String INTERVAL = "INTERVAL";
	private static final String ASIA_KOLKATA = "Asia/Kolkata";

	public Map<String, Integer> drawGame(Game game, Instant gameDrawTime) {

		final Instant drawTime = CommonUtil.roundSecondAndMillis(gameDrawTime);
		Map<String, Integer> coinResultMap = new HashMap<>();
		List<Bid> bidSearch = markBidDrawDoneAndUpadeAgentCommision(game, drawTime);

		game.getCoins().forEach(coin -> {
			int drawNum = drowNumber(game, coin, drawTime);
			coinResultMap.put(coin.getDisplayName(), drawNum);
			publishGameResult(game, drawTime, coin, drawNum);
			updateAllWinnerWallet(drawTime, bidSearch, coin, drawNum);

		});

		return coinResultMap;
	}

	private void updateAllWinnerWallet(Instant drawTime, List<Bid> bidSearch, Coin coin, int drawNum) {
		if (!bidSearch.isEmpty()) {
			List<Bid> winnerList = bidSearch.stream()
					.filter(bid -> bid.getCoin().getId().equals(coin.getId())
							&& bid.getBidNo().equals(Integer.valueOf(String.valueOf(drawNum).substring(0, 1))))
					.collect(Collectors.toList());
			winnerList.forEach(bid -> {
				bidService.save(bid.isWinner(true));
				creditWinningAmountForWinner(drawTime, bid);

			});
		}
	}

	private void updateAgentCommision(Bid bid) {
		Optional<User> agent = userRepository.findOneWithAuthoritiesByLogin(bid.getUser().getCreatedBy());
		if (agent.isPresent() && agent.get().getAuthorities().contains(new Authority(AuthoritiesConstants.AGENT))) {

			Optional<AgentCommision> agentCommision = commisionRepository.commisionExist(bid.getGame().getId(),
					bid.getCoin().getId(), agent.get().getId());
			if (agentCommision.isPresent()) {
				float totatlCommision = (bid.getNoOfCoins() * bid.getCoin().getBasePrice())
						* agentCommision.get().getCommisionPercentage() / 100;

				Wallet agentWallet = walletService.getRecentWalletByUser(agent.get());
				float currentBalance = 0;
				if (agentWallet != null) {
					currentBalance = agentWallet.getBalance();
				}
				Wallet newBalance = new Wallet().balance(currentBalance + totatlCommision).credit(totatlCommision)
						.date(CommonUtil.currentDateTime())
						.remarks(Constants.COMMISION + " agaist (Game : " + bid.getGame().getGameName() +", Coin: "+ bid.getCoin().getDisplayName()+",  total price = "
								+ (bid.getNoOfCoins() * bid.getCoin().getBasePrice()) 
								+ ", userOfBids : "+bid.getUser().getLogin()
								+ ", commison % : "
								+ agentCommision.get().getCommisionPercentage()+")")
						.user(agent.get());
				walletService.save(newBalance);

			}
		}
	}

	private void creditWinningAmountForWinner(Instant drawTime, Bid bid) {
		float toalCreditAmout = bid.getCoin().getBaseWinningPrice() * bid.getNoOfCoins();
		Wallet lastBalance = walletService.getRecentWalletByUser(bid.getUser());
		Wallet wallet = new Wallet();
		wallet.credit(toalCreditAmout).balance(lastBalance.getBalance() + toalCreditAmout)
				.date(CommonUtil.currentDateTime()).user(bid.getUser())
				.setRemarks(getRemarkForWallet(bid.getGame(), drawTime, bid.getCoin()));
		walletService.save(wallet);
	}

	private String getRemarkForWallet(Game game, Instant drawTime, Coin coin) {
		return "credit winning amount of game " + game.getGameName() + " On " + drawTime + " for coin "
				+ coin.getDisplayName();
	}

	private void publishGameResult(Game game, Instant drawTime, Coin coin, int drawNum) {
		String drawNumStr = String.valueOf(drawNum);
		if (drawNumStr.length() == 1) {
			drawNumStr += String.valueOf(CommonUtil.getRandom(0, 9));
		}
		GameResult gameResult = new GameResult().coin(coin).game(game).gameTime(drawTime)
				.remarks(getRemarks(game, drawTime, coin, drawNum)).winnersNos(drawNumStr);
		gameResultService.save(gameResult);
	}

	private String getLog(Game game, Coin coin, int drawNum) {
		return "Current DRAW for " + coin.getDisplayName() + ":" + drawNum + " for Game:" + game.getGameName() + " at "
				+ LocalDateTime.now();
	}

	private String getRemarks(Game game, Instant drawTime, Coin coin, int drawNum) {
		return "Current DRAW for " + coin.getDisplayName() + ":" + drawNum + " for Game:" + game.getGameName() + " at "
				+ drawTime.toString();
	}

	private int drowNumber(Game game, Coin coin, Instant drowTime) {
		int drawNum = 0;
		int min = game.getNumberRangeFrom();
		int max = game.getNumberRangeTo();
		List<PublishResult> publishResultList = publishResultRepository.findByGameIdAndCoinIdAndDateAndTime(
				game.getId(), coin.getId(), CommonUtil.currentDate(drowTime), CommonUtil.getCurrTimeStr(drowTime));
		if (!CollectionUtils.isEmpty(publishResultList)) {
			drawNum = publishResultList.get(0).getPublishNumber();
		} else {
			List<GameDrawConfig> gameDrawConfigList = gameDrawConfigService.findByGameIdAndCoinId(game.getId(),
					coin.getId());
			if (!gameDrawConfigList.isEmpty()) {
				drawNum = drowFromConfig(game, min, max, coin, gameDrawConfigList, drowTime);
			} else {
				drawNum = CommonUtil.getRandom(min, max);
			}
		}

		if (drawNum <= 9) {
			drawNum = Integer.valueOf(String.valueOf(drawNum) + CommonUtil.getRandom(min, max));
		}
		log.info(getLog(game, coin, drawNum));
		return drawNum;
	}

	private List<Bid> markBidDrawDoneAndUpadeAgentCommision(Game game, Instant drowTime) {
		List<Bid> bidSearch = bidService.findAllByGameIdAndIsDrowHappan(game.getId(), false, drowTime);
		if (bidSearch.size() > 0) {
			bidSearch.forEach(bid -> {
				bid.setIsDrowHappan(Boolean.TRUE);
				bidService.save(bid);
				updateAgentCommision(bid);
			});
		}
		return bidSearch;
	}

	private int drowFromConfig(Game game, int min, int max, Coin coin, List<GameDrawConfig> gameDrawConfigList,
			Instant drowTime) {
		int drawNum;
		GameDrawConfig gameDrawConfig = gameDrawConfigList.get(0);
		if (gameDrawConfig.getPriority() == Constants.MIN) {
			// MIN
			Bid bid = bidService.findDrawNumberWithMinimumBid(game.getId(), coin.getId(), false, drowTime);
			drawNum = bid == null ? CommonUtil.getRandom(min, max) : bid.getBidNo();
			log.info("Game config priority is set to MIN. Setted drawNum is:" + drawNum);
		} else if (gameDrawConfig.getPriority() == Constants.MAX) {
			// MAX
			Bid bid = bidService.findDrawNumberWithMaximumBid(game.getId(), coin.getId(), false, drowTime);
			drawNum = bid == null ? CommonUtil.getRandom(min, max) : bid.getBidNo();
			log.info("Game config priority is set to MAX. Setted drawNum is:" + drawNum);
		} else {
			// RANGE
			drawNum = CommonUtil.getRandom(gameDrawConfig.getRangeFrom(), gameDrawConfig.getRangeTo());
			log.info("Game config priority is set to RANGE. Setted drawNum is:" + drawNum);
		}
		return drawNum;
	}

	public void sendEmail() {
		// TODO Code to send email
		log.info("Mail send template...");
	}

	public void sendSMS() {
		// TODO Code to send SMS
		log.info("SMS send template...");
	}

	public void drawGameDowntime(Game game, GameResult gameLastDraw, String cronExp) {
		if (null == gameLastDraw) {
			log.info("Game was not draw at the time of server down : " + game.getGameName());
		} else if (Strings.isNullOrEmpty(cronExp)) {
			log.info("Loaded game has no cron expression to draw game : " + game.getGameName());
		} else {
			Instant gameDrawnTime = gameLastDraw.getGameTime();
			Instant currentTime = CommonUtil
					.roundSecondAndMillis(Instant.now().atZone(ZoneId.of("Asia/Kolkata")).toInstant());
			parseCronExpression(cronExp);
			int interval = parsedCronData.get(INTERVAL);

			// TODO Need to test more for subtract for interval. It seems working fine
			// without subtraction.
			// currentTime = currentTime.minus(Long.valueOf(interval), ChronoUnit.MINUTES);
			if (gameDrawnTime.isBefore(currentTime)) {
				// TODO draw game for interval minus
				int iterations = calculateIterations(currentTime, gameDrawnTime, Integer.valueOf(interval));
				log.info("Total iterations for game : " + game.getGameName() + " is " + iterations);
				for (int iteration = 1; iteration <= iterations; iteration++) {
					try {
						// TODO Implement logic for Night over drawn
						int hour = gameDrawnTime.atZone(ZoneId.of(ASIA_KOLKATA)).getHour();
						// int minute = gameDrawnTime.atZone(ZoneId.of(ASIA_KOLKATA)).getMinute();
						if (hour > parsedCronData.get(END_HOUR) || hour < parsedCronData.get(START_HOUR)) {
							gameDrawnTime = gameDrawnTime.plus(Long.valueOf(interval), ChronoUnit.MINUTES);
							continue;
						}
						gameDrawnTime = gameDrawnTime.plus(Long.valueOf(interval), ChronoUnit.MINUTES);
						executor.execute(new DrawerThread(game, gameDrawnTime, this));
					} catch (Exception e) {
						log.error("Game couldn't be draw for the time : " + gameDrawnTime, e);
					}
				}
			} else {
				log.info("No need to draw game as las drawn time is less than the interval " + interval + " : "
						+ game.getGameName());
			}
		}
	}

	private void parseCronExpression(String cronExp) {
		String[] cronAttribute = cronExp.split(" ");

		String[] minutsNInterval = cronAttribute[1].split("/");
		parsedCronData.put(MINUTE, Strings.isNullOrEmpty(minutsNInterval[0]) ? 0 : Integer.valueOf(minutsNInterval[0]));
		parsedCronData.put(INTERVAL,
				Strings.isNullOrEmpty(minutsNInterval[1]) ? 0 : Integer.valueOf(minutsNInterval[1]));

		String[] hours = cronAttribute[2].split("-");
		parsedCronData.put(START_HOUR, Strings.isNullOrEmpty(hours[0]) ? 0 : Integer.valueOf(hours[0]));
		parsedCronData.put(END_HOUR, Strings.isNullOrEmpty(hours[1]) ? 0 : Integer.valueOf(hours[1]));

	}

	private int calculateIterations(Instant currentTime, Instant gameDrawnTime, int interval) {
		Long diffInMinuts = gameDrawnTime.until(currentTime, ChronoUnit.MINUTES);
		log.info("Diff in minuts for down time drawn " + diffInMinuts);
		return interval <= 0 ? 0 : diffInMinuts.intValue() / interval;
	}

}
