package com.jas.playwin.service.impl;

import com.jas.playwin.service.GameService;
import com.jas.playwin.service.UserService;
import com.jas.playwin.domain.Game;
import com.jas.playwin.domain.User;
import com.jas.playwin.repository.GameRepository;
import com.jas.playwin.repository.search.GameSearchRepository;
import com.jas.playwin.security.SecurityUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Game}.
 */
@Service
@Transactional
public class GameServiceImpl implements GameService {

    private final Logger log = LoggerFactory.getLogger(GameServiceImpl.class);

    private final GameRepository gameRepository;
    private final UserService userService;

    private final GameSearchRepository gameSearchRepository;

    public GameServiceImpl(UserService userService,GameRepository gameRepository, GameSearchRepository gameSearchRepository) {
        this.gameRepository = gameRepository;
        this.gameSearchRepository = gameSearchRepository;
        this.userService=userService;
    }

    /**
     * Save a game.
     *
     * @param game the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Game save(Game game) {
        log.debug("Request to save Game : {}", game);
        Game result = gameRepository.save(game);
        gameSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the games.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Game> findAll(Pageable pageable) {
        log.debug("Request to get all Games");
        return gameRepository.findAll(pageable);
    }

    /**
     * Get all the games with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<Game> findAllWithEagerRelationships(Pageable pageable) {
        return gameRepository.findAllWithEagerRelationships(pageable);
    }
    

    /**
     * Get one game by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Game> findOne(Long id) {
        log.debug("Request to get Game : {}", id);
        return gameRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the game by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Game : {}", id);
        gameRepository.deleteById(id);
        gameSearchRepository.deleteById(id);
    }
    
    

    /**
     * Search for the game corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Game> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Games for query {}", query);
        return gameSearchRepository.search(queryStringQuery(query), pageable);    }

	@Override
	public List<Game> findAllWithCoinEagerRelationships() {
		// TODO Auto-generated method stub
		String currUserName =SecurityUtils.getCurrentUserLogin().get();
        User currUser =userService.getUserWithAuthoritiesByLogin(currUserName).get();
		return gameRepository.findAllWithCoinEagerRelationships().stream().filter(game->{
			return  game.getUsers()!=null && !game.getUsers().contains(currUser);
		}).map(game->{
			return game.users(null);}).collect(Collectors.toList());
	}
}
