package com.jas.playwin.service.impl;

import com.jas.playwin.service.SmsConfigService;
import com.jas.playwin.domain.SmsConfig;
import com.jas.playwin.repository.SmsConfigRepository;
import com.jas.playwin.repository.search.SmsConfigSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link SmsConfig}.
 */
@Service
@Transactional
public class SmsConfigServiceImpl implements SmsConfigService {

    private final Logger log = LoggerFactory.getLogger(SmsConfigServiceImpl.class);

    private final SmsConfigRepository smsConfigRepository;

    private final SmsConfigSearchRepository smsConfigSearchRepository;

    public SmsConfigServiceImpl(SmsConfigRepository smsConfigRepository, SmsConfigSearchRepository smsConfigSearchRepository) {
        this.smsConfigRepository = smsConfigRepository;
        this.smsConfigSearchRepository = smsConfigSearchRepository;
    }

    /**
     * Save a smsConfig.
     *
     * @param smsConfig the entity to save.
     * @return the persisted entity.
     */
    @Override
    public SmsConfig save(SmsConfig smsConfig) {
        log.debug("Request to save SmsConfig : {}", smsConfig);
        SmsConfig result = smsConfigRepository.save(smsConfig);
        smsConfigSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the smsConfigs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SmsConfig> findAll(Pageable pageable) {
        log.debug("Request to get all SmsConfigs");
        return smsConfigRepository.findAll(pageable);
    }


    /**
     * Get one smsConfig by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SmsConfig> findOne(Long id) {
        log.debug("Request to get SmsConfig : {}", id);
        return smsConfigRepository.findById(id);
    }

    /**
     * Delete the smsConfig by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SmsConfig : {}", id);
        smsConfigRepository.deleteById(id);
        smsConfigSearchRepository.deleteById(id);
    }

    /**
     * Search for the smsConfig corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SmsConfig> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SmsConfigs for query {}", query);
        return smsConfigSearchRepository.search(queryStringQuery(query), pageable);    }
}
