package com.jas.playwin.service.impl;

import com.jas.playwin.service.RedeemService;
import com.jas.playwin.domain.Redeem;
import com.jas.playwin.repository.RedeemRepository;
import com.jas.playwin.repository.search.RedeemSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Redeem}.
 */
@Service
@Transactional
public class RedeemServiceImpl implements RedeemService {

    private final Logger log = LoggerFactory.getLogger(RedeemServiceImpl.class);

    private final RedeemRepository redeemRepository;

    private final RedeemSearchRepository redeemSearchRepository;

    public RedeemServiceImpl(RedeemRepository redeemRepository, RedeemSearchRepository redeemSearchRepository) {
        this.redeemRepository = redeemRepository;
        this.redeemSearchRepository = redeemSearchRepository;
    }

    /**
     * Save a redeem.
     *
     * @param redeem the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Redeem save(Redeem redeem) {
        log.debug("Request to save Redeem : {}", redeem);
        Redeem result = redeemRepository.save(redeem);
        redeemSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the redeems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Redeem> findAll(Pageable pageable) {
        log.debug("Request to get all Redeems");
        return redeemRepository.findAll(pageable);
    }


    /**
     * Get one redeem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Redeem> findOne(Long id) {
        log.debug("Request to get Redeem : {}", id);
        return redeemRepository.findById(id);
    }

    /**
     * Delete the redeem by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Redeem : {}", id);
        redeemRepository.deleteById(id);
        redeemSearchRepository.deleteById(id);
    }

    /**
     * Search for the redeem corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Redeem> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Redeems for query {}", query);
        return redeemSearchRepository.search(queryStringQuery(query), pageable);    }
}
