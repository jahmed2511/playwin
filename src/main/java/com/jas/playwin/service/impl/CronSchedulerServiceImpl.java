package com.jas.playwin.service.impl;

import com.jas.playwin.service.CronSchedulerService;
import com.jas.playwin.domain.CronScheduler;
import com.jas.playwin.repository.CronSchedulerRepository;
import com.jas.playwin.repository.search.CronSchedulerSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link CronScheduler}.
 */
@Service
@Transactional
public class CronSchedulerServiceImpl implements CronSchedulerService {

	private final Logger log = LoggerFactory.getLogger(CronSchedulerServiceImpl.class);

	private final CronSchedulerRepository cronSchedulerRepository;

	private final CronSchedulerSearchRepository cronSchedulerSearchRepository;

	public CronSchedulerServiceImpl(CronSchedulerRepository cronSchedulerRepository,
			CronSchedulerSearchRepository cronSchedulerSearchRepository) {
		this.cronSchedulerRepository = cronSchedulerRepository;
		this.cronSchedulerSearchRepository = cronSchedulerSearchRepository;
	}

	/**
	 * Save a cronScheduler.
	 *
	 * @param cronScheduler the entity to save.
	 * @return the persisted entity.
	 */
	@Override
	public CronScheduler save(CronScheduler cronScheduler) {
		log.debug("Request to save CronScheduler : {}", cronScheduler);
		CronScheduler result = cronSchedulerRepository.save(cronScheduler);
		cronSchedulerSearchRepository.save(result);
		return result;
	}

	/**
	 * Get all the cronSchedulers.
	 *
	 * @return the list of entities.
	 */
	@Override
	@Transactional(readOnly = true)
	public List<CronScheduler> findAll() {
		log.debug("Request to get all CronSchedulers");
		return cronSchedulerRepository.findAll();
	}

	/**
	 * Get one cronScheduler by id.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<CronScheduler> findOne(Long id) {
		log.debug("Request to get CronScheduler : {}", id);
		return cronSchedulerRepository.findById(id);
	}

	/**
	 * Delete the cronScheduler by id.
	 *
	 * @param id the id of the entity.
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete CronScheduler : {}", id);
		cronSchedulerRepository.deleteById(id);
		cronSchedulerSearchRepository.deleteById(id);
	}

	/**
	 * Search for the cronScheduler corresponding to the query.
	 *
	 * @param query the query of the search.
	 * @return the list of entities.
	 */
	@Override
	@Transactional(readOnly = true)
	public List<CronScheduler> search(String query) {
		log.debug("Request to search CronSchedulers for query {}", query);
		return StreamSupport.stream(cronSchedulerSearchRepository.search(queryStringQuery(query)).spliterator(), false)
				.collect(Collectors.toList());
	}

	@Override
	public void deleteByGameId(Long gameId) {
		cronSchedulerRepository.deleteByGameId(gameId);
	}
}
