package com.jas.playwin.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jas.playwin.domain.Game;
import com.jas.playwin.domain.GameResult;
import com.jas.playwin.repository.GameResultRepository;
import com.jas.playwin.repository.search.GameResultSearchRepository;
import com.jas.playwin.service.GameResultService;

/**
 * Service Implementation for managing {@link GameResult}.
 */
@Service
@Transactional
public class GameResultServiceImpl implements GameResultService {

	private final Logger log = LoggerFactory.getLogger(GameResultServiceImpl.class);

	private final GameResultRepository gameResultRepository;

	private final GameResultSearchRepository gameResultSearchRepository;

	public GameResultServiceImpl(GameResultRepository gameResultRepository,
			GameResultSearchRepository gameResultSearchRepository) {
		this.gameResultRepository = gameResultRepository;
		this.gameResultSearchRepository = gameResultSearchRepository;
	}

	/**
	 * Save a gameResult.
	 *
	 * @param gameResult the entity to save.
	 * @return the persisted entity.
	 */
	@Override
	public GameResult save(GameResult gameResult) {
		log.debug("Request to save GameResult : {}", gameResult);
		GameResult result = gameResultRepository.save(gameResult);
		gameResultSearchRepository.save(result);
		return result;
	}

	/**
	 * Get all the gameResults.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<GameResult> findAll(Pageable pageable) {
		log.debug("Request to get all GameResults");
		return gameResultRepository.findAll(pageable);
	}

	/**
	 * Get one gameResult by id.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<GameResult> findOne(Long id) {
		log.debug("Request to get GameResult : {}", id);
		return gameResultRepository.findById(id);
	}

	/**
	 * Delete the gameResult by id.
	 *
	 * @param id the id of the entity.
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete GameResult : {}", id);
		gameResultRepository.deleteById(id);
		gameResultSearchRepository.deleteById(id);
	}

	/**
	 * Search for the gameResult corresponding to the query.
	 *
	 * @param query    the query of the search.
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<GameResult> search(String query, Pageable pageable) {
		log.debug("Request to search for a page of GameResults for query {}", query);
		return gameResultSearchRepository.search(queryStringQuery(query), pageable);
	}

	@Override
	public GameResult findLastDraw(Game game) {
		return gameResultRepository.findTopByGameOrderByGameTimeDesc(game);
	}
}
