package com.jas.playwin.service.impl;

import com.jas.playwin.service.CoinService;
import com.jas.playwin.domain.Coin;
import com.jas.playwin.repository.CoinRepository;
import com.jas.playwin.repository.search.CoinSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Coin}.
 */
@Service
@Transactional
public class CoinServiceImpl implements CoinService {

	private final Logger log = LoggerFactory.getLogger(CoinServiceImpl.class);

	private final CoinRepository coinRepository;

	private final CoinSearchRepository coinSearchRepository;

	public CoinServiceImpl(CoinRepository coinRepository, CoinSearchRepository coinSearchRepository) {
		this.coinRepository = coinRepository;
		this.coinSearchRepository = coinSearchRepository;
	}

	/**
	 * Save a coin.
	 *
	 * @param coin the entity to save.
	 * @return the persisted entity.
	 */
	@Override
	public Coin save(Coin coin) {
		log.debug("Request to save Coin : {}", coin);
		Coin result = coinRepository.save(coin);
		coinSearchRepository.save(result);
		return result;
	}

	/**
	 * Get all the coins.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<Coin> findAll(Pageable pageable) {
		log.debug("Request to get all Coins");
		return coinRepository.findAll(pageable);
	}

	/**
	 * Get one coin by id.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<Coin> findOne(Long id) {
		log.debug("Request to get Coin : {}", id);
		return coinRepository.findById(id);
	}

	/**
	 * Delete the coin by id.
	 *
	 * @param id the id of the entity.
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Coin : {}", id);
		coinRepository.deleteById(id);
		coinSearchRepository.deleteById(id);
	}

	/**
	 * Search for the coin corresponding to the query.
	 *
	 * @param query    the query of the search.
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<Coin> search(String query, Pageable pageable) {
		log.debug("Request to search for a page of Coins for query {}", query);
		return coinSearchRepository.search(queryStringQuery(query), pageable);
	}

	@Override
	public List<Coin> findByUserIsCurrentUser() {
		return coinRepository.findByUserIsCurrentUser();
	}

	@Override
	public List<Coin> findByGameId(Long id) {
		return coinRepository.findByGameId(id);
	}
}
