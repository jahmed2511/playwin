package com.jas.playwin.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jas.playwin.domain.Bid;
import com.jas.playwin.domain.Coin;
import com.jas.playwin.domain.Game;
import com.jas.playwin.repository.BidRepository;
import com.jas.playwin.repository.search.BidSearchRepository;
import com.jas.playwin.service.BidService;
import com.jas.playwin.service.dto.BidDTO;
import com.jas.playwin.service.dto.CoinDTO;

/**
 * Service Implementation for managing {@link Bid}.
 */
@Service
@Transactional
public class BidServiceImpl implements BidService {

	private static final String NOT_BIDED = "Not Bided";

	private static final String N_A = "N/A";

	private static final String PARTIAL_WIN = "Partial Win";

	private static final String LOST = "Lost";

	private static final String WIN = "Win";

	private static final String PENDING = "Pending";

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(BidServiceImpl.class);

	/** The bid repository. */
	private final BidRepository bidRepository;

	/** The bid search repository. */
	private final BidSearchRepository bidSearchRepository;

	/**
	 * Instantiates a new bid service impl.
	 *
	 * @param bidRepository       the bid repository
	 * @param bidSearchRepository the bid search repository
	 */
	public BidServiceImpl(BidRepository bidRepository, BidSearchRepository bidSearchRepository) {
		this.bidRepository = bidRepository;
		this.bidSearchRepository = bidSearchRepository;
	}

	/**
	 * Save a bid.
	 *
	 * @param bid the entity to save.
	 * @return the persisted entity.
	 */
	@Override
	public Bid save(Bid bid) {
		log.info("Request to save Bid : {}", bid);
		Bid result = bidRepository.save(bid);
		bidSearchRepository.save(result);
		return result;
	}

	/**
	 * Get all the bids.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<Bid> findAll(Pageable pageable) {
		log.debug("Request to get all Bids");
		return bidRepository.findAll(pageable);
	}

	/**
	 * Get one bid by id.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<Bid> findOne(Long id) {
		log.debug("Request to get Bid : {}", id);
		return bidRepository.findById(id);
	}

	/**
	 * Delete the bid by id.
	 *
	 * @param id the id of the entity.
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Bid : {}", id);
		bidRepository.deleteById(id);
		bidSearchRepository.deleteById(id);
	}

	/**
	 * Search for the bid corresponding to the query.
	 *
	 * @param query    the query of the search.
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<Bid> search(String query, Pageable pageable) {
		log.debug("Request to search for a page of Bids for query {}", query);
		return bidSearchRepository.search(queryStringQuery(query), pageable);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jas.playwin.service.BidService#findByCurrentUserAndUndrow()
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Bid> findByCurrentUserAndUndrow() {
		log.debug("Request to get all Bids undrow bids");
		return bidRepository.findByUserIsCurrentUserAndDrowNotHappen();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.jas.playwin.service.BidService#findAllByGameIdAndIsDrowHappan(java.lang.
	 * Long, boolean, java.time.Instant)
	 */
	@Override
	public List<Bid> findAllByGameIdAndIsDrowHappan(Long gameId, boolean isDrowHappan, Instant bidTime) {
		log.debug("Request to find all by game_id and is_draw_happen {}", gameId, isDrowHappan);

		return bidRepository.findAllByGameIdAndIsDrowHappan(gameId, isDrowHappan, bidTime);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jas.playwin.service.BidService#getByBidReport()
	 */
	@Transactional(readOnly = true)
	public List<BidDTO> getByBidReport(final String login) {
		log.debug("Request to get all Bids undrow bids");
		List<Bid> bidList = bidRepository.findByUserLogin(login);
		Map<Instant, Map<Game, List<Bid>>> bidList1  =bidList.stream().collect(Collectors.groupingBy(Bid::getBidTime,Collectors.groupingBy(Bid::getGame)));
		List<BidDTO> dtos = new ArrayList<BidDTO>();
		for (Entry<Instant, Map<Game, List<Bid>>> bidEntry : bidList1.entrySet()) {
			for (Entry<Game, List<Bid>> currBidEntry : bidEntry.getValue().entrySet()) {
				BidDTO dto = new BidDTO();
				dto.setDate(bidEntry.getKey());
				String status=PENDING;
				List<Long> bidingCoinIdList = new ArrayList<Long>();
				dto.setGameName(currBidEntry.getKey().getGameName());
				for (Bid currBid : currBidEntry.getValue()) {
					CoinDTO coinDTO = setBidDtoData(dto, bidingCoinIdList, currBid);
					
					status = getOverallStatus(status, coinDTO);
					dto.addCoinDTO(coinDTO);
				}
				for(Coin coin : currBidEntry.getKey().getCoins()) {
					if(!bidingCoinIdList.contains(coin.getId())) {
						CoinDTO coinDTO = new CoinDTO();
						coinDTO.setBidNo(N_A);
						coinDTO.setPrice(String.valueOf(coin.getBasePrice()));
						coinDTO.setWinPrice(String.valueOf(coin.getBaseWinningPrice()));
						coinDTO.setDisplayName(coin.getDisplayName()); 
						coinDTO.setNotBided(true);
						coinDTO.setStatus(NOT_BIDED);
						dto.addCoinDTO(coinDTO);
					}
				}
				dto.setCoinDtos(dto.getCoinDtos().stream().sorted(Comparator.comparing(CoinDTO::getDisplayName).thenComparing(CoinDTO::getBidNo))
						.collect(Collectors.toList()));
				dto.setStatus(status);
				dtos.add(dto);
			}
		} 
		
		return dtos.stream().sorted(Comparator.comparing(BidDTO::getDate).reversed()).collect(Collectors.toList());
	}

	private CoinDTO setBidDtoData(BidDTO dto, List<Long> bidingCoinIdList, Bid currBid) {
		CoinDTO coinDTO = new CoinDTO();
		coinDTO.setBidNo(String.valueOf(currBid.getBidNo()));
		coinDTO.setNoOfCoin(currBid.getNoOfCoins());
		dto.setTotalCoins(dto.getTotalCoins()+ currBid.getNoOfCoins());
		coinDTO.setStatus(getStatus(currBid));
		bidingCoinIdList.add(currBid.getCoin().getId());
		coinDTO.setDisplayName(currBid.getCoin().getDisplayName()); 
		coinDTO.setPrice(String.valueOf(currBid.getCoin().getBasePrice()));
		coinDTO.setWinPrice(String.valueOf(currBid.getCoin().getBaseWinningPrice()));
		coinDTO.setTotalPrice(String.valueOf(currBid.getCoin().getBasePrice() * currBid.getNoOfCoins()));
		if(!PENDING.equals(coinDTO.getStatus())) { 
			float dtoTotoalWiningPrice= N_A.equals(dto.getTotalWinningPrice())? 0 : Float.parseFloat(dto.getTotalWinningPrice());
			
			float coinTotoalWiningPrice=currBid.getCoin().getBaseWinningPrice() * currBid.getNoOfCoins();
			if(!currBid.isIsWinner()) {
				coinTotoalWiningPrice=0;
			}
			
			float dtoTotalProfit=N_A.equals(dto.getTotalProfit())? 0 : Float.parseFloat(dto.getTotalProfit());;
			float coinTotalPrice =currBid.getCoin().getBasePrice() * currBid.getNoOfCoins();
			float coinTotalProfit=coinTotoalWiningPrice - coinTotalPrice;
			
			coinDTO.setTotalPrice(String.valueOf(coinTotalPrice));
			
			coinDTO.setTotalWinningPrice(String.valueOf(coinTotoalWiningPrice));
			coinDTO.setTotalProfit(String.valueOf(coinTotalProfit));
			
			dto.setTotalWinningPrice(String.valueOf(dtoTotoalWiningPrice + coinTotoalWiningPrice));
			dto.setTotalProfit(String.valueOf(dtoTotalProfit + coinTotalProfit));
		}
		if(!N_A.equals(coinDTO.getTotalPrice())) {
			if(N_A.equals(dto.getTotalPrice())){
				dto.setTotalPrice("0");
			}
			dto.setTotalPrice(String.valueOf(Float.valueOf(dto.getTotalPrice()) + Float.valueOf(coinDTO.getTotalPrice())));
		}
		return coinDTO;
	}

	private String getStatus(Bid currBid) {
		String status = PENDING;
		if (currBid.isIsDrowHappan()) {
			status = currBid.isIsWinner() ? WIN : LOST;
		}
		return status;
	}
	private String getOverallStatus(String status, CoinDTO coinDTO) {
		if(PENDING.equals(status)) {
			status=coinDTO.getStatus();
		}else if((WIN.equals(status) && coinDTO.getStatus().equals(LOST))
				||LOST.equals(status) && coinDTO.getStatus().equals(WIN)) {
			status=PARTIAL_WIN;
		}else if(LOST.equals(status) && coinDTO.getStatus().equals(LOST)){
			status=LOST;
		}else if(WIN.equals(status) && coinDTO.getStatus().equals(WIN)){
			status=WIN;
		}
		return status;
	}

	@Override
	public List<Bid> getBidForPublish() {
		return bidRepository.findAllForPublishByTime();
	}

	

	public Bid findDrawNumberWithMinimumBid(Long gameId, Long coinId, boolean isDrawHappen, Instant bidTime) {
		return bidRepository.findDrawNumberWithMinimumBid(gameId, coinId, isDrawHappen, bidTime);
	}
	
	@Override
	public Bid findDrawNumberWithMaximumBid(Long gameId, Long coinId, boolean isDrawHappen, Instant bidTime) {
		return bidRepository.findDrawNumberWithMaximumBid(gameId, coinId, isDrawHappen, bidTime);
	}
}
