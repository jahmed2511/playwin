package com.jas.playwin.service.impl;

import com.jas.playwin.service.UserPrefService;
import com.jas.playwin.domain.UserPref;
import com.jas.playwin.repository.UserPrefRepository;
import com.jas.playwin.repository.search.UserPrefSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link UserPref}.
 */
@Service
@Transactional
public class UserPrefServiceImpl implements UserPrefService {

    private final Logger log = LoggerFactory.getLogger(UserPrefServiceImpl.class);

    private final UserPrefRepository userPrefRepository;

    private final UserPrefSearchRepository userPrefSearchRepository;

    public UserPrefServiceImpl(UserPrefRepository userPrefRepository, UserPrefSearchRepository userPrefSearchRepository) {
        this.userPrefRepository = userPrefRepository;
        this.userPrefSearchRepository = userPrefSearchRepository;
    }

    /**
     * Save a userPref.
     *
     * @param userPref the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UserPref save(UserPref userPref) {
        log.debug("Request to save UserPref : {}", userPref);
        UserPref result = userPrefRepository.save(userPref);
        userPrefSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the userPrefs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserPref> findAll(Pageable pageable) {
        log.debug("Request to get all UserPrefs");
        return userPrefRepository.findAll(pageable);
    }


    /**
     * Get one userPref by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserPref> findOne(Long id) {
        log.debug("Request to get UserPref : {}", id);
        return userPrefRepository.findById(id);
    }

    /**
     * Delete the userPref by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserPref : {}", id);
        userPrefRepository.deleteById(id);
        userPrefSearchRepository.deleteById(id);
    }

    /**
     * Search for the userPref corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserPref> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of UserPrefs for query {}", query);
        return userPrefSearchRepository.search(queryStringQuery(query), pageable);    }
}
