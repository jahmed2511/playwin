package com.jas.playwin.service.impl;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.concurrent.ScheduledFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import com.jas.playwin.domain.CronScheduler;
import com.jas.playwin.domain.Game;
import com.jas.playwin.service.CronSchedulerService;
import com.jas.playwin.service.JobService;
import com.jas.playwin.service.util.DrawerThread;
import com.jas.playwin.service.util.DrawerThread;

@Service
public class JobServiceImpl implements JobService {

	private final Logger log = LoggerFactory.getLogger(JobServiceImpl.class);

	@Autowired
	private ThreadPoolTaskScheduler taskScheduler;

	@Autowired
	private CronSchedulerService cronSchedulerService;

	@Autowired
	private GameDrawerService drawerHelper;

	@Override
	public boolean addJob(Game game) {

		LocalDateTime gameStartAt = LocalDateTime.ofInstant(game.getStartTime(), ZoneId.of("Asia/Kolkata"));
		LocalDateTime gameEndAt = LocalDateTime.ofInstant(game.getEndTime(), ZoneId.of("Asia/Kolkata"));

		String cron = "0 " + gameStartAt.getMinute() + "/" + game.getInterval() + " " + gameStartAt.getHour() + "-"
				+ gameEndAt.getHour() + " * * *";
		log.info("Starting cron with CRON EXP:" + cron + " for Game:" + game.getGameName());

		CronTrigger cronTrigger = new CronTrigger(cron);
		DrawerThread drawerThread = new DrawerThread(game, null, drawerHelper);
		ScheduledFuture<?> scheduleAtFixedRate = taskScheduler.schedule(drawerThread, cronTrigger);

		activeJobs.put(game.getId(), scheduleAtFixedRate);
		try {
			CronScheduler cronScheduler = new CronScheduler();
			cronScheduler.setGameId(game.getId());
			cronScheduler.setCronExp(cron);
			cronScheduler.setGame(game);
			cronSchedulerService.save(cronScheduler);
		} catch (Exception e) {
			log.error("Error in saving cron expression : ", e);
		}
		log.info("Cron started for Game:" + game.getGameName());

		return false;
	}

	@Override
	public void updateJob(Game game) {
		if (!game.isIsEnable()) {
			deleteJob(game);
			log.info("Job deactivated/removed for Game:" + game.getGameName());
		} else {
			deleteJob(game);
			addJob(game);
			log.info("Job updated for Game:" + game.getGameName());
		}
	}

	@Override
	public boolean deleteJob(Game game) {
		Long gameId = game.getId();
		if (activeJobs.containsKey(gameId)) {
			ScheduledFuture<?> gameScheduler = activeJobs.get(gameId);
			if (gameScheduler != null) {
				boolean jobCancelResult = gameScheduler.cancel(true);
				if (jobCancelResult) {
					activeJobs.remove(gameId);
					log.info("Job removed from active jobs for game id:" + gameId);
				}

				if (jobCancelResult) {
					cronSchedulerService.deleteByGameId(gameId);
				}
				return jobCancelResult;
			}
		}
		log.info("No Active job for the game id:" + gameId);
		return false;
	}
}
