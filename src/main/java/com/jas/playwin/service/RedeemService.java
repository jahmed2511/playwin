package com.jas.playwin.service;

import com.jas.playwin.domain.Redeem;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Redeem}.
 */
public interface RedeemService {

    /**
     * Save a redeem.
     *
     * @param redeem the entity to save.
     * @return the persisted entity.
     */
    Redeem save(Redeem redeem);

    /**
     * Get all the redeems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Redeem> findAll(Pageable pageable);


    /**
     * Get the "id" redeem.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Redeem> findOne(Long id);

    /**
     * Delete the "id" redeem.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the redeem corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Redeem> search(String query, Pageable pageable);
}
