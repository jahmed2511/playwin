package com.jas.playwin.service;

import com.jas.playwin.domain.Game;
import com.jas.playwin.domain.GameResult;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link GameResult}.
 */
public interface GameResultService {

	/**
	 * Save a gameResult.
	 *
	 * @param gameResult the entity to save.
	 * @return the persisted entity.
	 */
	GameResult save(GameResult gameResult);

	/**
	 * Get all the gameResults.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	Page<GameResult> findAll(Pageable pageable);

	/**
	 * Get the "id" gameResult.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	Optional<GameResult> findOne(Long id);

	/**
	 * Delete the "id" gameResult.
	 *
	 * @param id the id of the entity.
	 */
	void delete(Long id);

	/**
	 * Search for the gameResult corresponding to the query.
	 *
	 * @param query    the query of the search.
	 * 
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	Page<GameResult> search(String query, Pageable pageable);

	/**
	 * To find the last game draw time.
	 * 
	 * @param game
	 * @return last drawn game
	 */
	GameResult findLastDraw(Game game);
}
