package com.jas.playwin.service;

import java.util.List;

import javax.persistence.criteria.JoinType;
import javax.persistence.metamodel.SingularAttribute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.jas.playwin.domain.Game;
import com.jas.playwin.domain.*; // for static metamodels
import com.jas.playwin.repository.GameRepository;
import com.jas.playwin.repository.search.GameSearchRepository;
import com.jas.playwin.service.dto.GameCriteria;

/**
 * Service for executing complex queries for {@link Game} entities in the database.
 * The main input is a {@link GameCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Game} or a {@link Page} of {@link Game} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class GameQueryService extends QueryService<Game> {

    private final Logger log = LoggerFactory.getLogger(GameQueryService.class);

    private final GameRepository gameRepository;

    private final GameSearchRepository gameSearchRepository;

    public GameQueryService(GameRepository gameRepository, GameSearchRepository gameSearchRepository) {
        this.gameRepository = gameRepository;
        this.gameSearchRepository = gameSearchRepository;
    }

    /**
     * Return a {@link List} of {@link Game} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Game> findByCriteria(GameCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Game> specification = createSpecification(criteria);
        return gameRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Game} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Game> findByCriteria(GameCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Game> specification = createSpecification(criteria);
        return gameRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(GameCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Game> specification = createSpecification(criteria);
        return gameRepository.count(specification);
    }

    /**
     * Function to convert GameCriteria to a {@link Specification}.
     */
    private Specification<Game> createSpecification(GameCriteria criteria) {
        Specification<Game> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Game_.id));
            }
            if (criteria.getGameName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGameName(), Game_.gameName));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Game_.description));
            }
            if (criteria.getStartTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStartTime(), Game_.startTime));
            }
            if (criteria.getEndTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEndTime(), Game_.endTime));
            }
            if (criteria.getInterval() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getInterval(), Game_.interval));
            }
            if (criteria.getNumberRangeFrom() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNumberRangeFrom(), Game_.numberRangeFrom));
            }
            if (criteria.getNumberRangeTo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNumberRangeTo(), Game_.numberRangeTo));
            }
            if (criteria.getIsEnable() != null) {
                specification = specification.and(buildSpecification(criteria.getIsEnable(), Game_.isEnable));
            }
            if (criteria.getCoinId() != null) {
                specification = specification.and(buildSpecification(criteria.getCoinId(),
                    root -> root.join(Game_.coins, JoinType.LEFT).get(Coin_.id)));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(Game_.users, JoinType.LEFT).get(User_.id)));
            }
//            if (criteria.getBidId() != null) {
//                specification = specification.and(buildSpecification(criteria.getBidId(),
//                    root -> root.join(Game_.bids, JoinType.LEFT).get(Bid_.id)));
//            }
        }
        return specification;
    }
}
