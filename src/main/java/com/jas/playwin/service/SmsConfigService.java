package com.jas.playwin.service;

import com.jas.playwin.domain.SmsConfig;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link SmsConfig}.
 */
public interface SmsConfigService {

    /**
     * Save a smsConfig.
     *
     * @param smsConfig the entity to save.
     * @return the persisted entity.
     */
    SmsConfig save(SmsConfig smsConfig);

    /**
     * Get all the smsConfigs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SmsConfig> findAll(Pageable pageable);


    /**
     * Get the "id" smsConfig.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SmsConfig> findOne(Long id);

    /**
     * Delete the "id" smsConfig.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the smsConfig corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SmsConfig> search(String query, Pageable pageable);
}
