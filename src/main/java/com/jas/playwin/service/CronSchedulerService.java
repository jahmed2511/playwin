package com.jas.playwin.service;

import com.jas.playwin.domain.CronScheduler;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link CronScheduler}.
 */
public interface CronSchedulerService {

	/**
	 * Save a cronScheduler.
	 *
	 * @param cronScheduler the entity to save.
	 * @return the persisted entity.
	 */
	CronScheduler save(CronScheduler cronScheduler);

	/**
	 * Get all the cronSchedulers.
	 *
	 * @return the list of entities.
	 */
	List<CronScheduler> findAll();

	/**
	 * Get the "id" cronScheduler.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	Optional<CronScheduler> findOne(Long id);

	/**
	 * Delete the "id" cronScheduler.
	 *
	 * @param id the id of the entity.
	 */
	void delete(Long id);

	/**
	 * Search for the cronScheduler corresponding to the query.
	 *
	 * @param query the query of the search.
	 * 
	 * @return the list of entities.
	 */
	List<CronScheduler> search(String query);

	void deleteByGameId(Long gameId);
}
