package com.jas.playwin.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;

/**
 * A GameDrawConfig.
 */
@Entity
@Table(name = "game_draw_config")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "gamedrawconfig")
public class GameDrawConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)            
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "range_from")
    private Long rangeFrom;

    @Column(name = "range_to")
    private Long rangeTo;

    @NotNull
    @Column(name = "active", nullable = false)
    private Boolean active;

    @OneToOne
    private Game game;

    @OneToOne(optional = false)
    private Coin coin;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPriority() {
        return priority;
    }

    public GameDrawConfig priority(Integer priority) {
        this.priority = priority;
        return this;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Long getRangeFrom() {
        return rangeFrom;
    }

    public GameDrawConfig rangeFrom(Long rangeFrom) {
        this.rangeFrom = rangeFrom;
        return this;
    }

    public void setRangeFrom(Long rangeFrom) {
        this.rangeFrom = rangeFrom;
    }

    public Long getRangeTo() {
        return rangeTo;
    }

    public GameDrawConfig rangeTo(Long rangeTo) {
        this.rangeTo = rangeTo;
        return this;
    }

    public void setRangeTo(Long rangeTo) {
        this.rangeTo = rangeTo;
    }

    public Boolean isActive() {
        return active;
    }

    public GameDrawConfig active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Game getGame() {
        return game;
    }

    public GameDrawConfig game(Game game) {
        this.game = game;
        return this;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Coin getCoin() {
        return coin;
    }

    public GameDrawConfig coin(Coin coin) {
        this.coin = coin;
        return this;
    }

    public void setCoin(Coin coin) {
        this.coin = coin;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GameDrawConfig)) {
            return false;
        }
        return id != null && id.equals(((GameDrawConfig) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "GameDrawConfig{" +
            "id=" + getId() +
            ", priority=" + getPriority() +
            ", rangeFrom=" + getRangeFrom() +
            ", rangeTo=" + getRangeTo() +
            ", active='" + isActive() + "'" +
            "}";
    }
}
