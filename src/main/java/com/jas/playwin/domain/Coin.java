package com.jas.playwin.domain;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.data.elasticsearch.annotations.FieldType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A Coin.
 */
@Entity
@Table(name = "coin")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "coin")
public class Coin implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    @JsonIgnoreProperties
    private Long id;

    @NotNull
    @Column(name = "coint_type", nullable = false)
    private String cointType;

    @NotNull
    @Column(name = "display_name", nullable = false)
    private String displayName;

    @NotNull
    @Column(name = "base_price", nullable = false)
    private Float basePrice;

    @NotNull
    @Column(name = "base_winning_price", nullable = false)
    private Float baseWinningPrice;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JsonIgnoreProperties("coins")
    private User user;

    @ManyToOne
    @JsonIgnoreProperties("coins")
    private Game game;

//    @OneToMany(mappedBy = "coin")
//    @JsonIgnoreProperties("coins")
//    private Set<Bid> bids = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCointType() {
        return cointType;
    }

    public Coin cointType(String cointType) {
        this.cointType = cointType;
        return this;
    }

    public void setCointType(String cointType) {
        this.cointType = cointType;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Coin displayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Float getBasePrice() {
        return basePrice;
    }

    public Coin basePrice(Float basePrice) {
        this.basePrice = basePrice;
        return this;
    }

    public void setBasePrice(Float basePrice) {
        this.basePrice = basePrice;
    }

    public Float getBaseWinningPrice() {
        return baseWinningPrice;
    }

    public Coin baseWinningPrice(Float baseWinningPrice) {
        this.baseWinningPrice = baseWinningPrice;
        return this;
    }

    public void setBaseWinningPrice(Float baseWinningPrice) {
        this.baseWinningPrice = baseWinningPrice;
    }

    public String getDescription() {
        return description;
    }

    public Coin description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public Coin user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Game getGame() {
        return game;
    }

    public Coin game(Game game) {
        this.game = game;
        return this;
    }

    public void setGame(Game game) {
        this.game = game;
    }

//    public Set<Bid> getBids() {
//        return bids;
//    }
//
//    public Coin bids(Set<Bid> bids) {
//        this.bids = bids;
//        return this;
//    }
//
//    public Coin addBid(Bid bid) {
//        this.bids.add(bid);
//        bid.setCoin(this);
//        return this;
//    }

//    public Coin removeBid(Bid bid) {
//        this.bids.remove(bid);
//        bid.setCoin(null);
//        return this;
//    }
//
//    public void setBids(Set<Bid> bids) {
//        this.bids = bids;
//    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Coin)) {
            return false;
        }
        return id != null && id.equals(((Coin) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Coin{" +
            "id=" + getId() +
            ", cointType='" + getCointType() + "'" +
            ", displayName='" + getDisplayName() + "'" +
            ", basePrice=" + getBasePrice() +
            ", baseWinningPrice=" + getBaseWinningPrice() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
