package com.jas.playwin.domain;



import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * A UserPref.
 */
@Entity
@Table(name = "user_pref")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "userpref")
public class UserPref implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

   // @NotNull
    @Column(name = "distributor_share")
    private Integer distributorShare;

    //@NotNull
    @Column(name = "is_specific_price")
    private Boolean isSpecificPrice;

    //@NotNull
    @Column(name = "mobile_no", nullable = false)
    private String mobileNo;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDistributorShare() {
        return distributorShare;
    }

    public UserPref distributorShare(Integer distributorShare) {
        this.distributorShare = distributorShare;
        return this;
    }

    public void setDistributorShare(Integer distributorShare) {
        this.distributorShare = distributorShare;
    }

    public Boolean isIsSpecificPrice() {
        return isSpecificPrice;
    }

    public UserPref isSpecificPrice(Boolean isSpecificPrice) {
        this.isSpecificPrice = isSpecificPrice;
        return this;
    }

    public void setIsSpecificPrice(Boolean isSpecificPrice) {
        this.isSpecificPrice = isSpecificPrice;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public UserPref mobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
        return this;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public User getUser() {
        return user;
    }

    public UserPref user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserPref)) {
            return false;
        }
        return id != null && id.equals(((UserPref) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UserPref{" +
            "id=" + getId() +
            ", distributorShare=" + getDistributorShare() +
            ", isSpecificPrice='" + isIsSpecificPrice() + "'" +
            ", mobileNo='" + getMobileNo() + "'" +
            "}";
    }
}
