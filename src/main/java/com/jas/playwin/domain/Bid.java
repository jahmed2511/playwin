package com.jas.playwin.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Bid.
 */
@Entity
@Table(name = "bid")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "bid")
public class Bid implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "is_winner")
    private Boolean isWinner;

    @Column(name = "bid_no")
    private Integer bidNo;

    @Column(name = "no_of_coins")
    private Integer noOfCoins;

    @NotNull
    @Column(name = "bid_time", nullable = false)
    private Instant bidTime;

    @Column(name = "is_drow_happan")
    private Boolean isDrowHappan;

    @Column(name = "remarks")
    private String remarks;

    @ManyToOne
    @JsonIgnoreProperties("bids")
    private Game game;

    @ManyToOne
    private Coin coin;

    @ManyToOne
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsWinner() {
        return isWinner;
    }

    public Bid isWinner(Boolean isWinner) {
        this.isWinner = isWinner;
        return this;
    }

    public void setIsWinner(Boolean isWinner) {
        this.isWinner = isWinner;
    }

    public Integer getBidNo() {
        return bidNo;
    }

    public Bid bidNo(Integer bidNo) {
        this.bidNo = bidNo;
        return this;
    }

    public void setBidNo(Integer bidNo) {
        this.bidNo = bidNo;
    }

    public Integer getNoOfCoins() {
        return noOfCoins;
    }

    public Bid noOfCoins(Integer noOfCoins) {
        this.noOfCoins = noOfCoins;
        return this;
    }

    public void setNoOfCoins(Integer noOfCoins) {
        this.noOfCoins = noOfCoins;
    }

    public Instant getBidTime() {
        return bidTime;
    }

    public Bid bidTime(Instant bidTime) {
        this.bidTime = bidTime;
        return this;
    }

    public void setBidTime(Instant bidTime) {
        this.bidTime = bidTime;
    }

    public Boolean isIsDrowHappan() {
        return isDrowHappan;
    }

    public Bid isDrowHappan(Boolean isDrowHappan) {
        this.isDrowHappan = isDrowHappan;
        return this;
    }

    public void setIsDrowHappan(Boolean isDrowHappan) {
        this.isDrowHappan = isDrowHappan;
    }

    public String getRemarks() {
        return remarks;
    }

    public Bid remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Game getGame() {
        return game;
    }

    public Bid game(Game game) {
        this.game = game;
        return this;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Coin getCoin() {
        return coin;
    }

    public Bid coin(Coin coin) {
        this.coin = coin;
        return this;
    }

    public void setCoin(Coin coin) {
        this.coin = coin;
    }

    public User getUser() {
        return user;
    }

    public Bid user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Bid)) {
            return false;
        }
        return id != null && id.equals(((Bid) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Bid{" +
            "id=" + getId() +
            ", isWinner='" + isIsWinner() + "'" +
            ", bidNo=" + getBidNo() +
            ", noOfCoins=" + getNoOfCoins() +
            ", bidTime='" + getBidTime() + "'" +
            ", isDrowHappan='" + isIsDrowHappan() + "'" +
            ", remarks='" + getRemarks() + "'" +
            "}";
    }
}
