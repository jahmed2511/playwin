package com.jas.playwin.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * A AgentCommision.
 */
@Entity
@Table(name = "agent_commision")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "agentcommision")
public class AgentCommision implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @NotNull
    @Column(name = "commision_percentage", nullable = false)
    private Integer commisionPercentage;

    @OneToOne(optional = false)    @NotNull

    @JoinColumn(unique = true)
    private User user;

    @OneToOne(optional = false)    
    @NotNull
    @JoinColumn(unique = true)
    private Coin coin;

    @OneToOne(optional = false)   
    @NotNull
    private Game game;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCommisionPercentage() {
        return commisionPercentage;
    }

    public AgentCommision commisionPercentage(Integer commisionPercentage) {
        this.commisionPercentage = commisionPercentage;
        return this;
    }

    public void setCommisionPercentage(Integer commisionPercentage) {
        this.commisionPercentage = commisionPercentage;
    }

    public User getUser() {
        return user;
    }

    public AgentCommision user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Coin getCoin() {
        return coin;
    }

    public AgentCommision coin(Coin coin) {
        this.coin = coin;
        return this;
    }

    public void setCoin(Coin coin) {
        this.coin = coin;
    }

    public Game getGame() {
        return game;
    }

    public AgentCommision game(Game game) {
        this.game = game;
        return this;
    }

    public void setGame(Game game) {
        this.game = game;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AgentCommision)) {
            return false;
        }
        return id != null && id.equals(((AgentCommision) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AgentCommision{" +
            "id=" + getId() +
            ", commisionPercentage=" + getCommisionPercentage() +
            "}";
    }
}
