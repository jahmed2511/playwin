package com.jas.playwin.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A SmsConfig.
 */
@Entity
@Table(name = "sms_config")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "smsconfig")
public class SmsConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "sms_time_slots")
    private String smsTimeSlots;

    @Column(name = "coins")
    private String coins;

    @Column(name = "expiry_date")
    private Instant expiryDate;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "mobile_no")
    private String mobileNo;

    @ManyToOne
    @JsonIgnoreProperties("smsConfigs")
    private User user;

    @ManyToOne
    @JsonIgnoreProperties("smsConfigs")
    private Game game;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSmsTimeSlots() {
        return smsTimeSlots;
    }

    public SmsConfig smsTimeSlots(String smsTimeSlots) {
        this.smsTimeSlots = smsTimeSlots;
        return this;
    }

    public void setSmsTimeSlots(String smsTimeSlots) {
        this.smsTimeSlots = smsTimeSlots;
    }

    public String getCoins() {
        return coins;
    }

    public SmsConfig coins(String coins) {
        this.coins = coins;
        return this;
    }

    public void setCoins(String coins) {
        this.coins = coins;
    }

    public Instant getExpiryDate() {
        return expiryDate;
    }

    public SmsConfig expiryDate(Instant expiryDate) {
        this.expiryDate = expiryDate;
        return this;
    }

    public void setExpiryDate(Instant expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getDescription() {
        return description;
    }

    public SmsConfig description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public SmsConfig mobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
        return this;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public User getUser() {
        return user;
    }

    public SmsConfig user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Game getGame() {
        return game;
    }

    public SmsConfig game(Game game) {
        this.game = game;
        return this;
    }

    public void setGame(Game game) {
        this.game = game;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SmsConfig)) {
            return false;
        }
        return id != null && id.equals(((SmsConfig) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "SmsConfig{" +
            "id=" + getId() +
            ", smsTimeSlots='" + getSmsTimeSlots() + "'" +
            ", coins='" + getCoins() + "'" +
            ", expiryDate='" + getExpiryDate() + "'" +
            ", description='" + getDescription() + "'" +
            ", mobileNo='" + getMobileNo() + "'" +
            "}";
    }
}
