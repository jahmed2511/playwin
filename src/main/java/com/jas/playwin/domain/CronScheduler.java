package com.jas.playwin.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.Objects;

/**
 * A CronScheduler.
 */
@Entity
@Table(name = "cron_scheduler")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "cronscheduler")
public class CronScheduler implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
	private Long id;

	@NotNull
	@Column(name = "game_id", nullable = false)
	private Long gameId;

	@NotNull
	@Column(name = "cron_exp", nullable = false)
	private String cronExp;

	@ManyToOne(optional = false)
	@JoinColumn(name = "game_id", insertable = false, updatable = false)
	private Game game;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getGameId() {
		return gameId;
	}

	public CronScheduler gameId(Long gameId) {
		this.gameId = gameId;
		return this;
	}

	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}

	public String getCronExp() {
		return cronExp;
	}

	public CronScheduler cronExp(String cronExp) {
		this.cronExp = cronExp;
		return this;
	}

	public void setCronExp(String cronExp) {
		this.cronExp = cronExp;
	}

	public Game getGame() {
		return game;
	}

	public CronScheduler game(Game game) {
		this.game = game;
		return this;
	}

	public void setGame(Game game) {
		this.game = game;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof CronScheduler)) {
			return false;
		}
		return id != null && id.equals(((CronScheduler) o).id);
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return "CronScheduler{" + "id=" + getId() + ", gameId=" + getGameId() + ", cronExp='" + getCronExp() + "'"
				+ "}";
	}
}
