package com.jas.playwin.domain;

import java.util.Map;

public class DrawResult {

	private Long gameId;
	private Map<Coin, Integer> result;

	public Long getGameId() {
		return gameId;
	}

	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}

	public Map<Coin, Integer> getResult() {
		return result;
	}

	public void setResult(Map<Coin, Integer> coinResultMap) {
		this.result = coinResultMap;
	}

}
