package com.jas.playwin.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A GameResult.
 */
@Entity
@Table(name = "game_result")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "gameresult")
public class GameResult implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @NotNull
    @Column(name = "game_time", nullable = false)
    private Instant gameTime;

    @Column(name = "winners_nos")
    private String winnersNos;

    @Column(name = "remarks")
    private String remarks;

    @ManyToOne
    @JsonIgnoreProperties("gameResults")
    private Game game;

    @ManyToOne
    @JsonIgnoreProperties("gameResults")
    private Coin coin;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getGameTime() {
        return gameTime;
    }

    public GameResult gameTime(Instant gameTime) {
        this.gameTime = gameTime;
        return this;
    }

    public void setGameTime(Instant gameTime) {
        this.gameTime = gameTime;
    }

    public String getWinnersNos() {
        return winnersNos;
    }

    public GameResult winnersNos(String winnersNos) {
        this.winnersNos = winnersNos;
        return this;
    }

    public void setWinnersNos(String winnersNos) {
        this.winnersNos = winnersNos;
    }

    public String getRemarks() {
        return remarks;
    }

    public GameResult remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Game getGame() {
        return game;
    }

    public GameResult game(Game game) {
        this.game = game;
        return this;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Coin getCoin() {
        return coin;
    }

    public GameResult coin(Coin coin) {
        this.coin = coin;
        return this;
    }

    public void setCoin(Coin coin) {
        this.coin = coin;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GameResult)) {
            return false;
        }
        return id != null && id.equals(((GameResult) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "GameResult{" +
            "id=" + getId() +
            ", gameTime='" + getGameTime() + "'" +
            ", winnersNos='" + getWinnersNos() + "'" +
            ", remarks='" + getRemarks() + "'" +
            "}";
    }
}
