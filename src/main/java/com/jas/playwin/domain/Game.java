package com.jas.playwin.domain;


import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.data.elasticsearch.annotations.FieldType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A Game.
 */
@Entity
@Table(name = "game")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "game")
public class Game implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @NotNull
    @Column(name = "game_name", nullable = false)
    private String gameName;

    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "start_time", nullable = false)
    private Instant startTime;

    @NotNull
    @Column(name = "end_time", nullable = false)
    private Instant endTime;

    @NotNull
    @Column(name = "jhi_interval", nullable = false)
    private Integer interval;

    @NotNull
    @Column(name = "number_range_from", nullable = false)
    private Integer numberRangeFrom;

    @NotNull
    @Column(name = "number_range_to", nullable = false)
    private Integer numberRangeTo;

    @Column(name = "is_enable")
    private Boolean isEnable;

    @OneToMany(mappedBy = "game")
    @JsonIgnoreProperties("coins")
    private Set<Coin> coins = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "game_user",
               joinColumns = @JoinColumn(name = "game_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
    private Set<User> users = new HashSet<>();

//    @OneToMany(mappedBy = "game")
//    private Set<Bid> bids = new HashSet<>();
//    
   

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGameName() {
        return gameName;
    }

    public Game gameName(String gameName) {
        this.gameName = gameName;
        return this;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getDescription() {
        return description;
    }

    public Game description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public Game startTime(Instant startTime) {
        this.startTime = startTime;
        return this;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public Game endTime(Instant endTime) {
        this.endTime = endTime;
        return this;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public Integer getInterval() {
        return interval;
    }

    public Game interval(Integer interval) {
        this.interval = interval;
        return this;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public Integer getNumberRangeFrom() {
        return numberRangeFrom;
    }

    public Game numberRangeFrom(Integer numberRangeFrom) {
        this.numberRangeFrom = numberRangeFrom;
        return this;
    }

    public void setNumberRangeFrom(Integer numberRangeFrom) {
        this.numberRangeFrom = numberRangeFrom;
    }

    public Integer getNumberRangeTo() {
        return numberRangeTo;
    }

    public Game numberRangeTo(Integer numberRangeTo) {
        this.numberRangeTo = numberRangeTo;
        return this;
    }

    public void setNumberRangeTo(Integer numberRangeTo) {
        this.numberRangeTo = numberRangeTo;
    }

    public Boolean isIsEnable() {
        return isEnable;
    }

    public Game isEnable(Boolean isEnable) {
        this.isEnable = isEnable;
        return this;
    }

    public void setIsEnable(Boolean isEnable) {
        this.isEnable = isEnable;
    }

    public Set<Coin> getCoins() {
        return coins;
    }

    public Game coins(Set<Coin> coins) {
        this.coins = coins;
        return this;
    }

    public Game addCoin(Coin coin) {
        this.coins.add(coin);
        coin.setGame(this);
        return this;
    }

    public Game removeCoin(Coin coin) {
        this.coins.remove(coin);
        coin.setGame(null);
        return this;
    }

    public void setCoins(Set<Coin> coins) {
        this.coins = coins;
    }

    public Set<User> getUsers() {
        return users;
    }

    public Game users(Set<User> users) {
        this.users = users;
        return this;
    }

    public Game addUser(User user) {
        this.users.add(user);
        return this;
    }

    public Game removeUser(User user) {
        this.users.remove(user);
        return this;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

//    public Set<Bid> getBids() {
//        return bids;
//    }
//
//    public Game bids(Set<Bid> bids) {
//        this.bids = bids;
//        return this;
//    }

//    public Game addBid(Bid bid) {
//        this.bids.add(bid);
//        bid.setGame(this);
//        return this;
//    }

//    public Game removeBid(Bid bid) {
//        this.bids.remove(bid);
//        bid.setGame(null);
//        return this;
//    }
//
//    public void setBids(Set<Bid> bids) {
//        this.bids = bids;
//    }
    
    
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

 

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Game)) {
            return false;
        }
        return id != null && id.equals(((Game) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Game{" +
            "id=" + getId() +
            ", gameName='" + getGameName() + "'" +
            ", description='" + getDescription() + "'" +
            ", startTime='" + getStartTime() + "'" +
            ", endTime='" + getEndTime() + "'" +
            ", interval=" + getInterval() +
            ", numberRangeFrom=" + getNumberRangeFrom() +
            ", numberRangeTo=" + getNumberRangeTo() +
            ", isEnable='" + isIsEnable() + "'" +
            "}";
    }
}
