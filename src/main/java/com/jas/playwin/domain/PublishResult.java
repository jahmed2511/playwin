package com.jas.playwin.domain;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.data.elasticsearch.annotations.FieldType;

@Entity
@Table(name = "publish_result")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "publishresult")

public class PublishResult implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "game_id")
    private Long gameId;

    @Column(name = "coin_id")
    private Long coinId;

    @Column(name = "publish_date")
    private LocalDate publishDate;
    
    @Column(name = "publish_time")
    private String publishTime;
    
    @Column(name = "publish_number")
    private Integer publishNumber;
    
    @NotNull
    @Column(name = "isPublish", nullable = false)
    private Boolean isPublish;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getGameId() {
		return gameId;
	}

	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}

	public Long getCoinId() {
		return coinId;
	}

	public void setCoinId(Long coinId) {
		this.coinId = coinId;
	}

	public LocalDate getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(LocalDate publishDate) {
		this.publishDate = publishDate;
	}

	public String getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(String publishTime) {
		this.publishTime = publishTime;
	}

	public Integer getPublishNumber() {
		return publishNumber;
	}

	public void setPublishNumber(Integer publishNumber) {
		this.publishNumber = publishNumber;
	}

	public Boolean getIsPublish() {
		return isPublish;
	}

	public void setIsPublish(Boolean isPublish) {
		this.isPublish = isPublish;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((coinId == null) ? 0 : coinId.hashCode());
		result = prime * result + ((gameId == null) ? 0 : gameId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((isPublish == null) ? 0 : isPublish.hashCode());
		result = prime * result + ((publishDate == null) ? 0 : publishDate.hashCode());
		result = prime * result + ((publishNumber == null) ? 0 : publishNumber.hashCode());
		result = prime * result + ((publishTime == null) ? 0 : publishTime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PublishResult other = (PublishResult) obj;
		if (coinId == null) {
			if (other.coinId != null)
				return false;
		} else if (!coinId.equals(other.coinId))
			return false;
		if (gameId == null) {
			if (other.gameId != null)
				return false;
		} else if (!gameId.equals(other.gameId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isPublish == null) {
			if (other.isPublish != null)
				return false;
		} else if (!isPublish.equals(other.isPublish))
			return false;
		if (publishDate == null) {
			if (other.publishDate != null)
				return false;
		} else if (!publishDate.equals(other.publishDate))
			return false;
		if (publishNumber == null) {
			if (other.publishNumber != null)
				return false;
		} else if (!publishNumber.equals(other.publishNumber))
			return false;
		if (publishTime == null) {
			if (other.publishTime != null)
				return false;
		} else if (!publishTime.equals(other.publishTime))
			return false;
		return true;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}


    

}
