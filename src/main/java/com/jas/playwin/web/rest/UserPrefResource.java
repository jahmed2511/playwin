package com.jas.playwin.web.rest;

import com.jas.playwin.domain.UserPref;
import com.jas.playwin.service.UserPrefService;
import com.jas.playwin.web.rest.errors.BadRequestAlertException;
import com.jas.playwin.service.dto.UserPrefCriteria;
import com.jas.playwin.service.UserPrefQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.jas.playwin.domain.UserPref}.
 */
@RestController
@RequestMapping("/api")
public class UserPrefResource {

    private final Logger log = LoggerFactory.getLogger(UserPrefResource.class);

    private static final String ENTITY_NAME = "userPref";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserPrefService userPrefService;

    private final UserPrefQueryService userPrefQueryService;

    public UserPrefResource(UserPrefService userPrefService, UserPrefQueryService userPrefQueryService) {
        this.userPrefService = userPrefService;
        this.userPrefQueryService = userPrefQueryService;
    }

    /**
     * {@code POST  /user-prefs} : Create a new userPref.
     *
     * @param userPref the userPref to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userPref, or with status {@code 400 (Bad Request)} if the userPref has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-prefs")
    public ResponseEntity<UserPref> createUserPref(@Valid @RequestBody UserPref userPref) throws URISyntaxException {
        log.debug("REST request to save UserPref : {}", userPref);
        if (userPref.getId() != null) {
            throw new BadRequestAlertException("A new userPref cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserPref result = userPrefService.save(userPref);
        return ResponseEntity.created(new URI("/api/user-prefs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-prefs} : Updates an existing userPref.
     *
     * @param userPref the userPref to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userPref,
     * or with status {@code 400 (Bad Request)} if the userPref is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userPref couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-prefs")
    public ResponseEntity<UserPref> updateUserPref(@Valid @RequestBody UserPref userPref) throws URISyntaxException {
        log.debug("REST request to update UserPref : {}", userPref);
        if (userPref.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserPref result = userPrefService.save(userPref);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userPref.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /user-prefs} : get all the userPrefs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userPrefs in body.
     */
    @GetMapping("/user-prefs")
    public ResponseEntity<List<UserPref>> getAllUserPrefs(UserPrefCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get UserPrefs by criteria: {}", criteria);
        Page<UserPref> page = userPrefQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /user-prefs/count} : count all the userPrefs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/user-prefs/count")
    public ResponseEntity<Long> countUserPrefs(UserPrefCriteria criteria) {
        log.debug("REST request to count UserPrefs by criteria: {}", criteria);
        return ResponseEntity.ok().body(userPrefQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /user-prefs/:id} : get the "id" userPref.
     *
     * @param id the id of the userPref to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userPref, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-prefs/{id}")
    public ResponseEntity<UserPref> getUserPref(@PathVariable Long id) {
        log.debug("REST request to get UserPref : {}", id);
        Optional<UserPref> userPref = userPrefService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userPref);
    }

    /**
     * {@code DELETE  /user-prefs/:id} : delete the "id" userPref.
     *
     * @param id the id of the userPref to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-prefs/{id}")
    public ResponseEntity<Void> deleteUserPref(@PathVariable Long id) {
        log.debug("REST request to delete UserPref : {}", id);
        userPrefService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/user-prefs?query=:query} : search for the userPref corresponding
     * to the query.
     *
     * @param query the query of the userPref search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/user-prefs")
    public ResponseEntity<List<UserPref>> searchUserPrefs(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to search for a page of UserPrefs for query {}", query);
        Page<UserPref> page = userPrefService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
