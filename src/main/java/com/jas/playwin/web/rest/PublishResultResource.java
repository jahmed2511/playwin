package com.jas.playwin.web.rest;

import java.net.URI;
import java.net.URISyntaxException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jas.playwin.domain.PublishResult;
import com.jas.playwin.repository.PublishResultRepository;

import io.github.jhipster.web.util.HeaderUtil;

@RestController
@RequestMapping("/api")
public class PublishResultResource {

	@Value("${jhipster.clientApp.name}")
	private String applicationName;
	  private static final String ENTITY_NAME = "publishResult";

	private final PublishResultRepository publishResultRepository;

	public PublishResultResource(PublishResultRepository publishResultRepository) {
		this.publishResultRepository = publishResultRepository;
	}

	@PostMapping("/publish-result")
	public ResponseEntity<PublishResult> createBid(@Valid @RequestBody PublishResult publishResult)
			throws URISyntaxException {

		PublishResult result = publishResultRepository.save(publishResult);
		return ResponseEntity
				.created(new URI("/api/publish-result/" + result.getId())).headers(HeaderUtil
						.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
				.body(result);
	}

}
