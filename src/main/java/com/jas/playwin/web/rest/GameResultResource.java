package com.jas.playwin.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.jas.playwin.domain.GameResult;
import com.jas.playwin.service.GameResultQueryService;
import com.jas.playwin.service.GameResultService;
import com.jas.playwin.service.dto.GameResultCriteria;
import com.jas.playwin.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.jas.playwin.domain.GameResult}.
 */
@RestController
@RequestMapping("/api")
public class GameResultResource {
	
	private static final String ASIA_CALCUTTA = "Asia/Calcutta";

    private final Logger log = LoggerFactory.getLogger(GameResultResource.class);

    private static final String ENTITY_NAME = "gameResult";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GameResultService gameResultService;

    private final GameResultQueryService gameResultQueryService;

    public GameResultResource(GameResultService gameResultService, GameResultQueryService gameResultQueryService) {
        this.gameResultService = gameResultService;
        this.gameResultQueryService = gameResultQueryService;
    }

    /**
     * {@code POST  /game-results} : Create a new gameResult.
     *
     * @param gameResult the gameResult to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new gameResult, or with status {@code 400 (Bad Request)} if the gameResult has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/game-results")
    public ResponseEntity<GameResult> createGameResult(@Valid @RequestBody GameResult gameResult) throws URISyntaxException {
        log.debug("REST request to save GameResult : {}", gameResult);
        if (gameResult.getId() != null) {
            throw new BadRequestAlertException("A new gameResult cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GameResult result = gameResultService.save(gameResult);
        return ResponseEntity.created(new URI("/api/game-results/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /game-results} : Updates an existing gameResult.
     *
     * @param gameResult the gameResult to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated gameResult,
     * or with status {@code 400 (Bad Request)} if the gameResult is not valid,
     * or with status {@code 500 (Internal Server Error)} if the gameResult couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/game-results")
    public ResponseEntity<GameResult> updateGameResult(@Valid @RequestBody GameResult gameResult) throws URISyntaxException {
        log.debug("REST request to update GameResult : {}", gameResult);
        if (gameResult.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        GameResult result = gameResultService.save(gameResult);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, gameResult.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /game-results} : get all the gameResults.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of gameResults in body.
     */
    @GetMapping("/game-results")
    public ResponseEntity<List<GameResult>> getAllGameResults(GameResultCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get GameResults by criteria: {}", criteria);
        List<String> gameIds = queryParams.get("criteria.gameId");
        List<String> gameDateStr = queryParams.get("criteria.date");
        
        if(gameIds != null && !gameIds.isEmpty()) {
	        if(!StringUtils.isEmpty(gameIds.get(0))) {
	        	LongFilter gameIdFilter = new LongFilter();
		        gameIdFilter.setEquals(Long.parseLong(gameIds.get(0)));
		        criteria.setGameId(gameIdFilter);
	        }
	    }
        if(gameDateStr != null && !gameDateStr.isEmpty()) {
	        if(!StringUtils.isEmpty(gameDateStr.get(0))) {
	        	Instant gameTime = Instant.now().atZone(ZoneId.of("Asia/Kolkata")).toInstant();
	        	LocalDate date = LocalDate.parse(gameDateStr.get(0));
	        	gameTime =date.atStartOfDay(ZoneId.of(ASIA_CALCUTTA)).toInstant();
	        	Instant nextDayTime =gameTime.plus(1, ChronoUnit.DAYS);
	        	InstantFilter gameTimeFilter = new InstantFilter();
	        	gameTimeFilter.setLessThan(nextDayTime);
	        	gameTimeFilter.setGreaterThan(gameTime);
	        	criteria.setGameTime(gameTimeFilter);
		      	        
	        }
	    }
        pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by("gameTime").descending());
        Page<GameResult> page = gameResultQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
  

    /**
    * {@code GET  /game-results/count} : count all the gameResults.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/game-results/count")
    public ResponseEntity<Long> countGameResults(GameResultCriteria criteria) {
        log.debug("REST request to count GameResults by criteria: {}", criteria);
        return ResponseEntity.ok().body(gameResultQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /game-results/:id} : get the "id" gameResult.
     *
     * @param id the id of the gameResult to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the gameResult, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/game-results/{id}")
    public ResponseEntity<GameResult> getGameResult(@PathVariable Long id) {
        log.debug("REST request to get GameResult : {}", id);
        Optional<GameResult> gameResult = gameResultService.findOne(id);
        return ResponseUtil.wrapOrNotFound(gameResult);
    }

    /**
     * {@code DELETE  /game-results/:id} : delete the "id" gameResult.
     *
     * @param id the id of the gameResult to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/game-results/{id}")
    public ResponseEntity<Void> deleteGameResult(@PathVariable Long id) {
        log.debug("REST request to delete GameResult : {}", id);
        gameResultService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/game-results?query=:query} : search for the gameResult corresponding
     * to the query.
     *
     * @param query the query of the gameResult search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/game-results")
    public ResponseEntity<List<GameResult>> searchGameResults(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to search for a page of GameResults for query {}", query);
        Page<GameResult> page = gameResultService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
