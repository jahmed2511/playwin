package com.jas.playwin.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.jas.playwin.domain.Authority;
import com.jas.playwin.domain.Redeem;
import com.jas.playwin.domain.User;
import com.jas.playwin.domain.Wallet;
import com.jas.playwin.security.AuthoritiesConstants;
import com.jas.playwin.security.SecurityUtils;
import com.jas.playwin.service.RedeemQueryService;
import com.jas.playwin.service.RedeemService;
import com.jas.playwin.service.UserService;
import com.jas.playwin.service.WalletService;
import com.jas.playwin.service.dto.RedeemCriteria;
import com.jas.playwin.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.jas.playwin.domain.Redeem}.
 */
@RestController
@RequestMapping("/api")
public class RedeemResource {

    private static final String APPROVED = "Approved";

	private static final String PENDING = "Pending";

	private final Logger log = LoggerFactory.getLogger(RedeemResource.class);

    private static final String ENTITY_NAME = "redeem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RedeemService redeemService;

    private final RedeemQueryService redeemQueryService;
    
    private final UserService userService;
    
    private final WalletService walletService;

    public RedeemResource(RedeemService redeemService, RedeemQueryService redeemQueryService, UserService userService, WalletService walletService) {
        this.redeemService = redeemService;
        this.redeemQueryService = redeemQueryService;
        this.userService = userService;
        this.walletService = walletService;
    }

    /**
     * {@code POST  /redeems} : Create a new redeem.
     *
     * @param redeem the redeem to create. 
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new redeem, or with status {@code 400 (Bad Request)} if the redeem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/redeems")
    public ResponseEntity<Redeem> createRedeem(@Valid @RequestBody Redeem redeem) throws URISyntaxException {
        log.debug("REST request to save Redeem : {}", redeem);
        if (redeem.getId() != null) {
            throw new BadRequestAlertException("A new redeem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        String currUserName =SecurityUtils.getCurrentUserLogin().get();
        User currUser =userService.getUserWithAuthoritiesByLogin(currUserName).get();
        Wallet currBalance = walletService.getRecentWalletByUser(currUser);
        if(currBalance.getBalance() < redeem.getAmount() ) {
        	throw new BadRequestAlertException("Insuffieciet Balance in your wallet", ENTITY_NAME, "lowBalnace");
        }
        Instant instant =Instant.now().atZone(ZoneId.of("Asia/Kolkata")).toInstant();
        redeem.createdOn(instant).status(PENDING).lastModifiedOn(instant).user(currUser);
        Redeem result = redeemService.save(redeem);
        return ResponseEntity.created(new URI("/api/redeems/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /redeems} : Updates an existing redeem.
     *
     * @param redeem the redeem to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated redeem,
     * or with status {@code 400 (Bad Request)} if the redeem is not valid,
     * or with status {@code 500 (Internal Server Error)} if the redeem couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/redeems")
    public ResponseEntity<Redeem> updateRedeem(@Valid @RequestBody Redeem redeem) throws URISyntaxException {
        log.debug("REST request to update Redeem : {}", redeem);
        if (redeem.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        String currUserName =SecurityUtils.getCurrentUserLogin().get();
        Wallet currBalance = walletService.getRecentWalletByUser(redeem.getUser());
        if(currBalance.getBalance() < redeem.getAmount() ) {
        	throw new BadRequestAlertException("Insuffieciet Balance in your wallet", ENTITY_NAME, "lowBalnace");
        }
        if(!PENDING.equals(redeem.getStatus()))  {
        	throw new BadRequestAlertException("Invalid redeems status", ENTITY_NAME, "InvalidStatus");
        }
        Instant instant =Instant.now().atZone(ZoneId.of("Asia/Kolkata")).toInstant();
        Wallet updateWallet = new Wallet();
        updateWallet.balance(currBalance.getBalance() - redeem.getAmount()).date(instant).debit(redeem.getAmount()).remarks("Redeem request approved").user(redeem.getUser());
        walletService.save(updateWallet);
        redeem.status(APPROVED).lastModifiedOn(instant).approvedBy(currUserName);
        Redeem result = redeemService.save(redeem);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, redeem.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /redeems} : get all the redeems.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of redeems in body.
     */
    @GetMapping("/redeems")
    public ResponseEntity<List<Redeem>> getAllRedeems(RedeemCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get Redeems by criteria: {}", criteria);
        String currUserName =SecurityUtils.getCurrentUserLogin().get();
        Authority adminAuth = new Authority();
        adminAuth.setName(AuthoritiesConstants.ADMIN);
        Page<Redeem> page = null;
        if(SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
        	page=redeemService.findAll(pageable);
        }else {
        	User currUser =userService.getUserWithAuthoritiesByLogin(currUserName).get();
        	LongFilter lFilter=new LongFilter();
            lFilter.setEquals(currUser.getId());
            final List<String> idList=queryParams.get("criteria.userId");
            if(idList!=null && !idList.isEmpty()) {
            	lFilter.setEquals(Long.parseLong(idList.get(0)));
                criteria.setUserId(lFilter);
            }
            page=redeemService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /redeems/count} : count all the redeems.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/redeems/count")
    public ResponseEntity<Long> countRedeems(RedeemCriteria criteria) {
        log.debug("REST request to count Redeems by criteria: {}", criteria);
        return ResponseEntity.ok().body(redeemQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /redeems/:id} : get the "id" redeem.
     *
     * @param id the id of the redeem to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the redeem, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/redeems/{id}")
    public ResponseEntity<Redeem> getRedeem(@PathVariable Long id) {
        log.debug("REST request to get Redeem : {}", id);
        Optional<Redeem> redeem = redeemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(redeem);
    }

    /**
     * {@code DELETE  /redeems/:id} : delete the "id" redeem.
     *
     * @param id the id of the redeem to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/redeems/{id}")
    public ResponseEntity<Void> deleteRedeem(@PathVariable Long id) {
        log.debug("REST request to delete Redeem : {}", id);
        redeemService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/redeems?query=:query} : search for the redeem corresponding
     * to the query.
     *
     * @param query the query of the redeem search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/redeems")
    public ResponseEntity<List<Redeem>> searchRedeems(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to search for a page of Redeems for query {}", query);
        Page<Redeem> page = redeemService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
