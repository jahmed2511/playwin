package com.jas.playwin.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.jas.playwin.domain.Game;
import com.jas.playwin.service.GameQueryService;
import com.jas.playwin.service.GameService;
import com.jas.playwin.service.JobService;
import com.jas.playwin.service.dto.GameCriteria;
import com.jas.playwin.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.jas.playwin.domain.Game}.
 */
@RestController
@RequestMapping("/api")
public class GameResource {

	private final Logger log = LoggerFactory.getLogger(GameResource.class);

	private static final String ENTITY_NAME = "game";

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final GameService gameService;

	private final GameQueryService gameQueryService;

	@Autowired
	private JobService jobService;

	public GameResource(GameService gameService, GameQueryService gameQueryService) {
		this.gameService = gameService;
		this.gameQueryService = gameQueryService;
	}

	/**
     * {@code POST  /games} : Create a new game.
     *
     * @param game the game to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new game, or with status {@code 400 (Bad Request)} if the game has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
	@PostMapping("/games")
	public ResponseEntity<Game> createGame(@Valid @RequestBody Game game) throws URISyntaxException {
		log.debug("REST request to save Game : {}", game);
		if (game.getId() != null) {
			throw new BadRequestAlertException("A new game cannot already have an ID", ENTITY_NAME, "idexists");
		}
		Game result = gameService.save(game);
		Optional<Game> result1 = gameService.findOne(result.getId());
		if (result1.isPresent()&& result1.get().isIsEnable()) {
			jobService.addJob(game);
		}
		return ResponseEntity
				.created(new URI("/api/games/" + result.getId())).headers(HeaderUtil
						.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
				.body(result);
	}

	/**
     * {@code PUT  /games} : Updates an existing game.
     *
     * @param game the game to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated game,
     * or with status {@code 400 (Bad Request)} if the game is not valid,
     * or with status {@code 500 (Internal Server Error)} if the game couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
	@PutMapping("/games")
	public ResponseEntity<Game> updateGame(@Valid @RequestBody Game game) throws URISyntaxException {
		log.debug("REST request to update Game : {}", game);
		if (game.getId() == null) {
			throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
		}
		Game result = gameService.save(game);
		Optional<Game> result1 = gameService.findOne(result.getId());
		if (result1.isPresent()&& result1.get().isIsEnable()) {
			jobService.addJob(game);
		}
		jobService.updateJob(result1.get());
		return ResponseEntity.ok()
				.headers(
						HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, game.getId().toString()))
				.body(result);
	}

	/**
     * {@code GET  /games} : get all the games.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of games in body.
     */
	@GetMapping("/games")
	public ResponseEntity<List<Game>> getAllGames(GameCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
		log.debug("REST request to get Games by criteria: {}", criteria);
		Page<Game> page = gameQueryService.findByCriteria(criteria, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
		return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
	@GetMapping("/games/active")
	public ResponseEntity<List<Game>> getAllActiveGames() {
	    log.debug("REST request to get active Games by criteria: {}");
    
        List<Game> page = gameService.findAllWithCoinEagerRelationships();
        return ResponseEntity.ok().body(page);
    }

	/**
    * {@code GET  /games/count} : count all the games.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
	@GetMapping("/games/count")
	public ResponseEntity<Long> countGames(GameCriteria criteria) {
		log.debug("REST request to count Games by criteria: {}", criteria);
		return ResponseEntity.ok().body(gameQueryService.countByCriteria(criteria));
	}

	/**
     * {@code GET  /games/:id} : get the "id" game.
     *
     * @param id the id of the game to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the game, or with status {@code 404 (Not Found)}.
     */
	@GetMapping("/games/{id}")
	public ResponseEntity<Game> getGame(@PathVariable Long id) {
		log.debug("REST request to get Game : {}", id);
		Optional<Game> game = gameService.findOne(id);
		return ResponseUtil.wrapOrNotFound(game);
	}

	/**
	 * {@code DELETE  /games/:id} : delete the "id" game.
	 *
	 * @param id the id of the game to delete.
	 * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
	 */
	@DeleteMapping("/games/{id}")
	public ResponseEntity<Void> deleteGame(@PathVariable Long id) {
		log.debug("REST request to delete Game : {}", id);
		gameService.delete(id);
		return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
	}

	/**
     * {@code SEARCH  /_search/games?query=:query} : search for the game corresponding
     * to the query.
     *
     * @param query the query of the game search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
	@GetMapping("/_search/games")
	public ResponseEntity<List<Game>> searchGames(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
		log.debug("REST request to search for a page of Games for query {}", query);
		Page<Game> page = gameService.search(query, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
		return ResponseEntity.ok().headers(headers).body(page.getContent());
	}

}
