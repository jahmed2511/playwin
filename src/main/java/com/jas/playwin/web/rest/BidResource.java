package com.jas.playwin.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.jas.playwin.domain.Bid;
import com.jas.playwin.domain.User;
import com.jas.playwin.domain.Wallet;
import com.jas.playwin.security.SecurityUtils;
import com.jas.playwin.service.BidQueryService;
import com.jas.playwin.service.BidService;
import com.jas.playwin.service.UserService;
import com.jas.playwin.service.WalletService;
import com.jas.playwin.service.dto.BidCriteria;
import com.jas.playwin.service.dto.BidDTO;
import com.jas.playwin.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.jas.playwin.domain.Bid}.
 */
@RestController
@RequestMapping("/api")
public class BidResource {

    private static final String ASIA_CALCUTTA = "Asia/Calcutta";

	private final Logger log = LoggerFactory.getLogger(BidResource.class);

    private static final String ENTITY_NAME = "bid";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BidService bidService;

    private final BidQueryService bidQueryService;
    
    private final UserService userService;
    
    private final WalletService walletService;
    

    public BidResource(BidService bidService, BidQueryService bidQueryService, UserService userService,WalletService walletService) {
        this.bidService = bidService;
        this.bidQueryService = bidQueryService;
        this.userService = userService;
        this.walletService = walletService;
    }

    /**
     * {@code POST  /bids} : Create a new bid.
     *
     * @param bid the bid to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new bid, or with status {@code 400 (Bad Request)} if the bid has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/bids")
    public ResponseEntity<Bid> createBid(@Valid @RequestBody Bid bid) throws URISyntaxException {
    	
    	checkTimeout(bid);
    	
        log.info("REST request to save Bid : {}", bid);
        if (bid.getId() != null) {
            throw new BadRequestAlertException("A new bid cannot already have an ID", ENTITY_NAME, "idexists");
        }
        String currUserName =SecurityUtils.getCurrentUserLogin().get();
        User currUser =userService.getUserWithAuthoritiesByLogin(currUserName).get();
        debitAmount(bid, currUser);
        bid.setUser(currUser);
        log.info("createBid setting user {0} to bid {}", currUserName);
        Bid result = bidService.save(bid);
        return ResponseEntity.created(new URI("/api/bids/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

	private void checkTimeout(Bid bid) {
		// current time -3 mint is before biding draw time
		if (LocalTime.from(bid.getBidTime().atZone(ZoneId.of(ASIA_CALCUTTA))).isBefore(
				LocalTime.from(Instant.now().atZone(ZoneId.of(ASIA_CALCUTTA)).plus(3, ChronoUnit.MINUTES)))) {
			throw new BadRequestAlertException("Timeout for this bid, select another time to bid", ENTITY_NAME,
					"timeout");
		}
		// game end time passed
		if (LocalTime.from(bid.getGame().getEndTime().atZone(ZoneId.of(ASIA_CALCUTTA)))
				.isBefore(LocalTime.from(Instant.now().atZone(ZoneId.of(ASIA_CALCUTTA))))) {
			throw new BadRequestAlertException("Game closed for today", ENTITY_NAME, "gameclosed");
		}
	}

	private void debitAmount(Bid bid, User currUser) {
		
		Wallet wallet= walletService.getRecentWalletByUser(currUser);
        Float coinCost =bid.getCoin().getBasePrice();
        int noOfCoin = bid.getNoOfCoins();
        Float totalCost=(noOfCoin*coinCost);
        if(bid.getId() != null) {
        	Optional<Bid> oldBid = bidService.findOne(bid.getId());
        	if(!oldBid.isPresent()) {
        		 throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        	}else {
        		noOfCoin = noOfCoin -oldBid.get().getNoOfCoins();
        	}
        	 totalCost =(noOfCoin*coinCost);	
        	
        }
        if(wallet==null || totalCost > wallet.getBalance() ) {
        	throw new BadRequestAlertException("Insufficient Balance in your", ENTITY_NAME, "lowBalnace");
        }else {
        	Wallet debitFromWallet = new Wallet();
        	if(totalCost < 0) {
        		debitFromWallet.credit(-(totalCost))
        		.remarks("credit  Amount from "+currUser.getLogin()+" against game "+bid.getGame().getGameName()+" of coin "+bid.getCoin().getDisplayName()+" || no.of coin"+bid.getNoOfCoins());
        	}else {
        		//Convert negative to positive 
        		debitFromWallet.debit(totalCost)
        		.remarks("debit  Amount from "+currUser.getLogin()+" against game "+bid.getGame().getGameName()+" of coin "+bid.getCoin().getDisplayName()+" || no.of coin"+bid.getNoOfCoins());
        	}
        	debitFromWallet.balance((wallet.getBalance() - totalCost))
        	.date(Instant.now().atZone(ZoneId.of("Asia/Kolkata")).toInstant())
        	.user(currUser);
        	
        	log.debug("debit Amount from "+currUser.getLogin()+" against game "+bid.getGame().getGameName()+" of coin "+bid.getCoin().getDisplayName()+" || no.of coin"+bid.getNoOfCoins());
        	debitFromWallet  =walletService.save(debitFromWallet);
        	if(Objects.isNull(debitFromWallet)) {
        		throw new BadRequestAlertException("Something went wrong please try again.", ENTITY_NAME, "internalServerError");
        	}
        	
        }
	}

    /**
     * {@code PUT  /bids} : Updates an existing bid.
     *
     * @param bid the bid to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bid,
     * or with status {@code 400 (Bad Request)} if the bid is not valid,
     * or with status {@code 500 (Internal Server Error)} if the bid couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/bids")
    public ResponseEntity<Bid> updateBid(@Valid @RequestBody Bid bid) throws URISyntaxException {
        log.debug("REST request to update Bid : {}", bid);
        if (bid.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        checkTimeout(bid);
        String currUserName =SecurityUtils.getCurrentUserLogin().get();
        User currUser =userService.getUserWithAuthoritiesByLogin(currUserName).get();
        bid.setUser(currUser);
        debitAmount(bid, currUser);
        Bid result = bidService.save(bid);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, bid.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /bids} : get all the bids.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bids in body.
     */
    @GetMapping("/bids")
    public ResponseEntity<List<Bid>> getAllBids(BidCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get Bids by criteria: {}", criteria);
        
        Page<Bid> page = bidQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /bids/count} : count all the bids.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/bids/count")
    public ResponseEntity<Long> countBids(BidCriteria criteria) {
        log.debug("REST request to count Bids by criteria: {}", criteria);
        return ResponseEntity.ok().body(bidQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /bids/:id} : get the "id" bid.
     *
     * @param id the id of the bid to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the bid, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/bids/{id}")
    public ResponseEntity<Bid> getBid(@PathVariable Long id) {
        log.debug("REST request to get Bid : {}", id);
        Optional<Bid> bid = bidService.findOne(id);
        return ResponseUtil.wrapOrNotFound(bid);
    }

    /**
     * {@code DELETE  /bids/:id} : delete the "id" bid.
     *
     * @param id the id of the bid to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/bids/{id}")
    public ResponseEntity<Void> deleteBid(@PathVariable Long id) {
        log.debug("REST request to delete Bid : {}", id);
        Bid bid = bidService.findOne(id).get();
        String currUserName =SecurityUtils.getCurrentUserLogin().get();
        User currUser =userService.getUserWithAuthoritiesByLogin(currUserName).get();
        bid.setUser(currUser);
        checkTimeout(bid);
        bid.noOfCoins(0);
        debitAmount(bid, currUser);
        bidService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/bids?query=:query} : search for the bid corresponding
     * to the query.
     *
     * @param query the query of the bid search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/bids")
    public ResponseEntity<List<Bid>> searchBids(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to search for a page of Bids for query {}", query);
        Page<Bid> page = bidService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    @GetMapping("/bids/undrow-bid-for-current-user")
    public ResponseEntity<List<Bid>> getUndrowBidsOfCurrentUser() {
        log.debug("REST request to get undrow bids for current user {}");
        List<Bid> page = bidService.findByCurrentUserAndUndrow();
        return ResponseEntity.ok().body(page);
    }
    @GetMapping("/bids/bid-for-publish")
    public ResponseEntity<List<Bid>> getBidForPublish() {
        log.debug("REST request to get bid for publish {}");
        List<Bid> page = bidService.getBidForPublish();
        return ResponseEntity.ok().body(page);
    }
    
    
    @GetMapping("/bids/report/{userlogin}")
    public ResponseEntity<List<BidDTO>> getUserBidReport(@PathVariable("userlogin") String userlogin) {
        log.debug("REST request to getUserBidReport for user  {}",userlogin);
        List<BidDTO> page = bidService.getByBidReport(userlogin);
        return ResponseEntity.ok().body(page);
    }
}
