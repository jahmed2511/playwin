package com.jas.playwin.web.rest;

import com.jas.playwin.domain.GameDrawConfig;
import com.jas.playwin.repository.GameDrawConfigRepository;
import com.jas.playwin.repository.GameRepository;
import com.jas.playwin.repository.search.GameDrawConfigSearchRepository;
import com.jas.playwin.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional; 
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.jas.playwin.domain.GameDrawConfig}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class GameDrawConfigResource {

    private final Logger log = LoggerFactory.getLogger(GameDrawConfigResource.class);

    private static final String ENTITY_NAME = "gameDrawConfig";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GameDrawConfigRepository gameDrawConfigRepository;

    private final GameDrawConfigSearchRepository gameDrawConfigSearchRepository;

    private final GameRepository gameRepository;

    public GameDrawConfigResource(GameDrawConfigRepository gameDrawConfigRepository, GameDrawConfigSearchRepository gameDrawConfigSearchRepository, GameRepository gameRepository) {
        this.gameDrawConfigRepository = gameDrawConfigRepository;
        this.gameDrawConfigSearchRepository = gameDrawConfigSearchRepository;
        this.gameRepository = gameRepository;
    }

    /**
     * {@code POST  /game-draw-configs} : Create a new gameDrawConfig.
     *
     * @param gameDrawConfig the gameDrawConfig to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new gameDrawConfig, or with status {@code 400 (Bad Request)} if the gameDrawConfig has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/game-draw-configs")
    public ResponseEntity<GameDrawConfig> createGameDrawConfig(@Valid @RequestBody GameDrawConfig gameDrawConfig) throws URISyntaxException {
        log.debug("REST request to save GameDrawConfig : {}", gameDrawConfig);
        if (gameDrawConfig.getId() != null) {
            throw new BadRequestAlertException("A new gameDrawConfig cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (Objects.isNull(gameDrawConfig.getGame())) {
            throw new BadRequestAlertException("Invalid association value provided", ENTITY_NAME, "null");
        }
       
        GameDrawConfig result = gameDrawConfigRepository.save(gameDrawConfig);
        gameDrawConfigSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/game-draw-configs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /game-draw-configs} : Updates an existing gameDrawConfig.
     *
     * @param gameDrawConfig the gameDrawConfig to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated gameDrawConfig,
     * or with status {@code 400 (Bad Request)} if the gameDrawConfig is not valid,
     * or with status {@code 500 (Internal Server Error)} if the gameDrawConfig couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/game-draw-configs")
    public ResponseEntity<GameDrawConfig> updateGameDrawConfig(@Valid @RequestBody GameDrawConfig gameDrawConfig) throws URISyntaxException {
        log.debug("REST request to update GameDrawConfig : {}", gameDrawConfig);
        if (gameDrawConfig.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if(gameDrawConfig.getGame().getId()==null) {
        	throw new BadRequestAlertException("invalid game id", ENTITY_NAME, "invalidGameid");
        }
        GameDrawConfig result = gameDrawConfigRepository.save(gameDrawConfig);
        gameDrawConfigSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, gameDrawConfig.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /game-draw-configs} : get all the gameDrawConfigs.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of gameDrawConfigs in body.
     */
    @GetMapping("/game-draw-configs")
    @Transactional(readOnly = true)
    public List<GameDrawConfig> getAllGameDrawConfigs() {
        log.debug("REST request to get all GameDrawConfigs");
        return gameDrawConfigRepository.findAll();
    }

    /**
     * {@code GET  /game-draw-configs/:id} : get the "id" gameDrawConfig.
     *
     * @param id the id of the gameDrawConfig to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the gameDrawConfig, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/game-draw-configs/{id}")
    @Transactional(readOnly = true)
    public ResponseEntity<GameDrawConfig> getGameDrawConfig(@PathVariable Long id) {
        log.debug("REST request to get GameDrawConfig : {}", id);
        Optional<GameDrawConfig> gameDrawConfig = gameDrawConfigRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(gameDrawConfig);
    }

    /**
     * {@code DELETE  /game-draw-configs/:id} : delete the "id" gameDrawConfig.
     *
     * @param id the id of the gameDrawConfig to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/game-draw-configs/{id}")
    public ResponseEntity<Void> deleteGameDrawConfig(@PathVariable Long id) {
        log.debug("REST request to delete GameDrawConfig : {}", id);
        gameDrawConfigRepository.deleteById(id);
        gameDrawConfigSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/game-draw-configs?query=:query} : search for the gameDrawConfig corresponding
     * to the query.
     *
     * @param query the query of the gameDrawConfig search.
     * @return the result of the search.
     */
    @GetMapping("/_search/game-draw-configs")
    public List<GameDrawConfig> searchGameDrawConfigs(@RequestParam String query) {
        log.debug("REST request to search GameDrawConfigs for query {}", query);
        return StreamSupport
            .stream(gameDrawConfigSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
