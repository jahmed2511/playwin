package com.jas.playwin.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.jas.playwin.domain.AgentCommision;
import com.jas.playwin.repository.AgentCommisionRepository;
import com.jas.playwin.repository.search.AgentCommisionSearchRepository;
import com.jas.playwin.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.jas.playwin.domain.AgentCommision}.
 */
@RestController
@RequestMapping("/api")
public class AgentCommisionResource {

    private final Logger log = LoggerFactory.getLogger(AgentCommisionResource.class);

    private static final String ENTITY_NAME = "agentCommision";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AgentCommisionRepository agentCommisionRepository;

    private final AgentCommisionSearchRepository agentCommisionSearchRepository;

    public AgentCommisionResource(AgentCommisionRepository agentCommisionRepository, AgentCommisionSearchRepository agentCommisionSearchRepository) {
        this.agentCommisionRepository = agentCommisionRepository;
        this.agentCommisionSearchRepository = agentCommisionSearchRepository;
    }

    /**
     * {@code POST  /agent-commisions} : Create a new agentCommision.
     *
     * @param agentCommision the agentCommision to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new agentCommision, or with status {@code 400 (Bad Request)} if the agentCommision has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/agent-commisions")
    public ResponseEntity<AgentCommision> createAgentCommision(@Valid @RequestBody AgentCommision agentCommision) throws URISyntaxException {
        log.debug("REST request to save AgentCommision : {}", agentCommision);
        if (agentCommision.getId() != null) {
            throw new BadRequestAlertException("A new agentCommision cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if(agentCommisionRepository.commisionExist(agentCommision.getGame().getId(), 
        		agentCommision.getCoin().getId(),agentCommision.getUser().getId()).isPresent()) {
        	throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "commisionExist");
        }
        AgentCommision result = agentCommisionRepository.save(agentCommision);
        agentCommisionSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/agent-commisions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /agent-commisions} : Updates an existing agentCommision.
     *
     * @param agentCommision the agentCommision to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated agentCommision,
     * or with status {@code 400 (Bad Request)} if the agentCommision is not valid,
     * or with status {@code 500 (Internal Server Error)} if the agentCommision couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/agent-commisions")
    public ResponseEntity<AgentCommision> updateAgentCommision(@Valid @RequestBody AgentCommision agentCommision) throws URISyntaxException {
        log.debug("REST request to update AgentCommision : {}", agentCommision);
        if (agentCommision.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if(agentCommisionRepository.commisionExist(agentCommision.getGame().getId(), 
        		agentCommision.getCoin().getId(),agentCommision.getUser().getId(),agentCommision.getId()).isPresent()) {
        	throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "commisionExist");
        }
        AgentCommision result = agentCommisionRepository.save(agentCommision);
        agentCommisionSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, agentCommision.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /agent-commisions} : get all the agentCommisions.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of agentCommisions in body.
     */
    @GetMapping("/agent-commisions")
    public ResponseEntity<List<AgentCommision>> getAllAgentCommisions(Pageable pageable) {
        log.debug("REST request to get a page of AgentCommisions");
        Page<AgentCommision> page = agentCommisionRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /agent-commisions/:id} : get the "id" agentCommision.
     *
     * @param id the id of the agentCommision to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the agentCommision, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/agent-commisions/{id}")
    public ResponseEntity<AgentCommision> getAgentCommision(@PathVariable Long id) {
        log.debug("REST request to get AgentCommision : {}", id);
        Optional<AgentCommision> agentCommision = agentCommisionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(agentCommision);
    }

    /**
     * {@code DELETE  /agent-commisions/:id} : delete the "id" agentCommision.
     *
     * @param id the id of the agentCommision to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/agent-commisions/{id}")
    public ResponseEntity<Void> deleteAgentCommision(@PathVariable Long id) {
        log.debug("REST request to delete AgentCommision : {}", id);
        agentCommisionRepository.deleteById(id);
        agentCommisionSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/agent-commisions?query=:query} : search for the agentCommision corresponding
     * to the query.
     *
     * @param query the query of the agentCommision search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/agent-commisions")
    public ResponseEntity<List<AgentCommision>> searchAgentCommisions(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of AgentCommisions for query {}", query);
        Page<AgentCommision> page = agentCommisionSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
