package com.jas.playwin.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.jas.playwin.domain.Authority;
import com.jas.playwin.domain.User;
import com.jas.playwin.domain.Wallet;
import com.jas.playwin.security.AuthoritiesConstants;
import com.jas.playwin.security.SecurityUtils;
import com.jas.playwin.service.UserService;
import com.jas.playwin.service.WalletQueryService;
import com.jas.playwin.service.WalletService;
import com.jas.playwin.service.dto.WalletCriteria;
import com.jas.playwin.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.jas.playwin.domain.Wallet}.
 */
@RestController
@RequestMapping("/api")
public class WalletResource {

    private final Logger log = LoggerFactory.getLogger(WalletResource.class);

    private static final String ENTITY_NAME = "wallet";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WalletService walletService;

    private final WalletQueryService walletQueryService;

    private final UserService userService;

    public WalletResource(WalletService walletService, WalletQueryService walletQueryService, UserService userService) {
        this.walletService = walletService;
        this.walletQueryService = walletQueryService; 
        this.userService  = userService;
    }

    /**
     * {@code POST  /wallets} : Create a new wallet.
     *
     * @param wallet the wallet to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new wallet, or with status {@code 400 (Bad Request)} if the wallet has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/wallets")
    public ResponseEntity<Wallet> createWallet(@Valid @RequestBody Wallet wallet) throws URISyntaxException {
        log.debug("REST request to save Wallet : {}", wallet);
        if (wallet.getId() != null) {
            throw new BadRequestAlertException("A new wallet cannot already have an ID", ENTITY_NAME, "idexists");
        }
        User reciverUser = wallet.getUser();
        String currUserName =SecurityUtils.getCurrentUserLogin().get();
        User currUser =userService.getUserWithAuthoritiesByLogin(currUserName).get();
        Wallet senderUserBalance = walletService.getRecentWalletByUser(currUser);
        Wallet reciverUserBalance = walletService.getRecentWalletByUser(reciverUser);
        
        if(senderUserBalance.getBalance() > wallet.getDebit() ) {
	        Wallet transferFromWallet = new Wallet();
	        transferFromWallet.setBalance(senderUserBalance.getBalance() - wallet.getDebit());
	        transferFromWallet.setDebit(wallet.getDebit());
	        transferFromWallet.setDate(Instant.now().atZone(ZoneId.of("Asia/Kolkata")).toInstant());
	        transferFromWallet.setRemarks("Transferd amount from "+currUserName+"'s account to "+reciverUser.getLogin()+"'s account || remarks "+wallet.getRemarks());
	        transferFromWallet.setUser(currUser);
	        
	        Wallet transferToWallet = new Wallet();
	        if(reciverUserBalance!= null && reciverUserBalance.getBalance() != null) {
	        	transferToWallet.setBalance(reciverUserBalance.getBalance() + wallet.getDebit());
	        }else {
	        	transferToWallet.setBalance(wallet.getDebit());
	        }
	        
	        transferToWallet.setCredit( wallet.getDebit());
	        transferToWallet.setDate(Instant.now().atZone(ZoneId.of("Asia/Kolkata")).toInstant());
	        transferToWallet.setRemarks("Transferd amount from "+currUserName+"'s account to "+reciverUser.getLogin()+"'s account || remarks "+wallet.getRemarks());
	        transferToWallet.setUser(reciverUser);
	        
	        senderUserBalance= walletService.save(transferFromWallet);
	        reciverUserBalance = walletService.save(transferToWallet);
        }else {
        	throw new BadRequestAlertException("Insuffieciet Balance in your wallet", ENTITY_NAME, "lowBalnace");
        }
       // Wallet result = walletService.save(wallet);
        return ResponseEntity.created(new URI("/api/wallets/" + senderUserBalance.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, senderUserBalance.getId().toString()))
            .body(senderUserBalance);
    }


    /**
     * {@code PUT  /wallets} : Updates an existing wallet.
     *
     * @param wallet the wallet to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated wallet,
     * or with status {@code 400 (Bad Request)} if the wallet is not valid,
     * or with status {@code 500 (Internal Server Error)} if the wallet couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/wallets")
    public ResponseEntity<Wallet> updateWallet(@Valid @RequestBody Wallet wallet) throws URISyntaxException {
        log.debug("REST request to update Wallet : {}", wallet);
        if (wallet.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Wallet result = walletService.save(wallet);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, wallet.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /wallets} : get all the wallets.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of wallets in body.
     */
    @GetMapping("/wallets")
    public ResponseEntity<List<Wallet>> getAllWallets(WalletCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get Wallets by criteria: {}", criteria);
        String currUserName =SecurityUtils.getCurrentUserLogin().get();
        Authority adminAuth = new Authority();
        adminAuth.setName(AuthoritiesConstants.ADMIN);
        User currUser =userService.getUserWithAuthoritiesByLogin(currUserName).get();
    	LongFilter lFilter=new LongFilter();
        lFilter.setEquals(currUser.getId());
        if(currUser.getAuthorities()==null || !currUser.getAuthorities().contains(adminAuth)) {
            criteria.setUserId(lFilter);
        }else {
        	final List<String> idList=queryParams.get("criteria.userId");
            if(idList!=null && !idList.isEmpty()) {
            	lFilter.setEquals(Long.parseLong(idList.get(0)));
                criteria.setUserId(lFilter);
            }else {
            	criteria.setUserId(lFilter);
            }
        }
        pageable = PageRequest.of(pageable.getPageNumber(),pageable.getPageSize(),Sort.by("date").descending());
        Page<Wallet> page = walletQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /wallets/count} : count all the wallets.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/wallets/count")
    public ResponseEntity<Long> countWallets(WalletCriteria criteria) {
        log.debug("REST request to count Wallets by criteria: {}", criteria);
        return ResponseEntity.ok().body(walletQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /wallets/:id} : get the "id" wallet.
     *
     * @param id the id of the wallet to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the wallet, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/wallets/{id}")
    public ResponseEntity<Wallet> getWallet(@PathVariable Long id) {
        log.debug("REST request to get Wallet : {}", id);
        Optional<Wallet> wallet = walletService.findOne(id);
        return ResponseUtil.wrapOrNotFound(wallet);
    }

    /**
     * {@code DELETE  /wallets/:id} : delete the "id" wallet.
     *
     * @param id the id of the wallet to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/wallets/{id}")
    public ResponseEntity<Void> deleteWallet(@PathVariable Long id) {
        log.debug("REST request to delete Wallet : {}", id);
        walletService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/wallets?query=:query} : search for the wallet corresponding
     * to the query.
     *
     * @param query the query of the wallet search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/wallets")
    public ResponseEntity<List<Wallet>> searchWallets(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to search for a page of Wallets for query {}", query);
        Page<Wallet> page = walletService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
