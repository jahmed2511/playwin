/**
 * View Models used by Spring MVC REST controllers.
 */
package com.jas.playwin.web.rest.vm;
