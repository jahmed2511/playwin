package com.jas.playwin.web.rest;

import com.jas.playwin.domain.SmsConfig;
import com.jas.playwin.service.SmsConfigService;
import com.jas.playwin.web.rest.errors.BadRequestAlertException;
import com.jas.playwin.service.dto.SmsConfigCriteria;
import com.jas.playwin.service.SmsConfigQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.jas.playwin.domain.SmsConfig}.
 */
@RestController
@RequestMapping("/api")
public class SmsConfigResource {

    private final Logger log = LoggerFactory.getLogger(SmsConfigResource.class);

    private static final String ENTITY_NAME = "smsConfig";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SmsConfigService smsConfigService;

    private final SmsConfigQueryService smsConfigQueryService;

    public SmsConfigResource(SmsConfigService smsConfigService, SmsConfigQueryService smsConfigQueryService) {
        this.smsConfigService = smsConfigService;
        this.smsConfigQueryService = smsConfigQueryService;
    }

    /**
     * {@code POST  /sms-configs} : Create a new smsConfig.
     *
     * @param smsConfig the smsConfig to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new smsConfig, or with status {@code 400 (Bad Request)} if the smsConfig has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/sms-configs")
    public ResponseEntity<SmsConfig> createSmsConfig(@Valid @RequestBody SmsConfig smsConfig) throws URISyntaxException {
        log.debug("REST request to save SmsConfig : {}", smsConfig);
        if (smsConfig.getId() != null) {
            throw new BadRequestAlertException("A new smsConfig cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SmsConfig result = smsConfigService.save(smsConfig);
        return ResponseEntity.created(new URI("/api/sms-configs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /sms-configs} : Updates an existing smsConfig.
     *
     * @param smsConfig the smsConfig to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated smsConfig,
     * or with status {@code 400 (Bad Request)} if the smsConfig is not valid,
     * or with status {@code 500 (Internal Server Error)} if the smsConfig couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/sms-configs")
    public ResponseEntity<SmsConfig> updateSmsConfig(@Valid @RequestBody SmsConfig smsConfig) throws URISyntaxException {
        log.debug("REST request to update SmsConfig : {}", smsConfig);
        if (smsConfig.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SmsConfig result = smsConfigService.save(smsConfig);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, smsConfig.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /sms-configs} : get all the smsConfigs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of smsConfigs in body.
     */
    @GetMapping("/sms-configs")
    public ResponseEntity<List<SmsConfig>> getAllSmsConfigs(SmsConfigCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get SmsConfigs by criteria: {}", criteria);
        Page<SmsConfig> page = smsConfigQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /sms-configs/count} : count all the smsConfigs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/sms-configs/count")
    public ResponseEntity<Long> countSmsConfigs(SmsConfigCriteria criteria) {
        log.debug("REST request to count SmsConfigs by criteria: {}", criteria);
        return ResponseEntity.ok().body(smsConfigQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /sms-configs/:id} : get the "id" smsConfig.
     *
     * @param id the id of the smsConfig to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the smsConfig, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/sms-configs/{id}")
    public ResponseEntity<SmsConfig> getSmsConfig(@PathVariable Long id) {
        log.debug("REST request to get SmsConfig : {}", id);
        Optional<SmsConfig> smsConfig = smsConfigService.findOne(id);
        return ResponseUtil.wrapOrNotFound(smsConfig);
    }

    /**
     * {@code DELETE  /sms-configs/:id} : delete the "id" smsConfig.
     *
     * @param id the id of the smsConfig to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/sms-configs/{id}")
    public ResponseEntity<Void> deleteSmsConfig(@PathVariable Long id) {
        log.debug("REST request to delete SmsConfig : {}", id);
        smsConfigService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/sms-configs?query=:query} : search for the smsConfig corresponding
     * to the query.
     *
     * @param query the query of the smsConfig search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/sms-configs")
    public ResponseEntity<List<SmsConfig>> searchSmsConfigs(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to search for a page of SmsConfigs for query {}", query);
        Page<SmsConfig> page = smsConfigService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
