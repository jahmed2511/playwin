package com.jas.playwin.web.rest;

import com.jas.playwin.domain.Coin;
import com.jas.playwin.service.CoinService;
import com.jas.playwin.web.rest.errors.BadRequestAlertException;
import com.jas.playwin.service.dto.CoinCriteria;
import com.jas.playwin.service.CoinQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.jas.playwin.domain.Coin}.
 */
@RestController
@RequestMapping("/api")
public class CoinResource {

    private final Logger log = LoggerFactory.getLogger(CoinResource.class);

    private static final String ENTITY_NAME = "coin";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CoinService coinService;

    private final CoinQueryService coinQueryService;

    public CoinResource(CoinService coinService, CoinQueryService coinQueryService) {
        this.coinService = coinService;
        this.coinQueryService = coinQueryService;
    }

    /**
     * {@code POST  /coins} : Create a new coin.
     *
     * @param coin the coin to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new coin, or with status {@code 400 (Bad Request)} if the coin has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/coins")
    public ResponseEntity<Coin> createCoin(@Valid @RequestBody Coin coin) throws URISyntaxException {
        log.debug("REST request to save Coin : {}", coin);
        if (coin.getId() != null) {
            throw new BadRequestAlertException("A new coin cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Coin result = coinService.save(coin);
        return ResponseEntity.created(new URI("/api/coins/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /coins} : Updates an existing coin.
     *
     * @param coin the coin to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated coin,
     * or with status {@code 400 (Bad Request)} if the coin is not valid,
     * or with status {@code 500 (Internal Server Error)} if the coin couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/coins")
    public ResponseEntity<Coin> updateCoin(@Valid @RequestBody Coin coin) throws URISyntaxException {
        log.debug("REST request to update Coin : {}", coin);
        if (coin.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Coin result = coinService.save(coin);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, coin.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /coins} : get all the coins.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of coins in body.
     */
    @GetMapping("/coins")
    public ResponseEntity<List<Coin>> getAllCoins(CoinCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get Coins by criteria: {}", criteria);
        Page<Coin> page = coinQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    @GetMapping("/game-coins/{id}")
    public ResponseEntity<List<Coin>> getGameCoin(@PathVariable Long id) {
        log.debug("REST request to get Coin : {}", id);
        List<Coin> coins = coinService.findByGameId(id);
        return ResponseEntity.ok().body(coins);
    }

    /**
    * {@code GET  /coins/count} : count all the coins.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/coins/count")
    public ResponseEntity<Long> countCoins(CoinCriteria criteria) {
        log.debug("REST request to count Coins by criteria: {}", criteria);
        return ResponseEntity.ok().body(coinQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /coins/:id} : get the "id" coin.
     *
     * @param id the id of the coin to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the coin, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/coins/{id}")
    public ResponseEntity<Coin> getCoin(@PathVariable Long id) {
        log.debug("REST request to get Coin : {}", id);
        Optional<Coin> coin = coinService.findOne(id);
        return ResponseUtil.wrapOrNotFound(coin);
    }

    /**
     * {@code DELETE  /coins/:id} : delete the "id" coin.
     *
     * @param id the id of the coin to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/coins/{id}")
    public ResponseEntity<Void> deleteCoin(@PathVariable Long id) {
        log.debug("REST request to delete Coin : {}", id);
        coinService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/coins?query=:query} : search for the coin corresponding
     * to the query.
     *
     * @param query the query of the coin search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/coins")
    public ResponseEntity<List<Coin>> searchCoins(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to search for a page of Coins for query {}", query);
        Page<Coin> page = coinService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
