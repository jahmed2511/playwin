package com.jas.playwin.web.rest.vm;

import com.jas.playwin.service.dto.UserDTO;
import javax.validation.constraints.Size;

/**
 * View Model extending the UserDTO, which is meant to be used in the user management UI.
 */
public class ManagedUserVM extends UserDTO {

    public static final int PASSWORD_MIN_LENGTH = 4;

    public static final int PASSWORD_MAX_LENGTH = 100;

    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
    private String password;
    
    private String mobileNo;
    
    private String distributorShare;
    
    private Boolean isSpecificPrice; 

    public ManagedUserVM() {
        // Empty constructor needed for Jackson.
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getDistributorShare() {
		return distributorShare;
	}

	public void setDistributorShare(String distributorShare) {
		this.distributorShare = distributorShare;
	}

	

	public Boolean getIsSpecificPrice() {
		return isSpecificPrice;
	}

	public void setIsSpecificPrice(Boolean isSpecificPrice) {
		this.isSpecificPrice = isSpecificPrice;
	}

	@Override
    public String toString() {
        return "ManagedUserVM{" +
            "} " + super.toString();
    }
}
