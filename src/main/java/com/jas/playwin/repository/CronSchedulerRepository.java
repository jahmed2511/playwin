package com.jas.playwin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jas.playwin.domain.CronScheduler;

/**
 * Spring Data repository for the CronScheduler entity.
 */
@Repository
public interface CronSchedulerRepository extends JpaRepository<CronScheduler, Long> {

//	@Query("delete from CronScheduler cs where cs.game_id=:gameId")
//	void deleteByGameId(Long gameId);

	void deleteByGameId(Long gameId);
}
