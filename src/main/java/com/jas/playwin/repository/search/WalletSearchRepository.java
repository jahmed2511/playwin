package com.jas.playwin.repository.search;

import com.jas.playwin.domain.Wallet;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Wallet} entity.
 */
public interface WalletSearchRepository extends ElasticsearchRepository<Wallet, Long> {
}
