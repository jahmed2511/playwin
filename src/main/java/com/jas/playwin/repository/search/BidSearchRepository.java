package com.jas.playwin.repository.search;

import com.jas.playwin.domain.Bid;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Bid} entity.
 */
public interface BidSearchRepository extends ElasticsearchRepository<Bid, Long> {
}
