package com.jas.playwin.repository.search;

import com.jas.playwin.domain.SmsConfig;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link SmsConfig} entity.
 */
public interface SmsConfigSearchRepository extends ElasticsearchRepository<SmsConfig, Long> {
}
