package com.jas.playwin.repository.search;

import com.jas.playwin.domain.UserPref;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link UserPref} entity.
 */
public interface UserPrefSearchRepository extends ElasticsearchRepository<UserPref, Long> {
}
