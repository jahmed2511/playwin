package com.jas.playwin.repository.search;

import com.jas.playwin.domain.Game;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Game} entity.
 */
public interface GameSearchRepository extends ElasticsearchRepository<Game, Long> {
}
