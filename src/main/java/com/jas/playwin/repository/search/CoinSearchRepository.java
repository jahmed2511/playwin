package com.jas.playwin.repository.search;

import com.jas.playwin.domain.Coin;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Coin} entity.
 */
public interface CoinSearchRepository extends ElasticsearchRepository<Coin, Long> {
}
