package com.jas.playwin.repository.search;
import com.jas.playwin.domain.AgentCommision;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link AgentCommision} entity.
 */
public interface AgentCommisionSearchRepository extends ElasticsearchRepository<AgentCommision, Long> {
}
