package com.jas.playwin.repository.search;

import com.jas.playwin.domain.GameResult;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link GameResult} entity.
 */
public interface GameResultSearchRepository extends ElasticsearchRepository<GameResult, Long> {
}
