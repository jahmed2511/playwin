package com.jas.playwin.repository.search;

import com.jas.playwin.domain.Redeem;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Redeem} entity.
 */
public interface RedeemSearchRepository extends ElasticsearchRepository<Redeem, Long> {
}
