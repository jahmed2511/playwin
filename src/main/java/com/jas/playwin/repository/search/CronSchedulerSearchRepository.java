package com.jas.playwin.repository.search;

import com.jas.playwin.domain.CronScheduler;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link CronScheduler} entity.
 */
public interface CronSchedulerSearchRepository extends ElasticsearchRepository<CronScheduler, Long> {
}
