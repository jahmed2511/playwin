package com.jas.playwin.repository.search;
import com.jas.playwin.domain.GameDrawConfig;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link GameDrawConfig} entity.
 */
public interface GameDrawConfigSearchRepository extends ElasticsearchRepository<GameDrawConfig, Long> {
}
