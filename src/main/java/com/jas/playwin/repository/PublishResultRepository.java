package com.jas.playwin.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jas.playwin.domain.PublishResult;

@Repository
public interface PublishResultRepository
		extends JpaRepository<PublishResult, Long>, JpaSpecificationExecutor<PublishResult> {

	@Query("select pr from PublishResult pr where pr.gameId = :gameId and pr.coinId= :coinId and pr.publishDate=:publishDate and pr.publishTime=:publishTime")
	List<PublishResult> findByGameIdAndCoinIdAndDateAndTime(@Param("gameId") Long gameId, @Param("coinId") Long coinId,
			@Param("publishDate") LocalDate publishDate, @Param("publishTime") String publishTime);

}
