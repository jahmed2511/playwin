package com.jas.playwin.repository;

import com.jas.playwin.domain.SmsConfig;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the SmsConfig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SmsConfigRepository extends JpaRepository<SmsConfig, Long>, JpaSpecificationExecutor<SmsConfig> {

    @Query("select smsConfig from SmsConfig smsConfig where smsConfig.user.login = ?#{principal.username}")
    List<SmsConfig> findByUserIsCurrentUser();

}
