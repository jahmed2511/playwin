package com.jas.playwin.repository;

import com.jas.playwin.domain.Redeem;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Redeem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RedeemRepository extends JpaRepository<Redeem, Long>, JpaSpecificationExecutor<Redeem> {

    @Query("select redeem from Redeem redeem where redeem.user.login = ?#{principal.username}")
    List<Redeem> findByUserIsCurrentUser();

}
