package com.jas.playwin.repository;

import com.jas.playwin.domain.Authority;
import com.jas.playwin.domain.User;

import org.springframework.data.domain.Page;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.time.Instant;

/**
 * Spring Data JPA repository for the {@link User} entity.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findOneByActivationKey(String activationKey);

    List<User> findAllByActivatedIsFalseAndCreatedDateBefore(Instant dateTime);

    Optional<User> findOneByResetKey(String resetKey);

    Optional<User> findOneByEmailIgnoreCase(String email);

    Optional<User> findOneByLogin(String login);

    @EntityGraph(attributePaths = "authorities")
    Optional<User> findOneWithAuthoritiesById(Long id);

    @EntityGraph(attributePaths = "authorities")
    Optional<User> findOneWithAuthoritiesByLogin(String login);

    @EntityGraph(attributePaths = "authorities")
    Optional<User> findOneWithAuthoritiesByEmail(String email);

    Page<User> findAllByLoginNot(Pageable pageable, String login);
    
    @Query(value="select user from User user left join fetch user.authorities where user.id != :loginId and user.createdBy = :createdBy",
    		countQuery="select count(user) from User user where user.id != :loginId and user.createdBy = :createdBy")
    Page<User> findAllByCreatedByAndNotLogin(@Param("createdBy") String createdBy, @Param("loginId") Long currentId,Pageable pageable);
   
    @Query(value="select user from User user left join fetch user.authorities where user.login != :login and user.createdBy = :createdBy",
    		countQuery="select count(user) from User user where user.login != :login and user.createdBy = :createdBy")
    Page<User> findAllByCreatedByAndNotLoginUser(@Param("createdBy") String createdBy, @Param("login") String login,Pageable pageable);
   
    
    @EntityGraph(attributePaths = "authorities")
    List<User> findAllWithAuthoritiesEagerRelationshipsByAuthorities(List<Authority> authorities);
}
