package com.jas.playwin.repository;

import com.jas.playwin.domain.Bid;
import com.jas.playwin.service.dto.PublishDTO;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

/**
 * Spring Data repository for the Bid entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BidRepository extends JpaRepository<Bid, Long>, JpaSpecificationExecutor<Bid> {

	@Query("select bid from Bid bid where bid.user.login = ?#{principal.username} order by isDrowHappan desc")
	List<Bid> findByUserIsCurrentUser();

	@Query("select bid from Bid bid where bid.user.login = ?#{principal.username} and isDrowHappan = false")
	List<Bid> findByUserIsCurrentUserAndDrowNotHappen();

	@Query("select bid from Bid bid where bid.game.id = :gameId and bid.isDrowHappan = :isDrowHappan and bid.bidTime <= :bidTime")
	List<Bid> findAllByGameIdAndIsDrowHappan(@Param("gameId") Long gameId, @Param("isDrowHappan") boolean isDrowHappan,
			@Param("bidTime") Instant bidTime);

	@Query("select bid from Bid bid where bid.user.login = :login order by isDrowHappan desc")
	List<Bid> findByUserLogin(@Param("login") String login);

	@Query(value = "SELECT id, bid_no, bid_time, is_drow_happan, is_winner, SUM(no_of_coins) as no_of_coins,remarks, coin_id,game_id,user_id FROM bid b where is_drow_happan= false group by game_id,coin_id,bid_no,bid_time", nativeQuery = true)
	List<Bid> findAllForPublishByTime();

	@Query(value = "select * from Bid  where game_id = :gameId and coin_id = :coinId and is_drow_happan = :isDrawHappen and bid_time <= :bidTime order by no_of_coins asc  limit 0, 1;", nativeQuery = true)
	Bid findDrawNumberWithMinimumBid(@Param("gameId") Long gameId, @Param("coinId") Long coinId,
			@Param("isDrawHappen") boolean isDrawHappen, @Param("bidTime") Instant bidTime);

	@Query(value = "select * from Bid where game_id= :gameId and coin_id = :coinId and is_drow_happan = :isDrawHappen and bid_time <= :bidTime order by no_of_coins desc  limit 0, 1", nativeQuery = true)
	Bid findDrawNumberWithMaximumBid(@Param("gameId") Long gameId, @Param("coinId") Long coinId,
			@Param("isDrawHappen") boolean isDrawHappen, @Param("bidTime") Instant bidTime);

}
