package com.jas.playwin.repository;
import com.jas.playwin.domain.GameDrawConfig;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the GameDrawConfig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GameDrawConfigRepository extends JpaRepository<GameDrawConfig, Long> {

    @Query("select distinct gamedraw from GameDrawConfig gamedraw left join fetch gamedraw.game where gamedraw.game.id = :id")
	Optional<GameDrawConfig> findByGameId(@Param("id") Long id);

	@Query("select distinct gamedraw from GameDrawConfig gamedraw left join fetch gamedraw.game where gamedraw.id = :id")
	Optional<GameDrawConfig> findByIdWithGameEagerRelationships(@Param("id") Long id);

	@Query("select distinct gamedraw from GameDrawConfig gamedraw left join fetch gamedraw.game left join fetch gamedraw.coin where gamedraw.game.id = :gameId and gamedraw.coin.id = :coinId")
	List<GameDrawConfig> findByGameIdAndCoinId(@Param("gameId") Long gameId, @Param("coinId") Long coinId); 

    
    
}
