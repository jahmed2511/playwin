package com.jas.playwin.repository;

import com.jas.playwin.domain.Game;
import com.jas.playwin.domain.GameResult;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the GameResult entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GameResultRepository extends JpaRepository<GameResult, Long>, JpaSpecificationExecutor<GameResult> {
	
	GameResult findTopByGameOrderByGameTimeDesc(Game game);

}
