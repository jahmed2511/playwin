package com.jas.playwin.repository;

import com.jas.playwin.domain.User;
import com.jas.playwin.domain.Wallet;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Wallet entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WalletRepository extends JpaRepository<Wallet, Long>, JpaSpecificationExecutor<Wallet> {

	@Query("select wallet from Wallet wallet where wallet.user.login = ?#{principal.username}")
	List<Wallet> findByUserIsCurrentUser();

	Wallet findTopByUserOrderByDateDesc(User userName);

	Wallet findByUserId(Long userId);

}
