package com.jas.playwin.repository;
import com.jas.playwin.domain.AgentCommision;

import java.util.Optional;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AgentCommision entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgentCommisionRepository extends JpaRepository<AgentCommision, Long> {
	
	@Query("select ac from AgentCommision ac where ac.game.id = :gameId and ac.coin.id = :coinId and ac.user.id = :userId")
	Optional<AgentCommision> commisionExist(@Param("gameId") Long gameId,@Param("coinId") Long coinId,@Param("userId") Long userId );
	
	@Query("select ac from AgentCommision ac where ac.game.id = :gameId and ac.coin.id = :coinId and ac.user.id = :userId and ac.id != :id")
	Optional<AgentCommision> commisionExist(@Param("gameId") Long gameId,@Param("coinId") Long coinId,@Param("userId") Long userId,@Param("id") Long id );

}
