package com.jas.playwin.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jas.playwin.domain.User;
import com.jas.playwin.domain.UserPref;


/**
 * Spring Data  repository for the UserPref entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserPrefRepository extends JpaRepository<UserPref, Long>, JpaSpecificationExecutor<UserPref> {

	@Query("select userPref from UserPref userPref where userPref.user.id =:id")
	Optional<UserPref> findOneByUserId(@Param("id") Long id);

	List<UserPref> findByUserIn(List<User> ids);


	@Modifying
	@Query("delete From UserPref where user_id = :userId")
	void deleteByUserLogin(@Param("userId") Long userId);
 
}
