package com.jas.playwin.repository;

import com.jas.playwin.domain.Coin;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Coin entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CoinRepository extends JpaRepository<Coin, Long>, JpaSpecificationExecutor<Coin> {

    @Query("select coin from Coin coin where coin.user.login = ?#{principal.username}")
    List<Coin> findByUserIsCurrentUser();
    
    @Query("select coin from Coin coin where coin.game.id = :id")
    List<Coin> findByGameId(@Param("id") Long id);

}
