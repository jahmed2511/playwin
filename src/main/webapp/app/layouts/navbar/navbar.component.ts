import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiLanguageService } from 'ng-jhipster';
import { SessionStorageService } from 'ngx-webstorage';
import { UserService } from 'app/core/user/user.service';
import { VERSION } from 'app/app.constants';
import { JhiLanguageHelper, User, IUser, AccountService, LoginModalService, LoginService } from 'app/core';
import { ProfileService } from 'app/layouts/profiles/profile.service';
import { WalletService } from 'app/entities/wallet/wallet.service';
import { Wallet } from 'app/shared/model/wallet.model';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'jhi-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['navbar.scss']
})
export class NavbarComponent implements OnInit {
  inProduction: boolean;
  isNavbarCollapsed: boolean;
  languages: any[];
  swaggerEnabled: boolean;
  modalRef: NgbModalRef;
  version: string;

  constructor(
    private loginService: LoginService,
    private languageService: JhiLanguageService,
    private languageHelper: JhiLanguageHelper,
    private sessionStorage: SessionStorageService,
    private accountService: AccountService,
    private loginModalService: LoginModalService,
    private profileService: ProfileService,
    private walletService: WalletService,
    private router: Router,
    private userService: UserService
  ) {
    this.version = VERSION ? 'v' + VERSION : '';
    this.isNavbarCollapsed = true;
  }
  currentAccount: any;
  currUser: IUser;
  wallet: Wallet;
  ngOnInit() {
    console.log('....navbar..........');
    console.log('....navbar.....getAccountAndUpdatWallet.....');
    this.getAccountAndUpdatWallet();
    this.languageHelper.getAll().then(languages => {
      this.languages = languages;
    });

    this.profileService.getProfileInfo().then(profileInfo => {
      this.inProduction = profileInfo.inProduction;
      this.swaggerEnabled = profileInfo.swaggerEnabled;
    });
    if (!this.isAuthenticated()) {
      return;
    }
    this.walletService.walletUpdateEventEmmiter.subscribe(res => {
      console.log('recived notification ....');
      this.updateWallet();
    });
  }
  getAccountAndUpdatWallet() {
    console.log('getAccountAndUpdatWallet....');
    this.accountService.identity().then(account => {
      this.currentAccount = account;
      console.log('nav bar................ accunt return');
      this.userService.find(account.login).subscribe(resp => {
        console.log('getting user.......');
        console.log(this.currUser);
        this.currUser = resp.body;
        this.updateWallet();
      });
    });
  }
  updateWallet() {
    console.log('fecting walleted data.');
    this.walletService
      .query({
        page: 0,
        size: 1,
        sort: ['date' + ',' + 'desc'],
        'criteria.userId': this.currUser.id
      })
      .subscribe(
        (res: HttpResponse<Wallet[]>) => {
          this.wallet = res.body[0];
          console.log(this.wallet + 'upfate walllete....');
        },
        (res: HttpResponse<Wallet>) => alert(res.body)
      );
  }
  reloadWalltet() {
    this.getAccountAndUpdatWallet();
  }
  changeLanguage(languageKey: string) {
    this.sessionStorage.store('locale', languageKey);
    this.languageService.changeLanguage(languageKey);
  }

  collapseNavbar() {
    this.isNavbarCollapsed = true;
  }

  isAuthenticated() {
    return this.accountService.isAuthenticated();
  }

  login() {
    this.modalRef = this.loginModalService.open();
  }

  logout() {
    this.collapseNavbar();
    this.loginService.logout();
    this.router.navigate(['']);
  }

  toggleNavbar() {
    this.isNavbarCollapsed = !this.isNavbarCollapsed;
  }

  getImageUrl() {
    return this.isAuthenticated() ? this.accountService.getImageUrl() : null;
  }
}
