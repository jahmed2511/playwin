import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IGameDrawConfig } from 'app/shared/model/game-draw-config.model';
import { GameDrawConfigService } from './game-draw-config.service';

@Component({
  selector: 'jhi-game-draw-config-delete-dialog',
  templateUrl: './game-draw-config-delete-dialog.component.html'
})
export class GameDrawConfigDeleteDialogComponent {
  gameDrawConfig: IGameDrawConfig;

  constructor(
    protected gameDrawConfigService: GameDrawConfigService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.gameDrawConfigService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'gameDrawConfigListModification',
        content: 'Deleted an gameDrawConfig'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-game-draw-config-delete-popup',
  template: ''
})
export class GameDrawConfigDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ gameDrawConfig }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(GameDrawConfigDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.gameDrawConfig = gameDrawConfig;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/game-draw-config', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/game-draw-config', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
