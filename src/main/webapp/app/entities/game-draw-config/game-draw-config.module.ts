import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PlaywinSharedModule } from 'app/shared/shared.module';
import { GameDrawConfigComponent } from './game-draw-config.component';
import { GameDrawConfigDetailComponent } from './game-draw-config-detail.component';
import { GameDrawConfigUpdateComponent } from './game-draw-config-update.component';
import { GameDrawConfigDeletePopupComponent, GameDrawConfigDeleteDialogComponent } from './game-draw-config-delete-dialog.component';
import { gameDrawConfigRoute, gameDrawConfigPopupRoute } from './game-draw-config.route';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

const ENTITY_STATES = [...gameDrawConfigRoute, ...gameDrawConfigPopupRoute];

@NgModule({
  imports: [PlaywinSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    GameDrawConfigComponent,
    GameDrawConfigDetailComponent,
    GameDrawConfigUpdateComponent,
    GameDrawConfigDeleteDialogComponent,
    GameDrawConfigDeletePopupComponent
  ],
  entryComponents: [
    GameDrawConfigComponent,
    GameDrawConfigUpdateComponent,
    GameDrawConfigDeleteDialogComponent,
    GameDrawConfigDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PlaywinGameDrawConfigModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
