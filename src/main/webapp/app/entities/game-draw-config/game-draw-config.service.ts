import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IGameDrawConfig } from 'app/shared/model/game-draw-config.model';

type EntityResponseType = HttpResponse<IGameDrawConfig>;
type EntityArrayResponseType = HttpResponse<IGameDrawConfig[]>;

@Injectable({ providedIn: 'root' })
export class GameDrawConfigService {
  public resourceUrl = SERVER_API_URL + 'api/game-draw-configs';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/game-draw-configs';

  constructor(protected http: HttpClient) {}

  create(gameDrawConfig: IGameDrawConfig): Observable<EntityResponseType> {
    return this.http.post<IGameDrawConfig>(this.resourceUrl, gameDrawConfig, { observe: 'response' });
  }

  update(gameDrawConfig: IGameDrawConfig): Observable<EntityResponseType> {
    return this.http.put<IGameDrawConfig>(this.resourceUrl, gameDrawConfig, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IGameDrawConfig>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IGameDrawConfig[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IGameDrawConfig[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
