import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { GameDrawConfig } from 'app/shared/model/game-draw-config.model';
import { GameDrawConfigService } from './game-draw-config.service';
import { GameDrawConfigComponent } from './game-draw-config.component';
import { GameDrawConfigDetailComponent } from './game-draw-config-detail.component';
import { GameDrawConfigUpdateComponent } from './game-draw-config-update.component';
import { GameDrawConfigDeletePopupComponent } from './game-draw-config-delete-dialog.component';
import { IGameDrawConfig } from 'app/shared/model/game-draw-config.model';

@Injectable({ providedIn: 'root' })
export class GameDrawConfigResolve implements Resolve<IGameDrawConfig> {
  constructor(private service: GameDrawConfigService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IGameDrawConfig> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<GameDrawConfig>) => response.ok),
        map((gameDrawConfig: HttpResponse<GameDrawConfig>) => gameDrawConfig.body)
      );
    }
    return of(new GameDrawConfig());
  }
}

export const gameDrawConfigRoute: Routes = [
  {
    path: '',
    component: GameDrawConfigComponent,
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_DEV'],
      pageTitle: 'playwinApp.gameDrawConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: GameDrawConfigDetailComponent,
    resolve: {
      gameDrawConfig: GameDrawConfigResolve
    },
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_DEV'],
      pageTitle: 'playwinApp.gameDrawConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: GameDrawConfigUpdateComponent,
    resolve: {
      gameDrawConfig: GameDrawConfigResolve
    },
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_DEV'],
      pageTitle: 'playwinApp.gameDrawConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: GameDrawConfigUpdateComponent,
    resolve: {
      gameDrawConfig: GameDrawConfigResolve
    },
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_DEV'],
      pageTitle: 'playwinApp.gameDrawConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const gameDrawConfigPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: GameDrawConfigDeletePopupComponent,
    resolve: {
      gameDrawConfig: GameDrawConfigResolve
    },
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_DEV'],
      pageTitle: 'playwinApp.gameDrawConfig.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
