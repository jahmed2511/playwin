import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IGameDrawConfig } from 'app/shared/model/game-draw-config.model';

@Component({
  selector: 'jhi-game-draw-config-detail',
  templateUrl: './game-draw-config-detail.component.html'
})
export class GameDrawConfigDetailComponent implements OnInit {
  gameDrawConfig: IGameDrawConfig;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ gameDrawConfig }) => {
      this.gameDrawConfig = gameDrawConfig;
    });
  }

  previousState() {
    window.history.back();
  }
}
