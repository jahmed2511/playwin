import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { filter, map } from 'rxjs/operators';
import { JhiEventManager } from 'ng-jhipster';

import { IGameDrawConfig } from 'app/shared/model/game-draw-config.model';
import { AccountService } from 'app/core/auth/account.service';
import { GameDrawConfigService } from './game-draw-config.service';

@Component({
  selector: 'jhi-game-draw-config',
  templateUrl: './game-draw-config.component.html'
})
export class GameDrawConfigComponent implements OnInit, OnDestroy {
  gameDrawConfigs: IGameDrawConfig[];
  currentAccount: any;
  eventSubscriber: Subscription;
  currentSearch: string;

  constructor(
    protected gameDrawConfigService: GameDrawConfigService,
    protected eventManager: JhiEventManager,
    protected activatedRoute: ActivatedRoute,
    protected accountService: AccountService
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll() {
    if (this.currentSearch) {
      this.gameDrawConfigService
        .search({
          query: this.currentSearch
        })
        .pipe(
          filter((res: HttpResponse<IGameDrawConfig[]>) => res.ok),
          map((res: HttpResponse<IGameDrawConfig[]>) => res.body)
        )
        .subscribe((res: IGameDrawConfig[]) => (this.gameDrawConfigs = res));
      return;
    }
    this.gameDrawConfigService
      .query()
      .pipe(
        filter((res: HttpResponse<IGameDrawConfig[]>) => res.ok),
        map((res: HttpResponse<IGameDrawConfig[]>) => res.body)
      )
      .subscribe((res: IGameDrawConfig[]) => {
        this.gameDrawConfigs = res;
        this.currentSearch = '';
      });
  }

  search(query) {
    if (!query) {
      return this.clear();
    }
    this.currentSearch = query;
    this.loadAll();
  }

  clear() {
    this.currentSearch = '';
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInGameDrawConfigs();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IGameDrawConfig) {
    return item.id;
  }

  registerChangeInGameDrawConfigs() {
    this.eventSubscriber = this.eventManager.subscribe('gameDrawConfigListModification', response => this.loadAll());
  }
  getPriority(p: number) {
    switch (p) {
      case 1:
        return 'Min';
        break;
      case 2:
        return 'Max';
        break;
      case 3:
        return 'Range';
        break;
      default:
        break;
    }
  }
}
