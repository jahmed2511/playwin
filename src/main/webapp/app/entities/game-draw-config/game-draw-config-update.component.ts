import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IGameDrawConfig, GameDrawConfig } from 'app/shared/model/game-draw-config.model';
import { GameDrawConfigService } from './game-draw-config.service';
import { IGame } from 'app/shared/model/game.model';
import { GameService } from 'app/entities/game/game.service';
import { ICoin } from 'app/shared/model/coin.model';
import { CoinService } from 'app/entities/coin/coin.service';

@Component({
  selector: 'jhi-game-draw-config-update',
  templateUrl: './game-draw-config-update.component.html'
})
export class GameDrawConfigUpdateComponent implements OnInit {
  isSaving: boolean;

  gameDrawConfig: IGameDrawConfig;

  games: IGame[];

  coins: ICoin[];

  editForm = this.fb.group({
    id: [],
    priority: [],
    rangeFrom: [],
    rangeTo: [],
    active: [null, [Validators.required]],
    game: [null, Validators.required],
    coin: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected gameDrawConfigService: GameDrawConfigService,
    protected gameService: GameService,
    protected coinService: CoinService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ gameDrawConfig }) => {
      this.updateForm(gameDrawConfig);
      this.gameDrawConfig = gameDrawConfig;
      this.gameChange(gameDrawConfig.game.id);
    });
    this.gameService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IGame[]>) => mayBeOk.ok),
        map((response: HttpResponse<IGame[]>) => response.body)
      )
      .subscribe((res: IGame[]) => (this.games = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(gameDrawConfig: IGameDrawConfig) {
    this.editForm.patchValue({
      id: gameDrawConfig.id,
      priority: gameDrawConfig.priority,
      rangeFrom: gameDrawConfig.rangeFrom,
      rangeTo: gameDrawConfig.rangeTo,
      active: gameDrawConfig.active,
      game: gameDrawConfig.game,
      coin: gameDrawConfig.coin
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const gameDrawConfig = this.createFromForm();
    if (gameDrawConfig.id !== undefined) {
      this.subscribeToSaveResponse(this.gameDrawConfigService.update(gameDrawConfig));
    } else {
      this.subscribeToSaveResponse(this.gameDrawConfigService.create(gameDrawConfig));
    }
  }

  private createFromForm(): IGameDrawConfig {
    return {
      ...new GameDrawConfig(),
      id: this.editForm.get(['id']).value,
      priority: this.editForm.get(['priority']).value,
      rangeFrom: this.editForm.get(['rangeFrom']).value,
      rangeTo: this.editForm.get(['rangeTo']).value,
      active: this.editForm.get(['active']).value,
      game: this.editForm.get(['game']).value,
      coin: this.editForm.get(['coin']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IGameDrawConfig>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackGameById(index: number, item: IGame) {
    return item.id;
  }

  trackCoinById(index: number, item: ICoin) {
    return item.id;
  }
  gameChange(id) {
    if (id === null) {
      id = this.editForm.get('game').value.id;
    }
    this.coinService
      .findByGameId(id)
      .subscribe(res => (this.coins = res.body), (subRes: HttpErrorResponse) => this.onError(subRes.message));
  }
}
