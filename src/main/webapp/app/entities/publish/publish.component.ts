import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { IPublish, Publish } from 'app/shared/model/publish.model';
import { PublishService } from './publish.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IBid, Bid } from 'app/shared/model/bid.model';
import { AccountService } from 'app/core';
import { GameService } from 'app/entities/game/';
import { IGame, Game } from 'app/shared/model/game.model';
import * as moment from 'moment';
import { GameDto } from './game.dto';
import { BidDto } from './bid.dto';
import { CoinDto } from './coin.dto';
import { BidService } from 'app/entities/bid/bid.service';
import { WalletService } from '../wallet';

@Component({
  selector: 'jhi-publish',
  templateUrl: './publish.component.html'
})
export class PublishComponent implements OnInit {
  games: IGame[];
  gameDtos: Array<GameDto> = new Array<GameDto>();
  Bids: Array<IBid> = new Array<IBid>();
  timeArr: Array<String>;
  timeValArr: Array<Array<String>>;

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected activatedRoute: ActivatedRoute,
    protected accountService: AccountService,
    private gameService: GameService,
    private bidService: BidService,
    private publishService: PublishService
  ) {}
  private isLoadedGame: boolean = false;
  loadGames() {
    this.gameService
      .getAllActiveGames({
        page: 0
      })
      .subscribe(
        (res: HttpResponse<IGame[]>) => this.assignGames(res.body),
        (res: HttpErrorResponse) => this.jhiAlertService.error(res.error.message)
      );
  }
  loadBids() {
    this.bidService
      .getBidForPublish()
      .subscribe(
        (res: HttpResponse<IGame[]>) => this.assignBids(res.body),
        (res: HttpErrorResponse) => this.jhiAlertService.error(res.error.message)
      );
  }
  assignBids(bids) {
    this.Bids = bids;
    console.log('add {0} ', bids);
    if (!this.isLoadedGame) {
      this.loadGames();
    }
  }
  assignGames(games) {
    this.games = games;
    this.isLoadedGame = true;
    console.log('games {0} ', games);
    this.timeArr = new Array<String>(games.length);
    this.timeValArr = new Array<Array<String>>(games.length);

    for (let index = 0; index < games.length; index++) {
      const game = games[index];
      const timeArr = this.getTimeArr(game.interval, game.endTime, index);
      const gDto = this.initGame(game, index, null);
      this.gameDtos.push(gDto);
    }
    console.log(this.timeArr);
    console.log(this.timeValArr);
  }

  ngOnInit() {
    this.loadBids();
  }
  initGame(game: IGame, index, selectedTimes) {
    const gDto: GameDto = new GameDto();
    gDto.game = game;
    const bidDtos = new Array<BidDto>();
    for (let i = game.numberRangeFrom; i <= game.numberRangeTo; i++) {
      const bidDto = new BidDto();
      bidDto.gameDto = gDto;
      bidDto.gameId = game.id;
      bidDto.bidNo = i;
      const coindtos = new Array<CoinDto>();
      if (game.coins != null && game.coins.length > 0) {
        for (let j = 0; j < game.coins.length; j++) {
          const coinDto = new CoinDto();
          coinDto.coinId = game.coins[j].id;
          coinDto.coin = game.coins[j];
          coinDto.name = game.coins[j].displayName;
          coinDto.bidDto = bidDto;
          coinDto.gameDto = gDto;
          if (selectedTimes) {
            coinDto.time = this.getTime(selectedTimes);
          } else {
            coinDto.time = this.getTime(this.timeArr[index]);
          }

          this.isBidAlearyDefine(coinDto);
          coindtos.push(coinDto);
        }
        bidDto.coins = coindtos;
        bidDtos.push(bidDto);
      }
    }
    gDto.bidDtos = bidDtos;
    return gDto;
  }

  getTimeArr(interval, expireDate, i) {
    let date = moment(new Date());
    const mint = date.minutes();
    const diff = this.getLeftTime(interval, mint);
    date = date.add(diff, 'minutes');
    const timeArr = new Array<String>();
    timeArr.push(
      date.hours() +
        ' : ' +
        date
          .minute()
          .toString()
          .padStart(2, '0')
    );
    while (true) {
      if (date.hours() < expireDate.hours() || (date.hours() === expireDate.hours() && date.minutes <= expireDate.minutes())) {
        date = date.add(interval, 'minutes');
        timeArr.push(
          date.hours() +
            ' : ' +
            date
              .minute()
              .toString()
              .padStart(2, '0')
        );
      } else {
        break;
      }
    }
    this.timeArr[i] = timeArr[0];
    this.timeValArr[i] = timeArr;
    return timeArr;
  }
  getLeftTime(diff, mint) {
    if (mint <= diff) {
      return diff - mint;
    } else {
      let i = 2;
      while (diff <= 60) {
        if (mint <= diff * i) {
          return diff * i - mint;
        }
        i++;
      }
    }
  }
  selectTime(game, index, selectedTimes) {
    this.loadBids();
    this.gameDtos[index] = this.initGame(game, index, selectedTimes);
  }
  getTime(timeStr: String): moment.Moment {
    const timeArr = timeStr.split(':');
    const newDate = moment(new Date());
    const hr = parseInt(timeArr[0], 10);
    const min = parseInt(timeArr[1], 10);

    newDate.set({
      hours: hr,
      minutes: min,
      seconds: 0
    });
    return newDate;
  }
  publish(game: GameDto, coin: CoinDto, time, i) {
    let publish = new Publish();
    publish.coinId = coin.coin.id;
    publish.gameId = game.game.id;
    publish.publishDate = game.publishDate;
    publish.publishTime = time;
    publish.isPublish = false;
    publish.publishNumber = coin.publishValue;
    this.publishService
      .create(publish)
      .subscribe(
        (res: HttpResponse<IPublish>) => this.jhiAlertService.success(res.statusText),
        (res: HttpErrorResponse) => this.jhiAlertService.error(res.error.message)
      );
  }
  isBidAlearyDefine(coinDto: CoinDto) {
    if (this.Bids.length > 0) {
      const filterBids = this.Bids.filter(
        bid =>
          bid.game.id === coinDto.gameDto.game.id &&
          bid.bidTime.hours() === coinDto.time.hours() &&
          bid.bidTime.minutes() === coinDto.time.minutes() &&
          bid.bidTime.date() === coinDto.time.date() &&
          bid.coin.id === coinDto.coinId &&
          bid.coin.displayName === coinDto.name &&
          bid.bidNo === coinDto.bidDto.bidNo
      );
      if (filterBids != null && filterBids.length > 0) {
        coinDto.noOfCoin = filterBids[0].noOfCoins;
        coinDto.isUpdated = true;
        coinDto.bidDto.bidId = filterBids[0].id;
        coinDto.bidId = filterBids[0].id;
      }
    }
  }
}
