import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IPublish, Publish } from 'app/shared/model/publish.model';
import { PublishService } from './publish.service';

@Component({
  selector: 'jhi-publish-update',
  templateUrl: './publish-update.component.html'
})
export class PublishUpdateComponent implements OnInit {
  publish: IPublish;
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    bidNo: [],
    coinA: [],
    coinB: [],
    coinC: []
  });

  constructor(protected publishService: PublishService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ publish }) => {
      // this.updateForm(publish);
      this.publish = publish;
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const publish = this.createFromForm();
    if (publish.id !== undefined) {
      this.subscribeToSaveResponse(this.publishService.update(publish));
    } else {
      this.subscribeToSaveResponse(this.publishService.create(publish));
    }
  }

  private createFromForm(): IPublish {
    const entity = {
      ...new Publish(),
      id: this.editForm.get(['id']).value,
      bidNo: this.editForm.get(['bidNo']).value,
      coinA: this.editForm.get(['coinA']).value,
      coinB: this.editForm.get(['coinB']).value,
      coinC: this.editForm.get(['coinC']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPublish>>) {
    result.subscribe((res: HttpResponse<IPublish>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
