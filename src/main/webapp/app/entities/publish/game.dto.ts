import * as moment from 'moment';
import { BidDto } from './bid.dto';
import { IGame } from 'app/shared/model/game.model';

export class GameDto {
  constructor(public game?: IGame, public bidDtos?: BidDto[], public publishDate?: moment.Moment) {
    this.publishDate = moment(new Date());
  }
}
