import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { PlaywinSharedModule } from 'app/shared';
import {
  PublishComponent,
  PublishDetailComponent,
  PublishUpdateComponent,
  PublishDeletePopupComponent,
  PublishDeleteDialogComponent,
  publishRoute,
  publishPopupRoute
} from './';

const ENTITY_STATES = [...publishRoute, ...publishPopupRoute];

@NgModule({
  imports: [PlaywinSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    PublishComponent,
    PublishDetailComponent,
    PublishUpdateComponent,
    PublishDeleteDialogComponent,
    PublishDeletePopupComponent
  ],
  entryComponents: [PublishComponent, PublishUpdateComponent, PublishDeleteDialogComponent, PublishDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PlaywinPublishModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
