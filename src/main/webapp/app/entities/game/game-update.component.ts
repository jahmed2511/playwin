import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IGame, Game } from 'app/shared/model/game.model';
import { GameService } from './game.service';
import { IUser, UserService } from 'app/core';

@Component({
  selector: 'jhi-game-update',
  templateUrl: './game-update.component.html'
})
export class GameUpdateComponent implements OnInit {
  game: IGame;
  isSaving: boolean;

  users: IUser[];

  editForm = this.fb.group({
    id: [],
    gameName: [null, [Validators.required]],
    description: [],
    startTime: [null, [Validators.required]],
    endTime: [null, [Validators.required]],
    interval: [null, [Validators.required]],
    numberRangeFrom: [null, [Validators.required]],
    numberRangeTo: [null, [Validators.required]],
    isEnable: [],
    users: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected gameService: GameService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ game }) => {
      this.updateForm(game);
      this.game = game;
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(game: IGame) {
    this.editForm.patchValue({
      id: game.id,
      gameName: game.gameName,
      description: game.description,
      startTime: game.startTime != null ? game.startTime.format(DATE_TIME_FORMAT) : null,
      endTime: game.endTime != null ? game.endTime.format(DATE_TIME_FORMAT) : null,
      interval: game.interval,
      numberRangeFrom: game.numberRangeFrom,
      numberRangeTo: game.numberRangeTo,
      isEnable: game.isEnable,
      users: game.users
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const game = this.createFromForm();
    if (game.id !== undefined) {
      this.subscribeToSaveResponse(this.gameService.update(game));
    } else {
      this.subscribeToSaveResponse(this.gameService.create(game));
    }
  }

  private createFromForm(): IGame {
    const entity = {
      ...new Game(),
      id: this.editForm.get(['id']).value,
      gameName: this.editForm.get(['gameName']).value,
      description: this.editForm.get(['description']).value,
      startTime:
        this.editForm.get(['startTime']).value != null ? moment(this.editForm.get(['startTime']).value, DATE_TIME_FORMAT) : undefined,
      endTime: this.editForm.get(['endTime']).value != null ? moment(this.editForm.get(['endTime']).value, DATE_TIME_FORMAT) : undefined,
      interval: this.editForm.get(['interval']).value,
      numberRangeFrom: this.editForm.get(['numberRangeFrom']).value,
      numberRangeTo: this.editForm.get(['numberRangeTo']).value,
      isEnable: this.editForm.get(['isEnable']).value,
      users: this.editForm.get(['users']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IGame>>) {
    result.subscribe((res: HttpResponse<IGame>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
