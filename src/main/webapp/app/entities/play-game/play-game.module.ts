import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';
import { GameService } from 'app/entities/game/game.service';
import { BidService } from 'app/entities/bid/bid.service';

import { PlaywinSharedModule } from 'app/shared';
import {
  PlayGameComponent,
  PlayGameDetailComponent,
  PlayGameUpdateComponent,
  PlayGameDeletePopupComponent,
  PlayGameDeleteDialogComponent,
  playGameRoute,
  playGamePopupRoute
} from './';

const ENTITY_STATES = [...playGameRoute, ...playGamePopupRoute];

@NgModule({
  imports: [PlaywinSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    PlayGameComponent,
    PlayGameDetailComponent,
    PlayGameUpdateComponent,
    PlayGameDeleteDialogComponent,
    PlayGameDeletePopupComponent
  ],
  entryComponents: [PlayGameComponent, PlayGameUpdateComponent, PlayGameDeleteDialogComponent, PlayGameDeletePopupComponent],
  providers: [BidService, GameService, { provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PlaywinPlayGameModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
