import { Moment } from 'moment';
import { GameDto } from './game.dto';
import { CoinDto } from './coin.dto';

export class BidDto {
  constructor(
    public bidId?: number,
    public gameId?: number,
    public bidNo?: number,
    public gameDto?: GameDto,
    public coins?: CoinDto[],
    public isUpdate?: boolean
  ) {}
}
