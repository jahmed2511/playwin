import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { PlayGame } from 'app/shared/model/play-game.model';
import { PlayGameService } from './play-game.service';
import { PlayGameComponent } from './play-game.component';
import { PlayGameDetailComponent } from './play-game-detail.component';
import { PlayGameUpdateComponent } from './play-game-update.component';
import { PlayGameDeletePopupComponent } from './play-game-delete-dialog.component';
import { IPlayGame } from 'app/shared/model/play-game.model';

@Injectable({ providedIn: 'root' })
export class PlayGameResolve implements Resolve<IPlayGame> {
  constructor(private service: PlayGameService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPlayGame> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<PlayGame>) => response.ok),
        map((playGame: HttpResponse<PlayGame>) => playGame.body)
      );
    }
    return of(new PlayGame());
  }
}

export const playGameRoute: Routes = [
  {
    path: '',
    component: PlayGameComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'playwinApp.playGame.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PlayGameDetailComponent,
    resolve: {
      playGame: PlayGameResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'playwinApp.playGame.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PlayGameUpdateComponent,
    resolve: {
      playGame: PlayGameResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'playwinApp.playGame.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PlayGameUpdateComponent,
    resolve: {
      playGame: PlayGameResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'playwinApp.playGame.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const playGamePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: PlayGameDeletePopupComponent,
    resolve: {
      playGame: PlayGameResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'playwinApp.playGame.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
