import { BidDto } from './bid.dto';
import { GameDto } from './game.dto';
import { Coin } from 'app/shared/model/coin.model';
import * as moment from 'moment';
export class CoinDto {
  constructor(
    public coinId?: number,
    public name?: string,
    public noOfCoin?: number,
    public gameDto?: GameDto,
    public bidDto?: BidDto,
    public coin?: Coin,
    public time?: moment.Moment,
    public bidId?: number,
    public isUpdated?: boolean
  ) {}
}
