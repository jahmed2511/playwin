import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IPlayGame, PlayGame } from 'app/shared/model/play-game.model';
import { PlayGameService } from './play-game.service';

@Component({
  selector: 'jhi-play-game-update',
  templateUrl: './play-game-update.component.html'
})
export class PlayGameUpdateComponent implements OnInit {
  playGame: IPlayGame;
  isSaving: boolean;

  editForm = this.fb.group({
    id: []
  });

  constructor(protected playGameService: PlayGameService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ playGame }) => {
      this.updateForm(playGame);
      this.playGame = playGame;
    });
  }

  updateForm(playGame: IPlayGame) {
    this.editForm.patchValue({
      id: playGame.id
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const playGame = this.createFromForm();
    if (playGame.id !== undefined) {
      this.subscribeToSaveResponse(this.playGameService.update(playGame));
    } else {
      this.subscribeToSaveResponse(this.playGameService.create(playGame));
    }
  }

  private createFromForm(): IPlayGame {
    const entity = {
      ...new PlayGame(),
      id: this.editForm.get(['id']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPlayGame>>) {
    result.subscribe((res: HttpResponse<IPlayGame>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
