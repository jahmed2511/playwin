import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPlayGame } from 'app/shared/model/play-game.model';

type EntityResponseType = HttpResponse<IPlayGame>;
type EntityArrayResponseType = HttpResponse<IPlayGame[]>;

@Injectable({ providedIn: 'root' })
export class PlayGameService {
  public resourceUrl = SERVER_API_URL + 'api/play-games';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/play-games';

  constructor(protected http: HttpClient) {}

  create(playGame: IPlayGame): Observable<EntityResponseType> {
    return this.http.post<IPlayGame>(this.resourceUrl, playGame, { observe: 'response' });
  }

  update(playGame: IPlayGame): Observable<EntityResponseType> {
    return this.http.put<IPlayGame>(this.resourceUrl, playGame, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPlayGame>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPlayGame[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPlayGame[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
