import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPlayGame } from 'app/shared/model/play-game.model';

@Component({
  selector: 'jhi-play-game-detail',
  templateUrl: './play-game-detail.component.html'
})
export class PlayGameDetailComponent implements OnInit {
  playGame: IPlayGame;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ playGame }) => {
      this.playGame = playGame;
    });
  }

  previousState() {
    window.history.back();
  }
}
