import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPlayGame } from 'app/shared/model/play-game.model';
import { PlayGameService } from './play-game.service';

@Component({
  selector: 'jhi-play-game-delete-dialog',
  templateUrl: './play-game-delete-dialog.component.html'
})
export class PlayGameDeleteDialogComponent {
  playGame: IPlayGame;

  constructor(protected playGameService: PlayGameService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.playGameService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'playGameListModification',
        content: 'Deleted an playGame'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-play-game-delete-popup',
  template: ''
})
export class PlayGameDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ playGame }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(PlayGameDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.playGame = playGame;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/play-game', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/play-game', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
