import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IGameResult } from 'app/shared/model/game-result.model';
import { GameResultService } from './game-result.service';

@Component({
  selector: 'jhi-game-result-delete-dialog',
  templateUrl: './game-result-delete-dialog.component.html'
})
export class GameResultDeleteDialogComponent {
  gameResult: IGameResult;

  constructor(
    protected gameResultService: GameResultService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.gameResultService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'gameResultListModification',
        content: 'Deleted an gameResult'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-game-result-delete-popup',
  template: ''
})
export class GameResultDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ gameResult }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(GameResultDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.gameResult = gameResult;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/game-result', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/game-result', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
