import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IGameResult, GameResult } from 'app/shared/model/game-result.model';
import { GameResultService } from './game-result.service';
import { IGame } from 'app/shared/model/game.model';
import { GameService } from 'app/entities/game';
import { ICoin } from 'app/shared/model/coin.model';
import { CoinService } from 'app/entities/coin';

@Component({
  selector: 'jhi-game-result-update',
  templateUrl: './game-result-update.component.html'
})
export class GameResultUpdateComponent implements OnInit {
  gameResult: IGameResult;
  isSaving: boolean;

  games: IGame[];

  coins: ICoin[];

  editForm = this.fb.group({
    id: [],
    gameTime: [null, [Validators.required]],
    winnersNos: [],
    remarks: [],
    game: [],
    coin: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected gameResultService: GameResultService,
    protected gameService: GameService,
    protected coinService: CoinService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ gameResult }) => {
      this.updateForm(gameResult);
      this.gameResult = gameResult;
    });
    this.gameService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IGame[]>) => mayBeOk.ok),
        map((response: HttpResponse<IGame[]>) => response.body)
      )
      .subscribe((res: IGame[]) => (this.games = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.coinService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICoin[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICoin[]>) => response.body)
      )
      .subscribe((res: ICoin[]) => (this.coins = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(gameResult: IGameResult) {
    this.editForm.patchValue({
      id: gameResult.id,
      gameTime: gameResult.gameTime != null ? gameResult.gameTime.format(DATE_TIME_FORMAT) : null,
      winnersNos: gameResult.winnersNos,
      remarks: gameResult.remarks,
      game: gameResult.game,
      coin: gameResult.coin
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const gameResult = this.createFromForm();
    if (gameResult.id !== undefined) {
      this.subscribeToSaveResponse(this.gameResultService.update(gameResult));
    } else {
      this.subscribeToSaveResponse(this.gameResultService.create(gameResult));
    }
  }

  private createFromForm(): IGameResult {
    const entity = {
      ...new GameResult(),
      id: this.editForm.get(['id']).value,
      gameTime: this.editForm.get(['gameTime']).value != null ? moment(this.editForm.get(['gameTime']).value, DATE_TIME_FORMAT) : undefined,
      winnersNos: this.editForm.get(['winnersNos']).value,
      remarks: this.editForm.get(['remarks']).value,
      game: this.editForm.get(['game']).value,
      coin: this.editForm.get(['coin']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IGameResult>>) {
    result.subscribe((res: HttpResponse<IGameResult>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackGameById(index: number, item: IGame) {
    return item.id;
  }

  trackCoinById(index: number, item: ICoin) {
    return item.id;
  }
}
