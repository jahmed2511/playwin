import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { PlaywinSharedModule } from 'app/shared';
import {
  GameResultComponent,
  GameResultDetailComponent,
  GameResultUpdateComponent,
  GameResultDeletePopupComponent,
  GameResultDeleteDialogComponent,
  gameResultRoute,
  gameResultPopupRoute
} from './';

const ENTITY_STATES = [...gameResultRoute, ...gameResultPopupRoute];

@NgModule({
  imports: [PlaywinSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    GameResultComponent,
    GameResultDetailComponent,
    GameResultUpdateComponent,
    GameResultDeleteDialogComponent,
    GameResultDeletePopupComponent
  ],
  entryComponents: [GameResultComponent, GameResultUpdateComponent, GameResultDeleteDialogComponent, GameResultDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PlaywinGameResultModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
