import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IGameResult } from 'app/shared/model/game-result.model';

@Component({
  selector: 'jhi-game-result-detail',
  templateUrl: './game-result-detail.component.html'
})
export class GameResultDetailComponent implements OnInit {
  gameResult: IGameResult;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ gameResult }) => {
      this.gameResult = gameResult;
    });
  }

  previousState() {
    window.history.back();
  }
}
