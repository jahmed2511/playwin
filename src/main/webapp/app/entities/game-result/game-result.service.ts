import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IGameResult } from 'app/shared/model/game-result.model';

type EntityResponseType = HttpResponse<IGameResult>;
type EntityArrayResponseType = HttpResponse<IGameResult[]>;

@Injectable({ providedIn: 'root' })
export class GameResultService {
  public resourceUrl = SERVER_API_URL + 'api/game-results';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/game-results';

  constructor(protected http: HttpClient) {}

  create(gameResult: IGameResult): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(gameResult);
    return this.http
      .post<IGameResult>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(gameResult: IGameResult): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(gameResult);
    return this.http
      .put<IGameResult>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IGameResult>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IGameResult[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IGameResult[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(gameResult: IGameResult): IGameResult {
    const copy: IGameResult = Object.assign({}, gameResult, {
      gameTime: gameResult.gameTime != null && gameResult.gameTime.isValid() ? gameResult.gameTime.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.gameTime = res.body.gameTime != null ? moment(res.body.gameTime) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((gameResult: IGameResult) => {
        gameResult.gameTime = gameResult.gameTime != null ? moment(gameResult.gameTime) : null;
      });
    }
    return res;
  }
}
