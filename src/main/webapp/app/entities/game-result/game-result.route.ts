import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { GameResult } from 'app/shared/model/game-result.model';
import { GameResultService } from './game-result.service';
import { GameResultComponent } from './game-result.component';
import { GameResultDetailComponent } from './game-result-detail.component';
import { GameResultUpdateComponent } from './game-result-update.component';
import { GameResultDeletePopupComponent } from './game-result-delete-dialog.component';
import { IGameResult } from 'app/shared/model/game-result.model';

@Injectable({ providedIn: 'root' })
export class GameResultResolve implements Resolve<IGameResult> {
  constructor(private service: GameResultService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IGameResult> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<GameResult>) => response.ok),
        map((gameResult: HttpResponse<GameResult>) => gameResult.body)
      );
    }
    return of(new GameResult());
  }
}

export const gameResultRoute: Routes = [
  {
    path: '',
    component: GameResultComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER', 'ROLE_AGENT', 'ROLE_DEV'],
      defaultSort: 'id,asc',
      pageTitle: 'playwinApp.gameResult.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':game',
    component: GameResultComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER', 'ROLE_AGENT', 'ROLE_DEV'],
      defaultSort: 'id,asc',
      pageTitle: 'playwinApp.gameResult.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: GameResultDetailComponent,
    resolve: {
      gameResult: GameResultResolve
    },
    data: {
      authorities: ['ROLE_USER', 'ROLE_AGENT', 'ROLE_DEV'],
      pageTitle: 'playwinApp.gameResult.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: GameResultUpdateComponent,
    resolve: {
      gameResult: GameResultResolve
    },
    data: {
      authorities: ['ROLE_DEV'],
      pageTitle: 'playwinApp.gameResult.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: GameResultUpdateComponent,
    resolve: {
      gameResult: GameResultResolve
    },
    data: {
      authorities: ['ROLE_DEV'],
      pageTitle: 'playwinApp.gameResult.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const gameResultPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: GameResultDeletePopupComponent,
    resolve: {
      gameResult: GameResultResolve
    },
    data: {
      authorities: ['ROLE_DEV'],
      pageTitle: 'playwinApp.gameResult.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
