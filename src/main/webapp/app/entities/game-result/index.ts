export * from './game-result.service';
export * from './game-result-update.component';
export * from './game-result-delete-dialog.component';
export * from './game-result-detail.component';
export * from './game-result.component';
export * from './game-result.route';
