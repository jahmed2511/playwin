import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IGameResult, GameResult } from 'app/shared/model/game-result.model';
import { AccountService } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { GameResultService } from './game-result.service';
import { GameService } from '../game/game.service';
import { IGame } from 'app/shared/model/game.model';
import { Moment } from 'moment';
import * as moment from 'moment';
import { from } from 'rxjs';
import { groupBy, mergeMap, toArray } from 'rxjs/operators';
import { ResultWrapper } from './game-result-wrapper';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'jhi-game-result',
  templateUrl: './game-result.component.html',
  styles: ['.resultTab .tab-content > .tab-pane { display :block;} ']
})
export class GameResultComponent implements OnInit, OnDestroy {
  currentAccount: any;
  gameResults: IGameResult[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  currentSearch: string;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  games: IGame[];
  currDate: Moment = moment();
  resutlMap: Map<Number, ResultWrapper[]> = new Map();
  currentGameName: string;

  constructor(
    protected gameResultService: GameResultService,
    protected parseLinks: JhiParseLinks,
    protected jhiAlertService: JhiAlertService,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected gameService: GameService,
    private datePipe: DatePipe
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ? this.activatedRoute.snapshot.params['search'] : '';
    this.currentGameName =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['game'] ? this.activatedRoute.snapshot.params['game'] : '';
  }

  loadAll() {
    this.gameService.getAllActiveGames().subscribe(
      (res: HttpResponse<IGame[]>) => {
        this.games = res.body;
        if (this.games != null && this.games.length > 0) {
          this.games.forEach(game => {
            if (this.currentGameName === game.gameName) {
              game.isEnable = true;
            } else {
              game.isEnable = false;
            }
            this.loadResult(game.id);
          });
          if (this.currentGameName === '') {
            this.games[0].isEnable = true;
          }
        }
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
  }

  loadResult(gameId: number) {
    this.gameResultService
      .query({
        page: this.page - 1,
        size: 1000000,
        sort: this.sort(),
        'criteria.date': this.toDate(this.currDate),
        'criteria.gameId': gameId
      })
      .subscribe(
        (res: HttpResponse<IGameResult[]>) => {
          console.log('loaded resutl of game ' + gameId);
          console.log(res.body);
          this.mergeResult(gameId, res.body);
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/game-result'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        search: this.currentSearch,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  clear() {
    this.page = 0;
    this.currentSearch = '';
    this.router.navigate([
      '/game-result',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  search(query) {
    if (!query) {
      return this.clear();
    }
    this.page = 0;
    this.currentSearch = query;
    this.router.navigate([
      '/game-result',
      {
        search: this.currentSearch,
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInGameResults();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IGameResult) {
    return item.id;
  }

  registerChangeInGameResults() {
    this.eventSubscriber = this.eventManager.subscribe('gameResultListModification', response => this.loadAll());
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }
  tabClick(currGame: IGame) {
    console.log(currGame);
    this.games.forEach(game => {
      if (game.id === currGame.id) {
        game.isEnable = true;
      } else {
        game.isEnable = false;
      }
    });
  }
  protected paginateGameResults(data: IGameResult[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.gameResults = data;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  toDate(moment: Moment) {
    let date = this.datePipe.transform(moment, 'yyyy-MM-dd');
    return date;
  }
  groupByTime(moment: Moment) {
    let date = this.datePipe.transform(moment, 'HH:mm');
    return date;
  }

  mergeResult(gameid, gameResult: GameResult[]) {
    console.log('lmerge perfomrd ' + gameid);
    const source = from(gameResult);
    let wrapperArr: ResultWrapper[] = [];
    const array = source
      .pipe(
        groupBy(result => this.groupByTime(result.gameTime)),
        mergeMap(group => group.pipe(toArray()))
      )
      .subscribe(result => {
        const wraper = new ResultWrapper();
        wraper.gameResults = result;
        console.log('warreping result');
        console.log(wraper);
        wrapperArr.push(wraper);
      });
    this.resutlMap.set(gameid, wrapperArr);
  }
  getResult(gameid) {
    this.loadResult(gameid);
  }
}
