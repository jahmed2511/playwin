import { GameResult } from 'app/shared/model/game-result.model';

export class ResultWrapper {
  public gameResults: GameResult[];
}
