import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISmsConfig } from 'app/shared/model/sms-config.model';
import { SmsConfigService } from './sms-config.service';

@Component({
  selector: 'jhi-sms-config-delete-dialog',
  templateUrl: './sms-config-delete-dialog.component.html'
})
export class SmsConfigDeleteDialogComponent {
  smsConfig: ISmsConfig;

  constructor(protected smsConfigService: SmsConfigService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.smsConfigService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'smsConfigListModification',
        content: 'Deleted an smsConfig'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-sms-config-delete-popup',
  template: ''
})
export class SmsConfigDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ smsConfig }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(SmsConfigDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.smsConfig = smsConfig;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/sms-config', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/sms-config', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
