import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISmsConfig } from 'app/shared/model/sms-config.model';

type EntityResponseType = HttpResponse<ISmsConfig>;
type EntityArrayResponseType = HttpResponse<ISmsConfig[]>;

@Injectable({ providedIn: 'root' })
export class SmsConfigService {
  public resourceUrl = SERVER_API_URL + 'api/sms-configs';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/sms-configs';

  constructor(protected http: HttpClient) {}

  create(smsConfig: ISmsConfig): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(smsConfig);
    return this.http
      .post<ISmsConfig>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(smsConfig: ISmsConfig): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(smsConfig);
    return this.http
      .put<ISmsConfig>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ISmsConfig>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ISmsConfig[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ISmsConfig[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(smsConfig: ISmsConfig): ISmsConfig {
    const copy: ISmsConfig = Object.assign({}, smsConfig, {
      expiryDate: smsConfig.expiryDate != null && smsConfig.expiryDate.isValid() ? smsConfig.expiryDate.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.expiryDate = res.body.expiryDate != null ? moment(res.body.expiryDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((smsConfig: ISmsConfig) => {
        smsConfig.expiryDate = smsConfig.expiryDate != null ? moment(smsConfig.expiryDate) : null;
      });
    }
    return res;
  }
}
