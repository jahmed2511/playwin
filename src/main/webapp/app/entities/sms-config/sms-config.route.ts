import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { SmsConfig } from 'app/shared/model/sms-config.model';
import { SmsConfigService } from './sms-config.service';
import { SmsConfigComponent } from './sms-config.component';
import { SmsConfigDetailComponent } from './sms-config-detail.component';
import { SmsConfigUpdateComponent } from './sms-config-update.component';
import { SmsConfigDeletePopupComponent } from './sms-config-delete-dialog.component';
import { ISmsConfig } from 'app/shared/model/sms-config.model';

@Injectable({ providedIn: 'root' })
export class SmsConfigResolve implements Resolve<ISmsConfig> {
  constructor(private service: SmsConfigService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISmsConfig> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<SmsConfig>) => response.ok),
        map((smsConfig: HttpResponse<SmsConfig>) => smsConfig.body)
      );
    }
    return of(new SmsConfig());
  }
}

export const smsConfigRoute: Routes = [
  {
    path: '',
    component: SmsConfigComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_DEV'],
      defaultSort: 'id,asc',
      pageTitle: 'playwinApp.smsConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SmsConfigDetailComponent,
    resolve: {
      smsConfig: SmsConfigResolve
    },
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_DEV'],
      pageTitle: 'playwinApp.smsConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SmsConfigUpdateComponent,
    resolve: {
      smsConfig: SmsConfigResolve
    },
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_DEV'],
      pageTitle: 'playwinApp.smsConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SmsConfigUpdateComponent,
    resolve: {
      smsConfig: SmsConfigResolve
    },
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_DEV'],
      pageTitle: 'playwinApp.smsConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const smsConfigPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: SmsConfigDeletePopupComponent,
    resolve: {
      smsConfig: SmsConfigResolve
    },
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_DEV'],
      pageTitle: 'playwinApp.smsConfig.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
