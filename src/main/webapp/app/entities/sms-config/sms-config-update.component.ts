import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { ISmsConfig, SmsConfig } from 'app/shared/model/sms-config.model';
import { SmsConfigService } from './sms-config.service';
import { IUser, UserService } from 'app/core';
import { IGame } from 'app/shared/model/game.model';
import { GameService } from 'app/entities/game';
import { ICoin } from 'app/shared/model/coin.model';
import { IUserPref } from 'app/shared/model/user-pref.model';
import { UserPrefService } from '../user-pref';

@Component({
  selector: 'jhi-sms-config-update',
  templateUrl: './sms-config-update.component.html'
})
export class SmsConfigUpdateComponent implements OnInit {
  smsConfig: ISmsConfig;
  isSaving: boolean;

  users: IUser[];

  games: IGame[];

  prefs: IUserPref[];
  times: Array<String>;

  mobileNum: String = '';

  mobDisabled: boolean;

  coins: ICoin[];
  selectedCoins: String = '';
  selectedTimes: String = '';

  editForm = this.fb.group({
    id: [],
    smsTimeSlots: [],
    coins: [],
    expiryDate: [],
    description: [null, [Validators.required]],
    mobileNo: [],
    user: [],
    game: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected smsConfigService: SmsConfigService,
    protected userService: UserService,
    protected gameService: GameService,
    protected activatedRoute: ActivatedRoute,
    protected userPrefService: UserPrefService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;

    this.userPrefService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUserPref[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUserPref[]>) => response.body)
      )
      .subscribe(
        (res: IUserPref[]) => {
          this.prefs = res;
          this.users = this.prefs.map(pref => pref.user);
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );

    this.gameService
      .getAllActiveGames()
      .pipe(
        filter((mayBeOk: HttpResponse<IGame[]>) => mayBeOk.ok),
        map((response: HttpResponse<IGame[]>) => response.body)
      )
      .subscribe(
        (res: IGame[]) => {
          this.games = res;
          this.loadSmsConfig();
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }
  loadSmsConfig() {
    this.activatedRoute.data.subscribe(({ smsConfig }) => {
      this.updateForm(smsConfig);
      this.smsConfig = smsConfig;
      const game = this.games.find(game => game.id === smsConfig.game.id);
      this.coins = game.coins;
      this.initTimeSlot(game);
    });
  }

  updateForm(smsConfig: ISmsConfig) {
    this.editForm.patchValue({
      id: smsConfig.id,
      smsTimeSlots: smsConfig.smsTimeSlots,
      coins: smsConfig.coins,
      expiryDate: smsConfig.expiryDate != null ? smsConfig.expiryDate.format(DATE_TIME_FORMAT) : null,
      description: smsConfig.description,
      mobileNo: smsConfig.mobileNo,
      user: smsConfig.user,
      game: smsConfig.game
    });
    this.selectedCoins = smsConfig.coins;
    this.selectedTimes = smsConfig.smsTimeSlots;
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const smsConfig = this.createFromForm();
    if (smsConfig.id !== undefined) {
      this.subscribeToSaveResponse(this.smsConfigService.update(smsConfig));
    } else {
      this.subscribeToSaveResponse(this.smsConfigService.create(smsConfig));
    }
  }

  private createFromForm(): ISmsConfig {
    const entity = {
      ...new SmsConfig(),
      id: this.editForm.get(['id']).value,
      smsTimeSlots: this.editForm.get(['smsTimeSlots']).value,
      coins: this.editForm.get(['coins']).value,
      expiryDate:
        this.editForm.get(['expiryDate']).value != null ? moment(this.editForm.get(['expiryDate']).value, DATE_TIME_FORMAT) : undefined,
      description: this.editForm.get(['description']).value,
      mobileNo: this.editForm.get(['mobileNo']).value,
      user: this.editForm.get(['user']).value,
      game: this.editForm.get(['game']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISmsConfig>>) {
    result.subscribe((res: HttpResponse<ISmsConfig>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
  onChangeUser() {
    const user = this.editForm.get(['user']).value;
    if (user != null) {
      const prefArr = this.prefs.find(pref => pref.user.login === user.login);
      if (prefArr != null) {
        console.log('pref-----------');
        console.log(prefArr);
        this.mobileNum = prefArr.mobileNo;
        this.mobDisabled = true;
      } else {
        this.mobDisabled = false;
        this.mobileNum = '';
      }
    } else {
      this.mobDisabled = false;
      this.mobileNum = '';
    }
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackGameById(index: number, item: IGame) {
    return item.id;
  }
  gameChange() {
    if (this.editForm.get('game').value !== null) {
      const game = this.editForm.get('game').value;
      this.selectedCoins = '';
      this.selectedTimes = '';
      this.initTimeSlot(game);
      this.coins = game.coins;
      console.log(this.coins);
    } else {
      this.coins = [];
      this.times = [];
      this.selectedCoins = '';
      this.selectedTimes = '';
    }
  }
  initTimeSlot(game) {
    this.times = new Array<String>();
    let startTime = moment();
    startTime = startTime.hours(game.startTime.hours());
    startTime = startTime.minutes(game.startTime.minutes());
    let endTime = moment();
    endTime = endTime.hours(game.endTime.hours());
    endTime = endTime.minutes(game.endTime.minutes());
    console.log('starttime ' + startTime);
    console.log('endTime ' + endTime);
    while (startTime < endTime) {
      this.times.push(
        startTime
          .hours()
          .toString()
          .padStart(2, '0') +
          ':' +
          startTime
            .minutes()
            .toString()
            .padStart(2, '0')
      );
      startTime = startTime.add(game.interval, 'minutes');
    }
    console.log(this.times);
  }
  coinCheck(element) {
    const value = element.currentTarget.value;
    if (element.currentTarget.checked) {
      this.selectedCoins += value + ',';
    } else {
      this.selectedCoins = this.selectedCoins.replace(value + ',', '');
    }
  }
  timeCheck(element) {
    const value = element.currentTarget.value;
    if (element.currentTarget.checked) {
      this.selectedTimes += value + ',';
    } else {
      this.selectedTimes = this.selectedTimes.replace(value + ',', '');
    }
  }
}
