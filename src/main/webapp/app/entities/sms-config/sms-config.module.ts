import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { PlaywinSharedModule } from 'app/shared';
import {
  SmsConfigComponent,
  SmsConfigDetailComponent,
  SmsConfigUpdateComponent,
  SmsConfigDeletePopupComponent,
  SmsConfigDeleteDialogComponent,
  smsConfigRoute,
  smsConfigPopupRoute
} from './';

const ENTITY_STATES = [...smsConfigRoute, ...smsConfigPopupRoute];

@NgModule({
  imports: [PlaywinSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    SmsConfigComponent,
    SmsConfigDetailComponent,
    SmsConfigUpdateComponent,
    SmsConfigDeleteDialogComponent,
    SmsConfigDeletePopupComponent
  ],
  entryComponents: [SmsConfigComponent, SmsConfigUpdateComponent, SmsConfigDeleteDialogComponent, SmsConfigDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PlaywinSmsConfigModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
