import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISmsConfig } from 'app/shared/model/sms-config.model';

@Component({
  selector: 'jhi-sms-config-detail',
  templateUrl: './sms-config-detail.component.html'
})
export class SmsConfigDetailComponent implements OnInit {
  smsConfig: ISmsConfig;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ smsConfig }) => {
      this.smsConfig = smsConfig;
    });
  }

  previousState() {
    window.history.back();
  }
}
