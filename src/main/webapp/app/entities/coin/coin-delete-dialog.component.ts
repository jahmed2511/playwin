import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICoin } from 'app/shared/model/coin.model';
import { CoinService } from './coin.service';

@Component({
  selector: 'jhi-coin-delete-dialog',
  templateUrl: './coin-delete-dialog.component.html'
})
export class CoinDeleteDialogComponent {
  coin: ICoin;

  constructor(protected coinService: CoinService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.coinService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'coinListModification',
        content: 'Deleted an coin'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-coin-delete-popup',
  template: ''
})
export class CoinDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ coin }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(CoinDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.coin = coin;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/coin', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/coin', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
