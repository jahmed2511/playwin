import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ICoin, Coin } from 'app/shared/model/coin.model';
import { CoinService } from './coin.service';
import { IUser, UserService } from 'app/core';
import { IGame } from 'app/shared/model/game.model';
import { GameService } from 'app/entities/game';

@Component({
  selector: 'jhi-coin-update',
  templateUrl: './coin-update.component.html'
})
export class CoinUpdateComponent implements OnInit {
  coin: ICoin;
  isSaving: boolean;

  users: IUser[];

  games: IGame[];

  editForm = this.fb.group({
    id: [],
    cointType: [null, [Validators.required]],
    displayName: [null, [Validators.required]],
    basePrice: [null, [Validators.required]],
    baseWinningPrice: [null, [Validators.required]],
    description: [],
    user: [],
    game: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected coinService: CoinService,
    protected userService: UserService,
    protected gameService: GameService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ coin }) => {
      this.updateForm(coin);
      this.coin = coin;
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.gameService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IGame[]>) => mayBeOk.ok),
        map((response: HttpResponse<IGame[]>) => response.body)
      )
      .subscribe((res: IGame[]) => (this.games = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(coin: ICoin) {
    this.editForm.patchValue({
      id: coin.id,
      cointType: coin.cointType,
      displayName: coin.displayName,
      basePrice: coin.basePrice,
      baseWinningPrice: coin.baseWinningPrice,
      description: coin.description,
      user: coin.user,
      game: coin.game
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const coin = this.createFromForm();
    if (coin.id !== undefined) {
      this.subscribeToSaveResponse(this.coinService.update(coin));
    } else {
      this.subscribeToSaveResponse(this.coinService.create(coin));
    }
  }

  private createFromForm(): ICoin {
    const entity = {
      ...new Coin(),
      id: this.editForm.get(['id']).value,
      cointType: this.editForm.get(['cointType']).value,
      displayName: this.editForm.get(['displayName']).value,
      basePrice: this.editForm.get(['basePrice']).value,
      baseWinningPrice: this.editForm.get(['baseWinningPrice']).value,
      description: this.editForm.get(['description']).value,
      user: this.editForm.get(['user']).value,
      game: this.editForm.get(['game']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICoin>>) {
    result.subscribe((res: HttpResponse<ICoin>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackGameById(index: number, item: IGame) {
    return item.id;
  }
}
