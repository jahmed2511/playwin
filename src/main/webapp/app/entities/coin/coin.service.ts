import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICoin } from 'app/shared/model/coin.model';

type EntityResponseType = HttpResponse<ICoin>;
type EntityArrayResponseType = HttpResponse<ICoin[]>;

@Injectable({ providedIn: 'root' })
export class CoinService {
  public resourceUrl = SERVER_API_URL + 'api/coins';
  public resourceUrl_gameId = SERVER_API_URL + 'api/game-coins';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/coins';

  constructor(protected http: HttpClient) {}

  create(coin: ICoin): Observable<EntityResponseType> {
    return this.http.post<ICoin>(this.resourceUrl, coin, { observe: 'response' });
  }

  update(coin: ICoin): Observable<EntityResponseType> {
    return this.http.put<ICoin>(this.resourceUrl, coin, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICoin>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
  findByGameId(id: number): Observable<EntityArrayResponseType> {
    return this.http.get<ICoin[]>(`${this.resourceUrl_gameId}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICoin[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICoin[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
