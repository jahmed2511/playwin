import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { PlaywinSharedModule } from 'app/shared';
import {
  CoinComponent,
  CoinDetailComponent,
  CoinUpdateComponent,
  CoinDeletePopupComponent,
  CoinDeleteDialogComponent,
  coinRoute,
  coinPopupRoute
} from './';

const ENTITY_STATES = [...coinRoute, ...coinPopupRoute];

@NgModule({
  imports: [PlaywinSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [CoinComponent, CoinDetailComponent, CoinUpdateComponent, CoinDeleteDialogComponent, CoinDeletePopupComponent],
  entryComponents: [CoinComponent, CoinUpdateComponent, CoinDeleteDialogComponent, CoinDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PlaywinCoinModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
