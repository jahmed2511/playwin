export * from './coin.service';
export * from './coin-update.component';
export * from './coin-delete-dialog.component';
export * from './coin-detail.component';
export * from './coin.component';
export * from './coin.route';
