import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Coin } from 'app/shared/model/coin.model';
import { CoinService } from './coin.service';
import { CoinComponent } from './coin.component';
import { CoinDetailComponent } from './coin-detail.component';
import { CoinUpdateComponent } from './coin-update.component';
import { CoinDeletePopupComponent } from './coin-delete-dialog.component';
import { ICoin } from 'app/shared/model/coin.model';

@Injectable({ providedIn: 'root' })
export class CoinResolve implements Resolve<ICoin> {
  constructor(private service: CoinService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICoin> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Coin>) => response.ok),
        map((coin: HttpResponse<Coin>) => coin.body)
      );
    }
    return of(new Coin());
  }
}

export const coinRoute: Routes = [
  {
    path: '',
    component: CoinComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      defaultSort: 'id,asc',
      pageTitle: 'playwinApp.coin.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CoinDetailComponent,
    resolve: {
      coin: CoinResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'playwinApp.coin.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CoinUpdateComponent,
    resolve: {
      coin: CoinResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'playwinApp.coin.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CoinUpdateComponent,
    resolve: {
      coin: CoinResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'playwinApp.coin.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const coinPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: CoinDeletePopupComponent,
    resolve: {
      coin: CoinResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'playwinApp.coin.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
