import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PlaywinSharedModule } from 'app/shared/shared.module';
import { AgentCommisionComponent } from './agent-commision.component';
import { AgentCommisionDetailComponent } from './agent-commision-detail.component';
import { AgentCommisionUpdateComponent } from './agent-commision-update.component';
import { AgentCommisionDeletePopupComponent, AgentCommisionDeleteDialogComponent } from './agent-commision-delete-dialog.component';
import { agentCommisionRoute, agentCommisionPopupRoute } from './agent-commision.route';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

const ENTITY_STATES = [...agentCommisionRoute, ...agentCommisionPopupRoute];

@NgModule({
  imports: [PlaywinSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    AgentCommisionComponent,
    AgentCommisionDetailComponent,
    AgentCommisionUpdateComponent,
    AgentCommisionDeleteDialogComponent,
    AgentCommisionDeletePopupComponent
  ],
  entryComponents: [AgentCommisionDeleteDialogComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PlaywinAgentCommisionModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
