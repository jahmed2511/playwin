import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IAgentCommision, AgentCommision } from 'app/shared/model/agent-commision.model';
import { AgentCommisionService } from './agent-commision.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IGame } from 'app/shared/model/game.model';
import { GameService } from 'app/entities/game/game.service';
import { ICoin } from 'app/shared/model/coin.model';
import { CoinService } from 'app/entities/coin/coin.service';

@Component({
  selector: 'jhi-agent-commision-update',
  templateUrl: './agent-commision-update.component.html'
})
export class AgentCommisionUpdateComponent implements OnInit {
  isSaving: boolean;

  users: IUser[];

  coins: ICoin[];

  games: IGame[];

  agentCommision: AgentCommision;

  editForm = this.fb.group({
    id: [],
    commisionPercentage: [null, [Validators.required]],
    user: [null, Validators.required],
    coin: [null, Validators.required],
    game: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected agentCommisionService: AgentCommisionService,
    protected userService: UserService,
    protected gameService: GameService,
    protected coinService: CoinService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ agentCommision }) => {
      this.updateForm(agentCommision);
      this.agentCommision = agentCommision;
      this.gameChange(agentCommision.game.id);
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.gameService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IGame[]>) => mayBeOk.ok),
        map((response: HttpResponse<IGame[]>) => response.body)
      )
      .subscribe((res: IGame[]) => (this.games = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(agentCommision: IAgentCommision) {
    this.editForm.patchValue({
      id: agentCommision.id,
      commisionPercentage: agentCommision.commisionPercentage,
      user: agentCommision.user,
      coin: agentCommision.coin,
      game: agentCommision.game
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const agentCommision = this.createFromForm();
    if (agentCommision.id !== undefined) {
      this.subscribeToSaveResponse(this.agentCommisionService.update(agentCommision));
    } else {
      this.subscribeToSaveResponse(this.agentCommisionService.create(agentCommision));
    }
  }

  private createFromForm(): IAgentCommision {
    return {
      ...new AgentCommision(),
      id: this.editForm.get(['id']).value,
      commisionPercentage: this.editForm.get(['commisionPercentage']).value,
      user: this.editForm.get(['user']).value,
      coin: this.editForm.get(['coin']).value,
      game: this.editForm.get(['game']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAgentCommision>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackCoinById(index: number, item: ICoin) {
    return item.id;
  }

  trackGameById(index: number, item: IGame) {
    return item.id;
  }
  gameChange(id) {
    if (id === null) {
      id = this.editForm.get('game').value.id;
    }
    this.coinService
      .findByGameId(id)
      .subscribe(res => (this.coins = res.body), (subRes: HttpErrorResponse) => this.onError(subRes.message));
  }
}
