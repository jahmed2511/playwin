import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAgentCommision } from 'app/shared/model/agent-commision.model';

type EntityResponseType = HttpResponse<IAgentCommision>;
type EntityArrayResponseType = HttpResponse<IAgentCommision[]>;

@Injectable({ providedIn: 'root' })
export class AgentCommisionService {
  public resourceUrl = SERVER_API_URL + 'api/agent-commisions';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/agent-commisions';

  constructor(protected http: HttpClient) {}

  create(agentCommision: IAgentCommision): Observable<EntityResponseType> {
    return this.http.post<IAgentCommision>(this.resourceUrl, agentCommision, { observe: 'response' });
  }

  update(agentCommision: IAgentCommision): Observable<EntityResponseType> {
    return this.http.put<IAgentCommision>(this.resourceUrl, agentCommision, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAgentCommision>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAgentCommision[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAgentCommision[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
