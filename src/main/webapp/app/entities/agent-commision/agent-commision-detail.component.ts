import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAgentCommision } from 'app/shared/model/agent-commision.model';

@Component({
  selector: 'jhi-agent-commision-detail',
  templateUrl: './agent-commision-detail.component.html'
})
export class AgentCommisionDetailComponent implements OnInit {
  agentCommision: IAgentCommision;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ agentCommision }) => {
      this.agentCommision = agentCommision;
    });
  }

  previousState() {
    window.history.back();
  }
}
