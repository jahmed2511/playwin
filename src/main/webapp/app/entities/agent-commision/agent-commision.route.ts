import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { AgentCommision } from 'app/shared/model/agent-commision.model';
import { AgentCommisionService } from './agent-commision.service';
import { AgentCommisionComponent } from './agent-commision.component';
import { AgentCommisionDetailComponent } from './agent-commision-detail.component';
import { AgentCommisionUpdateComponent } from './agent-commision-update.component';
import { AgentCommisionDeletePopupComponent } from './agent-commision-delete-dialog.component';
import { IAgentCommision } from 'app/shared/model/agent-commision.model';

@Injectable({ providedIn: 'root' })
export class AgentCommisionResolve implements Resolve<IAgentCommision> {
  constructor(private service: AgentCommisionService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAgentCommision> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<AgentCommision>) => response.ok),
        map((agentCommision: HttpResponse<AgentCommision>) => agentCommision.body)
      );
    }
    return of(new AgentCommision());
  }
}

export const agentCommisionRoute: Routes = [
  {
    path: '',
    component: AgentCommisionComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_DEV'],
      defaultSort: 'id,asc',
      pageTitle: 'playwinApp.agentCommision.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: AgentCommisionDetailComponent,
    resolve: {
      agentCommision: AgentCommisionResolve
    },
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_DEV'],
      pageTitle: 'playwinApp.agentCommision.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: AgentCommisionUpdateComponent,
    resolve: {
      agentCommision: AgentCommisionResolve
    },
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_DEV'],
      pageTitle: 'playwinApp.agentCommision.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: AgentCommisionUpdateComponent,
    resolve: {
      agentCommision: AgentCommisionResolve
    },
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_DEV'],
      pageTitle: 'playwinApp.agentCommision.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const agentCommisionPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: AgentCommisionDeletePopupComponent,
    resolve: {
      agentCommision: AgentCommisionResolve
    },
    data: {
      authorities: ['ROLE_ADMIN', 'ROLE_DEV'],
      pageTitle: 'playwinApp.agentCommision.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
