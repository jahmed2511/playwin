import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAgentCommision } from 'app/shared/model/agent-commision.model';
import { AgentCommisionService } from './agent-commision.service';

@Component({
  selector: 'jhi-agent-commision-delete-dialog',
  templateUrl: './agent-commision-delete-dialog.component.html'
})
export class AgentCommisionDeleteDialogComponent {
  agentCommision: IAgentCommision;

  constructor(
    protected agentCommisionService: AgentCommisionService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.agentCommisionService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'agentCommisionListModification',
        content: 'Deleted an agentCommision'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-agent-commision-delete-popup',
  template: ''
})
export class AgentCommisionDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ agentCommision }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(AgentCommisionDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.agentCommision = agentCommision;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/agent-commision', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/agent-commision', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
