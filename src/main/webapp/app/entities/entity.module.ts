import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'game',
        loadChildren: './game/game.module#PlaywinGameModule'
      },
      {
        path: 'coin',
        loadChildren: './coin/coin.module#PlaywinCoinModule'
      },
      {
        path: 'wallet',
        loadChildren: './wallet/wallet.module#PlaywinWalletModule'
      },
      {
        path: 'bid',
        loadChildren: './bid/bid.module#PlaywinBidModule'
      },
      {
        path: 'game-result',
        loadChildren: './game-result/game-result.module#PlaywinGameResultModule'
      },
      {
        path: 'user-pref',
        loadChildren: './user-pref/user-pref.module#PlaywinUserPrefModule'
      },
      {
        path: 'play-game',
        loadChildren: './play-game/play-game.module#PlaywinPlayGameModule'
      },
      {
        path: 'redeem',
        loadChildren: './redeem/redeem.module#PlaywinRedeemModule'
      },
      {
        path: 'sms-config',
        loadChildren: './sms-config/sms-config.module#PlaywinSmsConfigModule'
      },
      {
        path: 'publish',
        loadChildren: './publish/publish.module#PlaywinPublishModule'
      },
      {
        path: 'game-draw-config',
        loadChildren: () => import('./game-draw-config/game-draw-config.module').then(m => m.PlaywinGameDrawConfigModule)
      },
      {
        path: 'agent-commision',
        loadChildren: () => import('./agent-commision/agent-commision.module').then(m => m.PlaywinAgentCommisionModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PlaywinEntityModule {}
