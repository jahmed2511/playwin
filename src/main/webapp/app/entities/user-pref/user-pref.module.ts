import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { PlaywinSharedModule } from 'app/shared';
import {
  UserPrefComponent,
  UserPrefDetailComponent,
  UserPrefUpdateComponent,
  UserPrefDeletePopupComponent,
  UserPrefDeleteDialogComponent,
  userPrefRoute,
  userPrefPopupRoute
} from './';

const ENTITY_STATES = [...userPrefRoute, ...userPrefPopupRoute];

@NgModule({
  imports: [PlaywinSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    UserPrefComponent,
    UserPrefDetailComponent,
    UserPrefUpdateComponent,
    UserPrefDeleteDialogComponent,
    UserPrefDeletePopupComponent
  ],
  entryComponents: [UserPrefComponent, UserPrefUpdateComponent, UserPrefDeleteDialogComponent, UserPrefDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PlaywinUserPrefModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
