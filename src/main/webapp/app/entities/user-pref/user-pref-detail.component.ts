import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUserPref } from 'app/shared/model/user-pref.model';

@Component({
  selector: 'jhi-user-pref-detail',
  templateUrl: './user-pref-detail.component.html'
})
export class UserPrefDetailComponent implements OnInit {
  userPref: IUserPref;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ userPref }) => {
      this.userPref = userPref;
    });
  }

  previousState() {
    window.history.back();
  }
}
