import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IUserPref, UserPref } from 'app/shared/model/user-pref.model';
import { UserPrefService } from './user-pref.service';
import { IUser, UserService } from 'app/core';

@Component({
  selector: 'jhi-user-pref-update',
  templateUrl: './user-pref-update.component.html'
})
export class UserPrefUpdateComponent implements OnInit {
  userPref: IUserPref;
  isSaving: boolean;

  users: IUser[];

  editForm = this.fb.group({
    id: [],
    distributorShare: [null, [Validators.required]],
    isSpecificPrice: [null, [Validators.required]],
    mobileNo: [null, [Validators.required]],
    user: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected userPrefService: UserPrefService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ userPref }) => {
      this.updateForm(userPref);
      this.userPref = userPref;
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(userPref: IUserPref) {
    this.editForm.patchValue({
      id: userPref.id,
      distributorShare: userPref.distributorShare,
      isSpecificPrice: userPref.isSpecificPrice,
      mobileNo: userPref.mobileNo,
      user: userPref.user
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const userPref = this.createFromForm();
    if (userPref.id !== undefined) {
      this.subscribeToSaveResponse(this.userPrefService.update(userPref));
    } else {
      this.subscribeToSaveResponse(this.userPrefService.create(userPref));
    }
  }

  private createFromForm(): IUserPref {
    const entity = {
      ...new UserPref(),
      id: this.editForm.get(['id']).value,
      distributorShare: this.editForm.get(['distributorShare']).value,
      isSpecificPrice: this.editForm.get(['isSpecificPrice']).value,
      mobileNo: this.editForm.get(['mobileNo']).value,
      user: this.editForm.get(['user']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserPref>>) {
    result.subscribe((res: HttpResponse<IUserPref>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }
}
