import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IUserPref } from 'app/shared/model/user-pref.model';
import { UserPrefService } from './user-pref.service';

@Component({
  selector: 'jhi-user-pref-delete-dialog',
  templateUrl: './user-pref-delete-dialog.component.html'
})
export class UserPrefDeleteDialogComponent {
  userPref: IUserPref;

  constructor(protected userPrefService: UserPrefService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.userPrefService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'userPrefListModification',
        content: 'Deleted an userPref'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-user-pref-delete-popup',
  template: ''
})
export class UserPrefDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ userPref }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(UserPrefDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.userPref = userPref;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/user-pref', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/user-pref', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
