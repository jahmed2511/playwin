import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IUserPref } from 'app/shared/model/user-pref.model';

type EntityResponseType = HttpResponse<IUserPref>;
type EntityArrayResponseType = HttpResponse<IUserPref[]>;

@Injectable({ providedIn: 'root' })
export class UserPrefService {
  public resourceUrl = SERVER_API_URL + 'api/user-prefs';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/user-prefs';

  constructor(protected http: HttpClient) {}

  create(userPref: IUserPref): Observable<EntityResponseType> {
    return this.http.post<IUserPref>(this.resourceUrl, userPref, { observe: 'response' });
  }

  update(userPref: IUserPref): Observable<EntityResponseType> {
    return this.http.put<IUserPref>(this.resourceUrl, userPref, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IUserPref>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUserPref[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUserPref[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
