import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { UserPref } from 'app/shared/model/user-pref.model';
import { UserPrefService } from './user-pref.service';
import { UserPrefComponent } from './user-pref.component';
import { UserPrefDetailComponent } from './user-pref-detail.component';
import { UserPrefUpdateComponent } from './user-pref-update.component';
import { UserPrefDeletePopupComponent } from './user-pref-delete-dialog.component';
import { IUserPref } from 'app/shared/model/user-pref.model';

@Injectable({ providedIn: 'root' })
export class UserPrefResolve implements Resolve<IUserPref> {
  constructor(private service: UserPrefService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IUserPref> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<UserPref>) => response.ok),
        map((userPref: HttpResponse<UserPref>) => userPref.body)
      );
    }
    return of(new UserPref());
  }
}

export const userPrefRoute: Routes = [
  {
    path: '',
    component: UserPrefComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'playwinApp.userPref.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: UserPrefDetailComponent,
    resolve: {
      userPref: UserPrefResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'playwinApp.userPref.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: UserPrefUpdateComponent,
    resolve: {
      userPref: UserPrefResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'playwinApp.userPref.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: UserPrefUpdateComponent,
    resolve: {
      userPref: UserPrefResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'playwinApp.userPref.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const userPrefPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: UserPrefDeletePopupComponent,
    resolve: {
      userPref: UserPrefResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'playwinApp.userPref.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
