import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRedeem } from 'app/shared/model/redeem.model';

@Component({
  selector: 'jhi-redeem-detail',
  templateUrl: './redeem-detail.component.html'
})
export class RedeemDetailComponent implements OnInit {
  redeem: IRedeem;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ redeem }) => {
      this.redeem = redeem;
    });
  }

  previousState() {
    window.history.back();
  }
}
