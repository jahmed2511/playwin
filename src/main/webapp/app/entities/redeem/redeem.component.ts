import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IRedeem } from 'app/shared/model/redeem.model';
import { AccountService, IUser, User } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { RedeemService } from './redeem.service';
import * as moment from 'moment';
import { WalletService } from '../wallet';

@Component({
  selector: 'jhi-redeem',
  templateUrl: './redeem.component.html'
})
export class RedeemComponent implements OnInit, OnDestroy {
  currentAccount: any;
  redeems: IRedeem[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  currentSearch: string;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  currUser: IUser = new User();

  constructor(
    protected redeemService: RedeemService,
    protected parseLinks: JhiParseLinks,
    protected jhiAlertService: JhiAlertService,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected walletService: WalletService
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ? this.activatedRoute.snapshot.params['search'] : '';
  }

  loadAll() {
    if (this.currUser.id) {
      this.redeemService
        .query({
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: ['lastModifiedOn', ',', 'desc'],
          'criteria.userId': this.currUser.id
        })
        .subscribe(
          (res: HttpResponse<IRedeem[]>) => this.paginateRedeems(res.body, res.headers),
          (res: HttpErrorResponse) => this.onError(res.message)
        );
    } else {
      this.redeemService
        .query({
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: ['lastModifiedOn', ',', 'desc']
        })
        .subscribe(
          (res: HttpResponse<IRedeem[]>) => this.paginateRedeems(res.body, res.headers),
          (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
  }
  loadforAdmin() {
    console.log('loading for adming....');
    this.redeemService
      .query({
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: ['lastModifiedOn', ',', 'desc']
      })
      .subscribe(
        (res: HttpResponse<IRedeem[]>) => this.paginateRedeems(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }
  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/redeem'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        search: this.currentSearch,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  clear() {
    this.page = 0;
    this.currentSearch = '';
    this.router.navigate([
      '/redeem',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  search(query) {
    if (!query) {
      return this.clear();
    }
    this.page = 0;
    this.currentSearch = query;
    this.router.navigate([
      '/redeem',
      {
        search: this.currentSearch,
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.accountService.identity().then(account => {
      this.currentAccount = account;
      if (this.currentAccount.authorities.indexOf('ROLE_ADMIN') !== -1) {
        this.loadforAdmin();
      } else {
        this.loadAll();
      }
    });
    this.registerChangeInRedeems();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IRedeem) {
    return item.id;
  }

  registerChangeInRedeems() {
    this.eventSubscriber = this.eventManager.subscribe('redeemListModification', response => this.loadAll());
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }
  approve(redeem: IRedeem) {
    this.redeemService.update(redeem).subscribe(response => {
      if (response.status === 200) {
        this.error = null;
        this.success = 'OK';
        this.loadAll();
        this.walletService.notifyUpdateWalletEvent(true);
        console.log('approved');
      } else {
        this.success = null;
        this.error = 'ERROR';
      }
    });
  }

  protected paginateRedeems(data: IRedeem[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.redeems = data;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
  loadRedeem(user) {
    this.currUser = user;
    this.loadAll();
  }
}
