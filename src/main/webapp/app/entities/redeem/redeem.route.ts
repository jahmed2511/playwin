import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Redeem } from 'app/shared/model/redeem.model';
import { RedeemService } from './redeem.service';
import { RedeemComponent } from './redeem.component';
import { RedeemDetailComponent } from './redeem-detail.component';
import { RedeemUpdateComponent } from './redeem-update.component';
import { RedeemDeletePopupComponent } from './redeem-delete-dialog.component';
import { IRedeem } from 'app/shared/model/redeem.model';

@Injectable({ providedIn: 'root' })
export class RedeemResolve implements Resolve<IRedeem> {
  constructor(private service: RedeemService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IRedeem> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Redeem>) => response.ok),
        map((redeem: HttpResponse<Redeem>) => redeem.body)
      );
    }
    return of(new Redeem());
  }
}

export const redeemRoute: Routes = [
  {
    path: '',
    component: RedeemComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_DEV', 'ROLE_AGENT'],
      defaultSort: 'id,asc',
      pageTitle: 'playwinApp.redeem.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: RedeemDetailComponent,
    resolve: {
      redeem: RedeemResolve
    },
    data: {
      authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_DEV', 'ROLE_AGENT'],
      pageTitle: 'playwinApp.redeem.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: RedeemUpdateComponent,
    resolve: {
      redeem: RedeemResolve
    },
    data: {
      authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_DEV', 'ROLE_AGENT'],
      pageTitle: 'playwinApp.redeem.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: RedeemUpdateComponent,
    resolve: {
      redeem: RedeemResolve
    },
    data: {
      authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_DEV', 'ROLE_AGENT'],
      pageTitle: 'playwinApp.redeem.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const redeemPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: RedeemDeletePopupComponent,
    resolve: {
      redeem: RedeemResolve
    },
    data: {
      authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_DEV', 'ROLE_AGENT'],
      pageTitle: 'playwinApp.redeem.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
