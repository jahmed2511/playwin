import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { PlaywinSharedModule } from 'app/shared';
import {
  RedeemComponent,
  RedeemDetailComponent,
  RedeemUpdateComponent,
  RedeemDeletePopupComponent,
  RedeemDeleteDialogComponent,
  redeemRoute,
  redeemPopupRoute
} from './';

const ENTITY_STATES = [...redeemRoute, ...redeemPopupRoute];

@NgModule({
  imports: [PlaywinSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [RedeemComponent, RedeemDetailComponent, RedeemUpdateComponent, RedeemDeleteDialogComponent, RedeemDeletePopupComponent],
  entryComponents: [RedeemComponent, RedeemUpdateComponent, RedeemDeleteDialogComponent, RedeemDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PlaywinRedeemModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
