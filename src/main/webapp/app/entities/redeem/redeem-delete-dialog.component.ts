import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRedeem } from 'app/shared/model/redeem.model';
import { RedeemService } from './redeem.service';

@Component({
  selector: 'jhi-redeem-delete-dialog',
  templateUrl: './redeem-delete-dialog.component.html'
})
export class RedeemDeleteDialogComponent {
  redeem: IRedeem;

  constructor(protected redeemService: RedeemService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.redeemService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'redeemListModification',
        content: 'Deleted an redeem'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-redeem-delete-popup',
  template: ''
})
export class RedeemDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ redeem }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(RedeemDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.redeem = redeem;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/redeem', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/redeem', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
