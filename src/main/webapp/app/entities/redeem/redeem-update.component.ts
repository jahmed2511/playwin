import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IRedeem, Redeem } from 'app/shared/model/redeem.model';
import { RedeemService } from './redeem.service';
import { IUser, UserService } from 'app/core';
import { WalletService } from '../wallet';

@Component({
  selector: 'jhi-redeem-update',
  templateUrl: './redeem-update.component.html'
})
export class RedeemUpdateComponent implements OnInit {
  redeem: IRedeem;
  isSaving: boolean;

  users: IUser[];

  editForm = this.fb.group({
    id: [],
    status: [],
    amount: [null, [Validators.required]],
    approvedBy: [],
    remarks: [null, [Validators.required]],
    createdOn: [],
    lastModifiedOn: [],
    user: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected redeemService: RedeemService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    protected walletService: WalletService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ redeem }) => {
      this.updateForm(redeem);
      this.redeem = redeem;
    });
  }

  updateForm(redeem: IRedeem) {
    this.editForm.patchValue({
      id: redeem.id,
      status: redeem.status,
      amount: redeem.amount,
      approvedBy: redeem.approvedBy,
      remarks: redeem.remarks,
      createdOn: redeem.createdOn != null ? redeem.createdOn.format(DATE_TIME_FORMAT) : null,
      lastModifiedOn: redeem.lastModifiedOn != null ? redeem.lastModifiedOn.format(DATE_TIME_FORMAT) : null,
      user: redeem.user
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const redeem = this.createFromForm();
    if (redeem.id !== undefined) {
      this.subscribeToSaveResponse(this.redeemService.update(redeem));
    } else {
      this.subscribeToSaveResponse(this.redeemService.create(redeem));
    }
  }

  private createFromForm(): IRedeem {
    const entity = {
      ...new Redeem(),
      id: this.editForm.get(['id']).value,
      status: this.editForm.get(['status']).value,
      amount: this.editForm.get(['amount']).value,
      approvedBy: null,
      remarks: this.editForm.get(['remarks']).value,
      createdOn: null,
      lastModifiedOn: null,
      user: null
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRedeem>>) {
    result.subscribe((res: HttpResponse<IRedeem>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }
}
