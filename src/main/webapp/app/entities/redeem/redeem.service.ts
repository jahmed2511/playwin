import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IRedeem } from 'app/shared/model/redeem.model';

type EntityResponseType = HttpResponse<IRedeem>;
type EntityArrayResponseType = HttpResponse<IRedeem[]>;

@Injectable({ providedIn: 'root' })
export class RedeemService {
  public resourceUrl = SERVER_API_URL + 'api/redeems';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/redeems';

  constructor(protected http: HttpClient) {}

  create(redeem: IRedeem): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(redeem);
    return this.http
      .post<IRedeem>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(redeem: IRedeem): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(redeem);
    return this.http
      .put<IRedeem>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IRedeem>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IRedeem[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IRedeem[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(redeem: IRedeem): IRedeem {
    const copy: IRedeem = Object.assign({}, redeem, {
      createdOn: redeem.createdOn != null && redeem.createdOn.isValid() ? redeem.createdOn.toJSON() : null,
      lastModifiedOn: redeem.lastModifiedOn != null && redeem.lastModifiedOn.isValid() ? redeem.lastModifiedOn.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdOn = res.body.createdOn != null ? moment(res.body.createdOn) : null;
      res.body.lastModifiedOn = res.body.lastModifiedOn != null ? moment(res.body.lastModifiedOn) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((redeem: IRedeem) => {
        redeem.createdOn = redeem.createdOn != null ? moment(redeem.createdOn) : null;
        redeem.lastModifiedOn = redeem.lastModifiedOn != null ? moment(redeem.lastModifiedOn) : null;
      });
    }
    return res;
  }
}
