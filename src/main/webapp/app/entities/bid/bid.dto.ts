import { Moment } from 'moment';
import { Status } from './Status.dto';
import { CoinDTO } from './Coin.dto';

export interface IBidDTO {
  status?: Status;
  date?: Moment;
  gameName?: string;
  totalCoins?: number;
  totalPrice?: string;
  totalWinningPrice?: string;
  totalProfit?: string;
  coinDtos?: CoinDTO[];
}

export class BidDTO implements IBidDTO {
  constructor(
    public status?: Status,
    public date?: Moment,
    public gameName?: string,
    public totalCoins?: number,
    public totalPrice?: string,
    public totalWinningPrice?: string,
    public totalProfit?: string,
    public coinDtos?: CoinDTO[],
    public isHide?: boolean
  ) {}
}
