import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { AccountService, IUser } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { BidService } from './bid.service';
import { IBidDTO, BidDTO } from './bid.dto';
import { Status } from './Status.dto';

@Component({
  selector: 'jhi-bid',
  templateUrl: './bid.component.html',
  styleUrls: ['bid.scss']
})
export class BidComponent implements OnInit, OnDestroy {
  currentAccount: any;
  bids: BidDTO[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  currentSearch: string;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  currUser: IUser;

  constructor(
    protected bidService: BidService,
    protected parseLinks: JhiParseLinks,
    protected jhiAlertService: JhiAlertService,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  loadAll() {
    this.bidService.getUserBidReport(this.currUser.login).subscribe(
      (res: HttpResponse<IBidDTO[]>) => {
        this.bids = res.body;
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/bid'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        search: this.currentSearch,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  clear() {
    this.page = 0;
    this.currentSearch = '';
    this.router.navigate([
      '/bid',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.registerChangeInBids();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  loadBids(user) {
    this.currUser = user;
    this.loadAll();
  }
  registerChangeInBids() {
    this.eventSubscriber = this.eventManager.subscribe('bidListModification', response => this.loadAll());
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateBids(data: IBidDTO[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.bids = data;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
  getIcon(status: Status) {
    switch (status) {
      case Status.Lost:
        break;

      default:
        break;
    }
  }
  getClass(status: Status) {
    switch (status) {
      case Status.Lost:
        return 'table-danger';

      case Status.Win:
        return 'table-success';
      case Status.Partial_Win:
        return 'table-warning';
      case Status.Pending:
        return 'table-light';
      case Status.NotBided:
        return 'table-secondary';
      default:
        break;
    }
  }
}
