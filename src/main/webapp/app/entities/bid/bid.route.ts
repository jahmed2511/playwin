import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Bid } from 'app/shared/model/bid.model';
import { BidService } from './bid.service';
import { BidComponent } from './bid.component';
import { BidDetailComponent } from './bid-detail.component';
import { BidUpdateComponent } from './bid-update.component';
import { BidDeletePopupComponent } from './bid-delete-dialog.component';
import { IBid } from 'app/shared/model/bid.model';

@Injectable({ providedIn: 'root' })
export class BidResolve implements Resolve<IBid> {
  constructor(private service: BidService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBid> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Bid>) => response.ok),
        map((bid: HttpResponse<Bid>) => bid.body)
      );
    }
    return of(new Bid());
  }
}

export const bidRoute: Routes = [
  {
    path: '',
    component: BidComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER', 'ROLE_AGENT', 'ROLE_DEV'],
      defaultSort: 'id,asc',
      pageTitle: 'playwinApp.bid.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BidDetailComponent,
    resolve: {
      bid: BidResolve
    },
    data: {
      authorities: ['ROLE_USER', 'ROLE_AGENT', 'ROLE_DEV'],
      pageTitle: 'playwinApp.bid.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BidUpdateComponent,
    resolve: {
      bid: BidResolve
    },
    data: {
      authorities: ['ROLE_USER', 'ROLE_DEV'],
      pageTitle: 'playwinApp.bid.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BidUpdateComponent,
    resolve: {
      bid: BidResolve
    },
    data: {
      authorities: ['ROLE_USER', 'ROLE_DEV'],
      pageTitle: 'playwinApp.bid.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const bidPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: BidDeletePopupComponent,
    resolve: {
      bid: BidResolve
    },
    data: {
      authorities: ['ROLE_USER', 'ROLE_DEV'],
      pageTitle: 'playwinApp.bid.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
