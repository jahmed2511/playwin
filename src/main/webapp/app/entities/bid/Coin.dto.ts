import { Status } from './Status.dto';

export interface ICoinDTO {
  notBided?: boolean;
  status?: Status;
  price?: string;
  winPrice?: string;
  noOfCoin?: number;
  bidNo?: string;
  totalPrice?: string;
  totalWinningPrice?: string;
  totalProfit?: string;
  displayName?: string;
}
export class CoinDTO implements ICoinDTO {
  constructor(
    notBided?: boolean,
    status?: Status,
    price?: string,
    winPrice?: string,
    noOfCoin?: number,
    bidNo?: string,
    totalPrice?: string,
    totalWinningPrice?: string,
    totalProfit?: string,
    displayName?: string
  ) {}
}
