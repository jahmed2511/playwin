import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { PlaywinSharedModule } from 'app/shared';
import {
  BidComponent,
  BidDetailComponent,
  BidUpdateComponent,
  BidDeletePopupComponent,
  BidDeleteDialogComponent,
  bidRoute,
  bidPopupRoute
} from './';

const ENTITY_STATES = [...bidRoute, ...bidPopupRoute];

@NgModule({
  imports: [PlaywinSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [BidComponent, BidDetailComponent, BidUpdateComponent, BidDeleteDialogComponent, BidDeletePopupComponent],
  entryComponents: [BidComponent, BidUpdateComponent, BidDeleteDialogComponent, BidDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PlaywinBidModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
