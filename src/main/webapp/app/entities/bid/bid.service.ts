import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IBid } from 'app/shared/model/bid.model';
import { IBidDTO } from './bid.dto';

type EntityResponseType = HttpResponse<IBid>;
type EntityArrayResponseType = HttpResponse<IBid[]>;

@Injectable({ providedIn: 'root' })
export class BidService {
  public resourceUrl = SERVER_API_URL + 'api/bids';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/bids';

  constructor(protected http: HttpClient) {}

  create(bid: IBid): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bid);
    return this.http
      .post<IBid>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(bid: IBid): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bid);
    return this.http
      .put<IBid>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IBid>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  getAllUndrowBidsOfCurrentUser(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(null);
    const URI = this.resourceUrl + '/undrow-bid-for-current-user';
    return this.http
      .get<IBid[]>(URI, { params: {}, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }
  getBidForPublish(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(null);
    const URI = this.resourceUrl + '/bid-for-publish';
    return this.http
      .get<IBid[]>(URI, { params: {}, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBid[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }
  getUserBidReport(login: string): Observable<HttpResponse<IBidDTO[]>> {
    //const options = createRequestOption(null);
    return this.http
      .get<IBidDTO[]>(this.resourceUrl + '/report/' + login, { params: {}, observe: 'response' })
      .pipe(map((res: HttpResponse<IBidDTO[]>) => this.convertDateArrayFromServerForDto(res)));
  }
  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBid[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(bid: IBid): IBid {
    const copy: IBid = Object.assign({}, bid, {
      bidTime: bid.bidTime != null && bid.bidTime.isValid() ? bid.bidTime.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.bidTime = res.body.bidTime != null ? moment(res.body.bidTime) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((bid: IBid) => {
        bid.bidTime = bid.bidTime != null ? moment(bid.bidTime) : null;
      });
    }
    return res;
  }
  public convertDateArrayFromServerForDto(res: HttpResponse<IBidDTO[]>): HttpResponse<IBidDTO[]> {
    if (res.body) {
      res.body.forEach((bid: IBidDTO) => {
        bid.date = bid.date != null ? moment(bid.date) : null;
      });
    }
    return res;
  }
}
