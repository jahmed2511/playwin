import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IBid, Bid } from 'app/shared/model/bid.model';
import { BidService } from './bid.service';
import { IGame } from 'app/shared/model/game.model';
import { GameService } from 'app/entities/game';
import { ICoin } from 'app/shared/model/coin.model';
import { CoinService } from 'app/entities/coin';
import { IUser, UserService } from 'app/core';

@Component({
  selector: 'jhi-bid-update',
  templateUrl: './bid-update.component.html'
})
export class BidUpdateComponent implements OnInit {
  bid: IBid;
  isSaving: boolean;

  games: IGame[];

  coins: ICoin[];

  users: IUser[];

  editForm = this.fb.group({
    id: [],
    isWinner: [],
    bidNo: [],
    noOfCoins: [],
    bidTime: [null, [Validators.required]],
    isDrowHappan: [],
    remarks: [],
    game: [],
    coin: [],
    user: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected bidService: BidService,
    protected gameService: GameService,
    protected coinService: CoinService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ bid }) => {
      this.updateForm(bid);
      this.bid = bid;
    });
    this.gameService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IGame[]>) => mayBeOk.ok),
        map((response: HttpResponse<IGame[]>) => response.body)
      )
      .subscribe((res: IGame[]) => (this.games = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.coinService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICoin[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICoin[]>) => response.body)
      )
      .subscribe((res: ICoin[]) => (this.coins = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(bid: IBid) {
    this.editForm.patchValue({
      id: bid.id,
      isWinner: bid.isWinner,
      bidNo: bid.bidNo,
      noOfCoins: bid.noOfCoins,
      bidTime: bid.bidTime != null ? bid.bidTime.format(DATE_TIME_FORMAT) : null,
      isDrowHappan: bid.isDrowHappan,
      remarks: bid.remarks,
      game: bid.game,
      coin: bid.coin,
      user: bid.user
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const bid = this.createFromForm();
    if (bid.id !== undefined) {
      this.subscribeToSaveResponse(this.bidService.update(bid));
    } else {
      this.subscribeToSaveResponse(this.bidService.create(bid));
    }
  }

  private createFromForm(): IBid {
    const entity = {
      ...new Bid(),
      id: this.editForm.get(['id']).value,
      isWinner: this.editForm.get(['isWinner']).value,
      bidNo: this.editForm.get(['bidNo']).value,
      noOfCoins: this.editForm.get(['noOfCoins']).value,
      bidTime: this.editForm.get(['bidTime']).value != null ? moment(this.editForm.get(['bidTime']).value, DATE_TIME_FORMAT) : undefined,
      isDrowHappan: this.editForm.get(['isDrowHappan']).value,
      remarks: this.editForm.get(['remarks']).value,
      game: this.editForm.get(['game']).value,
      coin: this.editForm.get(['coin']).value,
      user: this.editForm.get(['user']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBid>>) {
    result.subscribe((res: HttpResponse<IBid>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackGameById(index: number, item: IGame) {
    return item.id;
  }

  trackCoinById(index: number, item: ICoin) {
    return item.id;
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }
}
