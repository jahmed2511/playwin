import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBid } from 'app/shared/model/bid.model';
import { BidService } from './bid.service';

@Component({
  selector: 'jhi-bid-delete-dialog',
  templateUrl: './bid-delete-dialog.component.html'
})
export class BidDeleteDialogComponent {
  bid: IBid;

  constructor(protected bidService: BidService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.bidService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'bidListModification',
        content: 'Deleted an bid'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-bid-delete-popup',
  template: ''
})
export class BidDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ bid }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(BidDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.bid = bid;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/bid', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/bid', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
