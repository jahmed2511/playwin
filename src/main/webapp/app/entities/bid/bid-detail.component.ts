import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBid } from 'app/shared/model/bid.model';

@Component({
  selector: 'jhi-bid-detail',
  templateUrl: './bid-detail.component.html'
})
export class BidDetailComponent implements OnInit {
  bid: IBid;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ bid }) => {
      this.bid = bid;
    });
  }

  previousState() {
    window.history.back();
  }
}
