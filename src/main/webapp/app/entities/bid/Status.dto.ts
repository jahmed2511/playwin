import { getInjectionTokens } from '@angular/core/src/render3/discovery_utils';

export enum Status {
  NotBided = 'Not Bided',
  Pending = 'Pending',
  Win = 'Win',
  Lost = 'Lost',
  Partial_Win = 'Partial Win'
}
