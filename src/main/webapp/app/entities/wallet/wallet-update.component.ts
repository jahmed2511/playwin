import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IWallet, Wallet } from 'app/shared/model/wallet.model';
import { WalletService } from './wallet.service';
import { IUser, UserService } from 'app/core';

@Component({
  selector: 'jhi-wallet-update',
  templateUrl: './wallet-update.component.html'
})
export class WalletUpdateComponent implements OnInit {
  wallet: IWallet;
  isSaving: boolean;

  users: IUser[];

  editForm = this.fb.group({
    id: [],
    balance: [null, [Validators.required]],
    date: [null, [Validators.required]],
    credit: [],
    debit: [],
    remarks: [],
    user: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected walletService: WalletService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ wallet }) => {
      this.updateForm(wallet);
      this.wallet = wallet;
    });
    this.userService
      .query({
        page: 0,
        size: 1000000000
      })
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(wallet: IWallet) {
    this.editForm.patchValue({
      id: wallet.id,
      balance: wallet.balance,
      date: wallet.date != null ? wallet.date.format(DATE_TIME_FORMAT) : null,
      credit: wallet.credit,
      debit: wallet.debit,
      remarks: wallet.remarks,
      user: wallet.user
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const wallet = this.createFromForm();
    if (wallet.id !== undefined) {
      this.subscribeToSaveResponse(this.walletService.update(wallet));
    } else {
      this.subscribeToSaveResponse(this.walletService.create(wallet));
    }
  }

  private createFromForm(): IWallet {
    const entity = {
      ...new Wallet(),
      id: this.editForm.get(['id']).value,
      balance: 0,
      date: moment('2018-01-01 10:10', DATE_TIME_FORMAT),
      credit: this.editForm.get(['credit']).value,
      debit: this.editForm.get(['debit']).value,
      remarks: this.editForm.get(['remarks']).value,
      user: this.editForm.get(['user']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWallet>>) {
    result.subscribe((res: HttpResponse<IWallet>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.walletService.notifyUpdateWalletEvent(true);
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }
}
