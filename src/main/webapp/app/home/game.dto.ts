import { Moment } from 'moment';
import { IGame } from 'app/shared/model/game.model';
import { GameResult } from 'app/shared/model/game-result.model';
import { CountdownComponent } from 'ngx-countdown';

export class GameDto {
  constructor(
    public game?: IGame,
    public isCollapse?: Boolean,
    public image?: String,
    public gameResult?: GameResult[],
    public leftTime?: number,
    public countdown?: CountdownComponent,
    public isGameDrowInProgress?: boolean,
    public nextDrowTime?: Moment
  ) {}
}
