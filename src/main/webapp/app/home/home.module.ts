import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap/';

import { PlaywinSharedModule } from 'app/shared';
import { HOME_ROUTE, HomeComponent } from './';

import { CountdownModule } from 'ngx-countdown';

import { GameService } from 'app/entities/game/game.service';

import { GameResultService } from 'app/entities/game-result/game-result.service';

@NgModule({
  imports: [PlaywinSharedModule, CountdownModule, RouterModule.forChild([HOME_ROUTE]), NgbModule],
  providers: [GameService, GameResultService],
  declarations: [HomeComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PlaywinHomeModule {}
