import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';

import { LoginModalService, AccountService, Account } from 'app/core';
import { GameService } from 'app/entities/game/';
import { IGame } from 'app/shared/model/game.model';
import { GameDto } from 'app/home/game.dto';
import { GameResultService } from 'app/entities/game-result/game-result.service';
import { IGameResult } from 'app/shared/model/game-result.model';
import { timer } from 'rxjs';
import { Moment } from 'moment';
import * as moment from 'moment';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss']
})
export class HomeComponent implements OnInit {
  //timmer will check in every second
  source = timer(1000, 1000);

  subscribe = this.source.subscribe(val => {
    //console.log(val);
    this.gameDtos.forEach(game => {
      if (!this.isTimeout(game.game)) {
        const leftTime = this.getLeftTime(game.game.interval);
        if (leftTime <= 2 * 60 + 2) {
          // left time is small then 3 min
          game.isGameDrowInProgress = true;
          const gameDto = this.gameDtos.filter(g => g.game.id === game.game.id);
          gameDto[0].isGameDrowInProgress = true;
        }
      }
    });
    if (this.isDirty) {
      this.copy();
      this.isDirty = false;
    }
  });

  account: Account;
  modalRef: NgbModalRef;
  games: IGame[];
  gameDtos: Array<GameDto>;
  copyGameDtos: Array<GameDto>;
  isDirty: boolean;
  images = [
    '../../content/images/carousel/slider-1.jpg',
    '../../content/images/carousel/slider-2.jpg',
    '../../content/images/carousel/slider-3.jpg'
  ];

  constructor(
    private accountService: AccountService,
    private loginModalService: LoginModalService,
    private eventManager: JhiEventManager,
    private gameService: GameService,
    private gameResultService: GameResultService
  ) {}

  ngOnInit() {
    this.accountService.identity().then((account: Account) => {
      this.account = account;
    });
    this.registerAuthenticationSuccess();
    this.gameService
      .getAllActiveGames({
        page: 0
      })
      .subscribe((res: HttpResponse<IGame[]>) => this.assignGames(res.body), (res: HttpErrorResponse) => alert(res.message));
  }

  assignGames(games) {
    this.games = games;
    this.gameDtos = new Array<GameDto>();
    //this.copyGameDtos = new Array<GameDto>();
    let count = 1;
    games.forEach(g => {
      const img = '../../content/images/carousel/slider-' + count + '.jpg';
      const game1 = new GameDto(g, false, img);
      this.gameDtos.push(game1);
      this.loadGameResult(game1, null);
      count++;
    });
  }
  loadGameResult(game: GameDto, counter?: any) {
    console.log('loadGameResult ' + game.game.gameName);
    this.gameResultService
      .query({
        page: 0,
        size: game.game.coins.length,
        'criteria.gameId': game.game.id
      })
      .subscribe(
        (res: HttpResponse<IGameResult[]>) => {
          game.gameResult = res.body;
          game.leftTime = this.getLeftTime(game.game.interval);
          game.nextDrowTime = this.getNextDrowTime(game.gameResult[0].gameTime, game.game.interval);
          if (counter != null) {
            setTimeout(() => {
              counter.restart();
              game.isGameDrowInProgress = false;
            });
          }
          this.isDirty = true;
        },
        (res: HttpErrorResponse) => alert(res.message)
      );
  }

  registerAuthenticationSuccess() {
    this.eventManager.subscribe('authenticationSuccess', message => {
      this.accountService.identity().then(account => {
        this.account = account;
      });
    });
  }

  isAuthenticated() {
    return this.accountService.isAuthenticated();
  }

  login() {
    this.modalRef = this.loginModalService.open();
  }
  getLeftTime(diff) {
    const mint = new Date().getMinutes();
    if (mint < diff) {
      //console.log('game : ' + name + (diff - mint) * 60);
      return (diff - mint) * 60 + (new Date().getSeconds() + 2);
    } else if (mint == diff) {
      const sec = new Date().getSeconds() + 2;
      return sec;
    } else {
      let i = 2;
      while (diff <= 60) {
        if (mint <= diff * i) {
          //console.log('game : ' + name + (diff * i - mint) * 60);
          return (diff * i - mint) * 60 + (new Date().getSeconds() + 2);
        }
        i++;
      }
    }
  }
  timeReached(countd, game) {
    //this.ngxLoader.startLoader('loader-01');
    game.isGameDrowInProgress = true;
    console.log('reached');
    this.loadGameResult(game, countd);
    this.isDirty = true;
  }
  handleEvent(e) {
    console.log(e);
  }
  getNextDrowTime(m: Moment, min: number) {
    if (m !== null && m !== undefined) {
      const copy: Moment = m.clone();
      copy.add(min, 'minutes');
      return copy;
    }
  }
  isTimeout(game: IGame) {
    const currentTime = moment();
    const gameEndTime = game.endTime;
    return (
      gameEndTime.hours() < currentTime.hours() ||
      (gameEndTime.hours() === currentTime.hour() && gameEndTime.minutes() <= currentTime.hour())
    );
  }
  copy() {
    this.copyGameDtos = new Array<GameDto>();
    this.gameDtos.forEach(item => {
      this.copyGameDtos.push(Object.assign({}, item));
    });
  }
}
