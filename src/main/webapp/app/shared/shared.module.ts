import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PlaywinSharedLibsModule, PlaywinSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';
import { UserFilterComponent } from './userfilter/user-filter.component';

@NgModule({
  imports: [PlaywinSharedLibsModule, PlaywinSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective, UserFilterComponent],
  entryComponents: [JhiLoginModalComponent],
  exports: [PlaywinSharedCommonModule, UserFilterComponent, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PlaywinSharedModule {
  static forRoot() {
    return {
      ngModule: PlaywinSharedModule
    };
  }
}
