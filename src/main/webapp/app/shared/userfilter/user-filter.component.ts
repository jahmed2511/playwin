import { Component, OnDestroy, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { JhiAlertService } from 'ng-jhipster';
import { User, IUser, AccountService, UserService } from 'app/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'jhi-user-filter',
  templateUrl: './user-filter.component.html',
  styles: ['li {list-style-type: none;}']
})
export class UserFilterComponent implements OnInit, OnDestroy {
  users: IUser[];
  filterBy: String[] = ['Me', 'Agent', 'Customer'];
  agentList: IUser[] = [];
  userList: IUser[] = [];
  currFilter: String = 'Me';
  currAgent: IUser = new User();
  currUser: IUser;
  @Input('account') currentAccount: any;
  @Output('userChange') userEmiter: EventEmitter<IUser> = new EventEmitter<IUser>();

  constructor(protected accountService: AccountService, protected jhiAlertService: JhiAlertService, private userService: UserService) {}

  ngOnInit(): void {
    if (this.currentAccount == null) {
      this.accountService.identity().then(account => {
        this.currentAccount = account;
        this.userService.find(this.currentAccount.login).subscribe(resp => {
          this.currUser = resp.body;
          this.assignCurrentUser();
        });
      });
    } else {
      this.userService.find(this.currentAccount.login).subscribe(resp => {
        this.currUser = resp.body;
        this.assignCurrentUser();
      });
    }
  }

  assignCurrentUser() {
    this.userEmiter.emit(this.currUser);
  }
  loadAgent() {
    this.userService.findAgent().subscribe(
      (res: HttpResponse<IUser[]>) => {
        this.agentList = res.body;
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
  }
  loadUserOfAgent(user, id) {
    this.userService.findCustomerByAgent(user).subscribe(
      (res: HttpResponse<IUser[]>) => {
        this.userList = res.body;
        this.currUser.login = user;
        this.currUser.id = id;
        this.assignCurrentUser();
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
  }
  loadAllUser() {
    this.userService
      .query({
        page: 0,
        size: 1000000000
      })
      .subscribe(
        (res: HttpResponse<User[]>) => {
          this.userList = res.body;
        },
        (res: HttpResponse<any>) => this.onError(res.body)
      );
  }
  doFilter(type, login, id) {
    switch (type) {
      case 'Filter':
        this.currFilter = login;
        if (this.currFilter === 'Me') {
          this.agentList = [];
          this.userList = [];
          this.currAgent = new User();
          this.ngOnInit();
        } else if (this.currFilter === 'Agent') {
          this.loadAgent();
        } else if (this.currFilter === 'Customer') {
          this.agentList = [];
          this.userList = [];
          this.loadAllUser();
        }

        break;
      case 'AgentFilter':
        console.log(login);
        this.currAgent.login = login;
        this.currAgent.id = id;
        this.userList = [];
        this.loadUserOfAgent(login, id);
        break;
      case 'CustomerFilter':
        this.currUser.login = login;
        this.currUser.id = id;
        this.assignCurrentUser();
        break;

      default:
        break;
    }
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
  ngOnDestroy(): void {
    //this.eventManager.destroy(this.eventSubscriber);
  }
}
