import * as moment from 'moment';
export interface IPublish {
  id?: number;
  gameId?: number;
  coinId?: number;
  publishDate?: moment.Moment;
  isPublish?: boolean;
  publishTime?: string;
  publishNumber?: number;
}

export class Publish implements IPublish {
  constructor(
    public id?: number,
    public gameId?: number,
    public coinId?: number,
    public publishDate?: moment.Moment,
    public isPublish?: boolean,
    public publishNumber?: number,
    public publishTime?: string
  ) {}
}
