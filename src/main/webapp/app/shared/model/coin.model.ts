import { IUser } from 'app/core/user/user.model';
import { IGame } from 'app/shared/model/game.model';
import { IBid } from 'app/shared/model/bid.model';

export interface ICoin {
  id?: number;
  cointType?: string;
  displayName?: string;
  basePrice?: number;
  baseWinningPrice?: number;
  description?: string;
  user?: IUser;
  game?: IGame;
  bids?: IBid[];
}

export class Coin implements ICoin {
  constructor(
    public id?: number,
    public cointType?: string,
    public displayName?: string,
    public basePrice?: number,
    public baseWinningPrice?: number,
    public description?: string,
    public user?: IUser,
    public game?: IGame,
    public bids?: IBid[]
  ) {}
}
