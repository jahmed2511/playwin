import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';

export interface IRedeem {
  id?: number;
  status?: string;
  amount?: number;
  approvedBy?: string;
  remarks?: string;
  createdOn?: Moment;
  lastModifiedOn?: Moment;
  user?: IUser;
}

export class Redeem implements IRedeem {
  constructor(
    public id?: number,
    public status?: string,
    public amount?: number,
    public approvedBy?: string,
    public remarks?: string,
    public createdOn?: Moment,
    public lastModifiedOn?: Moment,
    public user?: IUser
  ) {}
}
