import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';

export interface IWallet {
  id?: number;
  balance?: number;
  date?: Moment;
  credit?: number;
  debit?: number;
  remarks?: string;
  user?: IUser;
}

export class Wallet implements IWallet {
  constructor(
    public id?: number,
    public balance?: number,
    public date?: Moment,
    public credit?: number,
    public debit?: number,
    public remarks?: string,
    public user?: IUser
  ) {}
}
