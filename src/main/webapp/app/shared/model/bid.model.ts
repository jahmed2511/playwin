import { Moment } from 'moment';
import { IGame } from 'app/shared/model/game.model';
import { ICoin } from 'app/shared/model/coin.model';
import { IUser } from 'app/core/user/user.model';

export interface IBid {
  id?: number;
  isWinner?: boolean;
  bidNo?: number;
  noOfCoins?: number;
  bidTime?: Moment;
  isDrowHappan?: boolean;
  remarks?: string;
  game?: IGame;
  coin?: ICoin;
  user?: IUser;
}

export class Bid implements IBid {
  constructor(
    public id?: number,
    public isWinner?: boolean,
    public bidNo?: number,
    public noOfCoins?: number,
    public bidTime?: Moment,
    public isDrowHappan?: boolean,
    public remarks?: string,
    public game?: IGame,
    public coin?: ICoin,
    public user?: IUser
  ) {
    this.isWinner = this.isWinner || false;
    this.isDrowHappan = this.isDrowHappan || false;
  }
}
