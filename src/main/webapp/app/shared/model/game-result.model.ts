import { Moment } from 'moment';
import { IGame } from 'app/shared/model/game.model';
import { ICoin } from 'app/shared/model/coin.model';

export interface IGameResult {
  id?: number;
  gameTime?: Moment;
  winnersNos?: string;
  remarks?: string;
  game?: IGame;
  coin?: ICoin;
}

export class GameResult implements IGameResult {
  constructor(
    public id?: number,
    public gameTime?: Moment,
    public winnersNos?: string,
    public remarks?: string,
    public game?: IGame,
    public coin?: ICoin
  ) {}
}
