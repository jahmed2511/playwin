import { IGame } from 'app/shared/model/game.model';
import { ICoin } from 'app/shared/model/coin.model';

export interface IGameDrawConfig {
  id?: number;
  priority?: number;
  rangeFrom?: number;
  rangeTo?: number;
  active?: boolean;
  game?: IGame;
  coin?: ICoin;
}

export class GameDrawConfig implements IGameDrawConfig {
  constructor(
    public id?: number,
    public priority?: number,
    public rangeFrom?: number,
    public rangeTo?: number,
    public active?: boolean,
    public game?: IGame,
    public coin?: ICoin
  ) {
    this.active = this.active || false;
  }
}
