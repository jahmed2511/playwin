import { Moment } from 'moment';
import { ICoin } from 'app/shared/model/coin.model';
import { IUser } from 'app/core/user/user.model';
import { IBid } from 'app/shared/model/bid.model';

export interface IGame {
  id?: number;
  gameName?: string;
  description?: string;
  startTime?: Moment;
  endTime?: Moment;
  interval?: number;
  numberRangeFrom?: number;
  numberRangeTo?: number;
  isEnable?: boolean;
  coins?: ICoin[];
  users?: IUser[];
  bids?: IBid[];
}

export class Game implements IGame {
  constructor(
    public id?: number,
    public gameName?: string,
    public description?: string,
    public startTime?: Moment,
    public endTime?: Moment,
    public interval?: number,
    public numberRangeFrom?: number,
    public numberRangeTo?: number,
    public isEnable?: boolean,
    public coins?: ICoin[],
    public users?: IUser[],
    public bids?: IBid[]
  ) {
    this.isEnable = this.isEnable || false;
  }
}
