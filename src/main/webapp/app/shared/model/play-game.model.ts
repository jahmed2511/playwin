export interface IPlayGame {
  id?: number;
}

export class PlayGame implements IPlayGame {
  constructor(public id?: number) {}
}
