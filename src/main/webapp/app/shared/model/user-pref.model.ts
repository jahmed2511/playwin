import { IUser } from 'app/core/user/user.model';

export interface IUserPref {
  id?: number;
  distributorShare?: number;
  isSpecificPrice?: boolean;
  mobileNo?: string;
  user?: IUser;
}

export class UserPref implements IUserPref {
  constructor(
    public id?: number,
    public distributorShare?: number,
    public isSpecificPrice?: boolean,
    public mobileNo?: string,
    public user?: IUser
  ) {
    this.isSpecificPrice = this.isSpecificPrice || false;
  }
}
