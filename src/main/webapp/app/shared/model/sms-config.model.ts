import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';
import { IGame } from 'app/shared/model/game.model';

export interface ISmsConfig {
  id?: number;
  smsTimeSlots?: string;
  coins?: string;
  expiryDate?: Moment;
  description?: string;
  mobileNo?: string;
  user?: IUser;
  game?: IGame;
}

export class SmsConfig implements ISmsConfig {
  constructor(
    public id?: number,
    public smsTimeSlots?: string,
    public coins?: string,
    public expiryDate?: Moment,
    public description?: string,
    public mobileNo?: string,
    public user?: IUser,
    public game?: IGame
  ) {}
}
