import { IUser } from 'app/core/user/user.model';
import { IGame } from 'app/shared/model/game.model';
import { ICoin } from 'app/shared/model/coin.model';

export interface IAgentCommision {
  id?: number;
  commisionPercentage?: number;
  user?: IUser;
  coin?: ICoin;
  game?: IGame;
}

export class AgentCommision implements IAgentCommision {
  constructor(public id?: number, public commisionPercentage?: number, public user?: IUser, public coin?: ICoin, public game?: IGame) {}
}
