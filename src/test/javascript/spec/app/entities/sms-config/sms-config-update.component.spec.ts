/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { PlaywinTestModule } from '../../../test.module';
import { SmsConfigUpdateComponent } from 'app/entities/sms-config/sms-config-update.component';
import { SmsConfigService } from 'app/entities/sms-config/sms-config.service';
import { SmsConfig } from 'app/shared/model/sms-config.model';

describe('Component Tests', () => {
  describe('SmsConfig Management Update Component', () => {
    let comp: SmsConfigUpdateComponent;
    let fixture: ComponentFixture<SmsConfigUpdateComponent>;
    let service: SmsConfigService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [SmsConfigUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(SmsConfigUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SmsConfigUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SmsConfigService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new SmsConfig(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new SmsConfig();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
