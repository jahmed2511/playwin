/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PlaywinTestModule } from '../../../test.module';
import { SmsConfigDeleteDialogComponent } from 'app/entities/sms-config/sms-config-delete-dialog.component';
import { SmsConfigService } from 'app/entities/sms-config/sms-config.service';

describe('Component Tests', () => {
  describe('SmsConfig Management Delete Component', () => {
    let comp: SmsConfigDeleteDialogComponent;
    let fixture: ComponentFixture<SmsConfigDeleteDialogComponent>;
    let service: SmsConfigService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [SmsConfigDeleteDialogComponent]
      })
        .overrideTemplate(SmsConfigDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SmsConfigDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SmsConfigService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
