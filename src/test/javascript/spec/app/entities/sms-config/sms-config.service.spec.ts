/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { SmsConfigService } from 'app/entities/sms-config/sms-config.service';
import { ISmsConfig, SmsConfig } from 'app/shared/model/sms-config.model';

describe('Service Tests', () => {
  describe('SmsConfig Service', () => {
    let injector: TestBed;
    let service: SmsConfigService;
    let httpMock: HttpTestingController;
    let elemDefault: ISmsConfig;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(SmsConfigService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new SmsConfig(0, 'AAAAAAA', 'AAAAAAA', currentDate, 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign(
          {
            expiryDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a SmsConfig', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            expiryDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            expiryDate: currentDate
          },
          returnedFromService
        );
        service
          .create(new SmsConfig(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a SmsConfig', async () => {
        const returnedFromService = Object.assign(
          {
            smsTimeSlots: 'BBBBBB',
            coins: 'BBBBBB',
            expiryDate: currentDate.format(DATE_TIME_FORMAT),
            description: 'BBBBBB',
            mobileNo: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            expiryDate: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of SmsConfig', async () => {
        const returnedFromService = Object.assign(
          {
            smsTimeSlots: 'BBBBBB',
            coins: 'BBBBBB',
            expiryDate: currentDate.format(DATE_TIME_FORMAT),
            description: 'BBBBBB',
            mobileNo: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            expiryDate: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a SmsConfig', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
