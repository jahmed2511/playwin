/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PlaywinTestModule } from '../../../test.module';
import { SmsConfigDetailComponent } from 'app/entities/sms-config/sms-config-detail.component';
import { SmsConfig } from 'app/shared/model/sms-config.model';

describe('Component Tests', () => {
  describe('SmsConfig Management Detail Component', () => {
    let comp: SmsConfigDetailComponent;
    let fixture: ComponentFixture<SmsConfigDetailComponent>;
    const route = ({ data: of({ smsConfig: new SmsConfig(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [SmsConfigDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(SmsConfigDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SmsConfigDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.smsConfig).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
