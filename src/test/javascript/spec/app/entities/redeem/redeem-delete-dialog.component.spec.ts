/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PlaywinTestModule } from '../../../test.module';
import { RedeemDeleteDialogComponent } from 'app/entities/redeem/redeem-delete-dialog.component';
import { RedeemService } from 'app/entities/redeem/redeem.service';

describe('Component Tests', () => {
  describe('Redeem Management Delete Component', () => {
    let comp: RedeemDeleteDialogComponent;
    let fixture: ComponentFixture<RedeemDeleteDialogComponent>;
    let service: RedeemService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [RedeemDeleteDialogComponent]
      })
        .overrideTemplate(RedeemDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RedeemDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RedeemService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
