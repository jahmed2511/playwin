/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { PlaywinTestModule } from '../../../test.module';
import { RedeemUpdateComponent } from 'app/entities/redeem/redeem-update.component';
import { RedeemService } from 'app/entities/redeem/redeem.service';
import { Redeem } from 'app/shared/model/redeem.model';

describe('Component Tests', () => {
  describe('Redeem Management Update Component', () => {
    let comp: RedeemUpdateComponent;
    let fixture: ComponentFixture<RedeemUpdateComponent>;
    let service: RedeemService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [RedeemUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(RedeemUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RedeemUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RedeemService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Redeem(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Redeem();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
