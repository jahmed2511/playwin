/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PlaywinTestModule } from '../../../test.module';
import { RedeemDetailComponent } from 'app/entities/redeem/redeem-detail.component';
import { Redeem } from 'app/shared/model/redeem.model';

describe('Component Tests', () => {
  describe('Redeem Management Detail Component', () => {
    let comp: RedeemDetailComponent;
    let fixture: ComponentFixture<RedeemDetailComponent>;
    const route = ({ data: of({ redeem: new Redeem(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [RedeemDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(RedeemDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RedeemDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.redeem).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
