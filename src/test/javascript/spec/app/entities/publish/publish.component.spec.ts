/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PlaywinTestModule } from '../../../test.module';
import { PublishComponent } from 'app/entities/publish/publish.component';
import { PublishService } from 'app/entities/publish/publish.service';
import { Publish } from 'app/shared/model/publish.model';

describe('Component Tests', () => {
  describe('Publish Management Component', () => {
    let comp: PublishComponent;
    let fixture: ComponentFixture<PublishComponent>;
    let service: PublishService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [PublishComponent],
        providers: []
      })
        .overrideTemplate(PublishComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PublishComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PublishService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Publish(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.publishes[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
