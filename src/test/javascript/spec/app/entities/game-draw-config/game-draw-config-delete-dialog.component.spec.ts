import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PlaywinTestModule } from '../../../test.module';
import { GameDrawConfigDeleteDialogComponent } from 'app/entities/game-draw-config/game-draw-config-delete-dialog.component';
import { GameDrawConfigService } from 'app/entities/game-draw-config/game-draw-config.service';

describe('Component Tests', () => {
  describe('GameDrawConfig Management Delete Component', () => {
    let comp: GameDrawConfigDeleteDialogComponent;
    let fixture: ComponentFixture<GameDrawConfigDeleteDialogComponent>;
    let service: GameDrawConfigService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [GameDrawConfigDeleteDialogComponent]
      })
        .overrideTemplate(GameDrawConfigDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(GameDrawConfigDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(GameDrawConfigService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
