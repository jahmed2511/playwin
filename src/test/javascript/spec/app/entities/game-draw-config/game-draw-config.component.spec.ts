import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PlaywinTestModule } from '../../../test.module';
import { GameDrawConfigComponent } from 'app/entities/game-draw-config/game-draw-config.component';
import { GameDrawConfigService } from 'app/entities/game-draw-config/game-draw-config.service';
import { GameDrawConfig } from 'app/shared/model/game-draw-config.model';

describe('Component Tests', () => {
  describe('GameDrawConfig Management Component', () => {
    let comp: GameDrawConfigComponent;
    let fixture: ComponentFixture<GameDrawConfigComponent>;
    let service: GameDrawConfigService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [GameDrawConfigComponent],
        providers: []
      })
        .overrideTemplate(GameDrawConfigComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(GameDrawConfigComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(GameDrawConfigService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new GameDrawConfig(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.gameDrawConfigs[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
