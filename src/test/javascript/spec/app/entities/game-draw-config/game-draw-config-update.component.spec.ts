import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PlaywinTestModule } from '../../../test.module';
import { GameDrawConfigUpdateComponent } from 'app/entities/game-draw-config/game-draw-config-update.component';
import { GameDrawConfigService } from 'app/entities/game-draw-config/game-draw-config.service';
import { GameDrawConfig } from 'app/shared/model/game-draw-config.model';

describe('Component Tests', () => {
  describe('GameDrawConfig Management Update Component', () => {
    let comp: GameDrawConfigUpdateComponent;
    let fixture: ComponentFixture<GameDrawConfigUpdateComponent>;
    let service: GameDrawConfigService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [GameDrawConfigUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(GameDrawConfigUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(GameDrawConfigUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(GameDrawConfigService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new GameDrawConfig(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new GameDrawConfig();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
