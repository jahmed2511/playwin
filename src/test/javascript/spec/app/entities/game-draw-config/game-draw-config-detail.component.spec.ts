import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PlaywinTestModule } from '../../../test.module';
import { GameDrawConfigDetailComponent } from 'app/entities/game-draw-config/game-draw-config-detail.component';
import { GameDrawConfig } from 'app/shared/model/game-draw-config.model';

describe('Component Tests', () => {
  describe('GameDrawConfig Management Detail Component', () => {
    let comp: GameDrawConfigDetailComponent;
    let fixture: ComponentFixture<GameDrawConfigDetailComponent>;
    const route = ({ data: of({ gameDrawConfig: new GameDrawConfig(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [GameDrawConfigDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(GameDrawConfigDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(GameDrawConfigDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.gameDrawConfig).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
