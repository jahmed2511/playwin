/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PlaywinTestModule } from '../../../test.module';
import { BidDetailComponent } from 'app/entities/bid/bid-detail.component';
import { Bid } from 'app/shared/model/bid.model';

describe('Component Tests', () => {
  describe('Bid Management Detail Component', () => {
    let comp: BidDetailComponent;
    let fixture: ComponentFixture<BidDetailComponent>;
    const route = ({ data: of({ bid: new Bid(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [BidDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(BidDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BidDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.bid).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
