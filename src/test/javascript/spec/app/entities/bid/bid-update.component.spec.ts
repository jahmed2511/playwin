/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { PlaywinTestModule } from '../../../test.module';
import { BidUpdateComponent } from 'app/entities/bid/bid-update.component';
import { BidService } from 'app/entities/bid/bid.service';
import { Bid } from 'app/shared/model/bid.model';

describe('Component Tests', () => {
  describe('Bid Management Update Component', () => {
    let comp: BidUpdateComponent;
    let fixture: ComponentFixture<BidUpdateComponent>;
    let service: BidService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [BidUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(BidUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BidUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BidService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Bid(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Bid();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
