/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PlaywinTestModule } from '../../../test.module';
import { UserPrefDeleteDialogComponent } from 'app/entities/user-pref/user-pref-delete-dialog.component';
import { UserPrefService } from 'app/entities/user-pref/user-pref.service';

describe('Component Tests', () => {
  describe('UserPref Management Delete Component', () => {
    let comp: UserPrefDeleteDialogComponent;
    let fixture: ComponentFixture<UserPrefDeleteDialogComponent>;
    let service: UserPrefService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [UserPrefDeleteDialogComponent]
      })
        .overrideTemplate(UserPrefDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UserPrefDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UserPrefService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
