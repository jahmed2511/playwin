/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PlaywinTestModule } from '../../../test.module';
import { UserPrefDetailComponent } from 'app/entities/user-pref/user-pref-detail.component';
import { UserPref } from 'app/shared/model/user-pref.model';

describe('Component Tests', () => {
  describe('UserPref Management Detail Component', () => {
    let comp: UserPrefDetailComponent;
    let fixture: ComponentFixture<UserPrefDetailComponent>;
    const route = ({ data: of({ userPref: new UserPref(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [UserPrefDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(UserPrefDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UserPrefDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.userPref).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
