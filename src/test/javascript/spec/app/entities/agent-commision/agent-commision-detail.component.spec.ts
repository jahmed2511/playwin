import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PlaywinTestModule } from '../../../test.module';
import { AgentCommisionDetailComponent } from 'app/entities/agent-commision/agent-commision-detail.component';
import { AgentCommision } from 'app/shared/model/agent-commision.model';

describe('Component Tests', () => {
  describe('AgentCommision Management Detail Component', () => {
    let comp: AgentCommisionDetailComponent;
    let fixture: ComponentFixture<AgentCommisionDetailComponent>;
    const route = ({ data: of({ agentCommision: new AgentCommision(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [AgentCommisionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(AgentCommisionDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AgentCommisionDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.agentCommision).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
