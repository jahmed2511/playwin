import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PlaywinTestModule } from '../../../test.module';
import { AgentCommisionUpdateComponent } from 'app/entities/agent-commision/agent-commision-update.component';
import { AgentCommisionService } from 'app/entities/agent-commision/agent-commision.service';
import { AgentCommision } from 'app/shared/model/agent-commision.model';

describe('Component Tests', () => {
  describe('AgentCommision Management Update Component', () => {
    let comp: AgentCommisionUpdateComponent;
    let fixture: ComponentFixture<AgentCommisionUpdateComponent>;
    let service: AgentCommisionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [AgentCommisionUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(AgentCommisionUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AgentCommisionUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AgentCommisionService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new AgentCommision(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new AgentCommision();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
