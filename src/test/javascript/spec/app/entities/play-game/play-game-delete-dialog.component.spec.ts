/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PlaywinTestModule } from '../../../test.module';
import { PlayGameDeleteDialogComponent } from 'app/entities/play-game/play-game-delete-dialog.component';
import { PlayGameService } from 'app/entities/play-game/play-game.service';

describe('Component Tests', () => {
  describe('PlayGame Management Delete Component', () => {
    let comp: PlayGameDeleteDialogComponent;
    let fixture: ComponentFixture<PlayGameDeleteDialogComponent>;
    let service: PlayGameService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [PlayGameDeleteDialogComponent]
      })
        .overrideTemplate(PlayGameDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PlayGameDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PlayGameService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
