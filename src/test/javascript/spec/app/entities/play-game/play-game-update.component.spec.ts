/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { PlaywinTestModule } from '../../../test.module';
import { PlayGameUpdateComponent } from 'app/entities/play-game/play-game-update.component';
import { PlayGameService } from 'app/entities/play-game/play-game.service';
import { PlayGame } from 'app/shared/model/play-game.model';

describe('Component Tests', () => {
  describe('PlayGame Management Update Component', () => {
    let comp: PlayGameUpdateComponent;
    let fixture: ComponentFixture<PlayGameUpdateComponent>;
    let service: PlayGameService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [PlayGameUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(PlayGameUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PlayGameUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PlayGameService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new PlayGame(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new PlayGame();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
