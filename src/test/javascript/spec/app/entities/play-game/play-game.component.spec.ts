/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PlaywinTestModule } from '../../../test.module';
import { PlayGameComponent } from 'app/entities/play-game/play-game.component';
import { PlayGameService } from 'app/entities/play-game/play-game.service';
import { PlayGame } from 'app/shared/model/play-game.model';

describe('Component Tests', () => {
  describe('PlayGame Management Component', () => {
    let comp: PlayGameComponent;
    let fixture: ComponentFixture<PlayGameComponent>;
    let service: PlayGameService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [PlayGameComponent],
        providers: []
      })
        .overrideTemplate(PlayGameComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PlayGameComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PlayGameService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new PlayGame(123)],
            headers
          })
        )
      );

      comp.ngOnInit();

      expect(service.query).toHaveBeenCalled();
    });
  });
});
