/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PlaywinTestModule } from '../../../test.module';
import { PlayGameDetailComponent } from 'app/entities/play-game/play-game-detail.component';
import { PlayGame } from 'app/shared/model/play-game.model';

describe('Component Tests', () => {
  describe('PlayGame Management Detail Component', () => {
    let comp: PlayGameDetailComponent;
    let fixture: ComponentFixture<PlayGameDetailComponent>;
    const route = ({ data: of({ playGame: new PlayGame(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [PlayGameDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(PlayGameDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PlayGameDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.playGame).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
