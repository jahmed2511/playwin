/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PlaywinTestModule } from '../../../test.module';
import { GameResultDetailComponent } from 'app/entities/game-result/game-result-detail.component';
import { GameResult } from 'app/shared/model/game-result.model';

describe('Component Tests', () => {
  describe('GameResult Management Detail Component', () => {
    let comp: GameResultDetailComponent;
    let fixture: ComponentFixture<GameResultDetailComponent>;
    const route = ({ data: of({ gameResult: new GameResult(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [GameResultDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(GameResultDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(GameResultDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.gameResult).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
