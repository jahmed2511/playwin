/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PlaywinTestModule } from '../../../test.module';
import { GameResultDeleteDialogComponent } from 'app/entities/game-result/game-result-delete-dialog.component';
import { GameResultService } from 'app/entities/game-result/game-result.service';

describe('Component Tests', () => {
  describe('GameResult Management Delete Component', () => {
    let comp: GameResultDeleteDialogComponent;
    let fixture: ComponentFixture<GameResultDeleteDialogComponent>;
    let service: GameResultService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [GameResultDeleteDialogComponent]
      })
        .overrideTemplate(GameResultDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(GameResultDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(GameResultService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
