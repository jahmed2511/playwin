/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { PlaywinTestModule } from '../../../test.module';
import { GameResultUpdateComponent } from 'app/entities/game-result/game-result-update.component';
import { GameResultService } from 'app/entities/game-result/game-result.service';
import { GameResult } from 'app/shared/model/game-result.model';

describe('Component Tests', () => {
  describe('GameResult Management Update Component', () => {
    let comp: GameResultUpdateComponent;
    let fixture: ComponentFixture<GameResultUpdateComponent>;
    let service: GameResultService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PlaywinTestModule],
        declarations: [GameResultUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(GameResultUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(GameResultUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(GameResultService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new GameResult(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new GameResult();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
